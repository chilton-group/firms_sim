#! /usr/bin/env python3

import numpy as np
import textwrap
import matplotlib.pyplot as plt
import argparse
from matplotlib.ticker import MultipleLocator, LinearLocator


def get_ax_size(fig, ax):
    bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    width, height = bbox.width, bbox.height
    width *= fig.dpi
    height *= fig.dpi
    return width, height


def read_user_input():
    #Read in command line arguments

    parser = argparse.ArgumentParser(description="Plots simulated far-infrared magnetospectroscopy (FIRMS) heatmap using output of firms_sim",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("nums", type = str, default = [""], nargs='+',
                        help = "mode numbers to plot. e.g. 34 will use data_34.dat, \n34_35_36 will use data_34_35_36.dat, \nand 30-40 will use data_30.dat to data_40.dat inclusive")
    parser.add_argument("--lines", type = int, default = None, 
                        help = "plot raw data")
    parser.add_argument("--add_noise", action="store_true",  
                        help = "Add random noise to signal before division")
    parser.add_argument("--division", type = int, default=3,
                        help=textwrap.dedent("""
Division scheme: 
    0 = Raw spectrum with no treatment
    1 = Subtraction style - Subtraction of data for previous field from current field
    2 = Division style     - Division of data for current field by previous field
    3 = Marbey style       - Division of each field step by mean of all field steps (default)
    """))
    parser.add_argument("--x_lims", nargs=2, type=float, metavar=("lower","upper"),
                        help=textwrap.dedent("""
Lower and upper x limits of final plot e.g. --x_lims 0 100"""))
    parser.add_argument("--xmal", type=float, metavar="number",
                        help=textwrap.dedent("""
Number of major x ticks"""))
    parser.add_argument("--xmil", type=float, metavar="number",
                        help=textwrap.dedent("""
Spacing of minor x ticks"""))
    parser.add_argument("--abs_shift", type=float, metavar="number", default=2., 
                        help=textwrap.dedent("""
Shift added to all spectra before normalisation - helps to avoid division-by-zero issues
                            """))
    parser.add_argument("--exp", action='store_true',
                        help=textwrap.dedent("""
Add experimental spectra above ab initio spectra
                            """))

    user_args = parser.parse_args()

    # Check 2 x lims provided
    if user_args.x_lims and len(user_args.x_lims) != 2:
        parser.error('Incorrect x limits specified!')

    return user_args


def trans_to_abs(transmittance):
    return [ 2 - np.log10(x) for x in transmittance ] # T = 10^(2-x) where x is normalised and considers the matching factor


def read_spectrum(file_name):

    with open(file_name,"r") as f:
        line = next(f)
        f_bounds = [float(i) for i in line.split()]
        fields = np.arange(f_bounds[0], f_bounds[1]+f_bounds[2], f_bounds[2])
        line = next(f)
        mode_numbers = line.split()
        line = next(f)
        e_lower, e_upper, resolution = [float(i) for i in line.split()]

        f.close()
    
    ir_energies = np.linspace(e_lower, e_upper, int(resolution))
    absorbances = np.loadtxt(file_name, skiprows=3)

    return e_lower, e_upper, int(resolution), fields, ir_energies, absorbances, mode_numbers


def get_data(user_args):

    # Figure out if user is giving a single mode number,
    # a list of modes which have been coupled together,
    # or a range of individual modes

    # _ between numbers indicates a single file with coupling to multiple modes
    if user_args.nums[0].count("_") != 0:

        for it in range(len(user_args.nums)):
            e_lower, e_upper, resolution, fields, ir_energies, temp_absorbances, mode_numbers = read_spectrum("data_{}.dat".format(user_args.nums[it]))
            if it == 0:
                absorbances = np.copy(temp_absorbances)
            else:
                absorbances += temp_absorbances
            print("Data taken from data_{}.dat".format(user_args.nums[it]))

    # - between numbers indicates multiple files each coupling to a single mode
    elif user_args.nums[0].count("-") != 0  and len(user_args.nums) == 1:
        lower, upper = user_args.nums[0].split("-")

        # Read first datafile and get energies, fields, resolution, etc
        e_lower, e_upper, resolution, fields, ir_energies, absorbances, mode_numbers = read_spectrum("data_{}.dat".format(lower))

        # Add absorbance for other modes
        summed_modes = np.arange(int(lower), int(upper) + 1)
        for it in summed_modes:
            absorbances += np.loadtxt("data_{:d}.dat".format(it), skiprows=3)
        # Normalise by number of summed spectra
        absorbances /= np.size(summed_modes)

        print("Data taken from data_{}.dat to data_{}.dat".format(lower,upper))

    # just a number indicates a single file with coupling to a single mode
    elif  len(user_args.nums) == 1:
        e_lower, e_upper, resolution, fields, ir_energies, absorbances, mode_numbers = read_spectrum("data_{}.dat".format(user_args.nums[0]))

        print("Data taken from data_{}.dat".format(user_args.nums[0]))

    return  ir_energies, absorbances, fields, mode_numbers, e_lower, e_upper, resolution


def get_data_bounds(x, y) :
    # Make arrays of x and y values - these specify the corners of each pixel
    # First find average step size between points in x axis 
    # This is used for the first and last points
    x_diff_avg = np.mean(np.diff(x))

    # Calculate forward difference - these are used to calculate the x bounds of each pixel other than the first and last
    x_diff_forward = np.diff(x)

    x_bounds = []

    # Set first (lower) bound of first datapoint as datapoint minus average forward difference/2
    x_bounds.append(x[0] - x_diff_avg / 2.)

    # Set remaining bounds other than upper bound of final datapoint
    # Set as data + half of forward difference
    for i in range(np.size(x) - 1) :
        x_bounds.append(x[i] + x_diff_forward[i] / 2)

    # Set upper bound of final datapoint as datapoint plus average forward difference/2
    x_bounds.append(x[-1] + x_diff_avg / 2.)

    # Now make y axis bounds
    # First find average step size between points in y axis
    # This is used for the first and last points
    y_diff_avg = np.mean(np.diff(y))

    # Calculate forward difference - these are used to calculate the x bounds of each pixel other than the first and last
    y_diff_forward = np.diff(y)

    y_bounds = []

    # Set first (lower) bound of first datapoint as datapoint minus average forward difference/2
    y_bounds.append(y[0] - y_diff_avg / 2.)

    # Set remaining bounds other than upper bound of final datapoint
    # Set as data + half of forward difference
    for i in range(np.size(y) - 1) :
        y_bounds.append(y[i] + y_diff_forward[i] / 2)

    # Set upper bound of final datapoint as datapoint plus average forward difference/2
    y_bounds.append(y[-1] + y_diff_avg / 2.)

    # Now use x_bounds and y_bounds to make matrices of bounds for x and y values
    xx_bounds, yy_bounds = np.meshgrid(x_bounds, y_bounds)

    return xx_bounds, yy_bounds


def min_max_ticks_with_zero(axdata, nticks):
    # Calculates tick positions including zero given a specified number of ticks either size of zero

    # Add on extra tick for some reason
    nticks += 1

    lowticks = np.linspace(np.min(axdata), 0, nticks)
    highticks = np.linspace(np.max(axdata), 0, nticks)
    ticks = np.append(np.append(lowticks[:-1],[0.0]), np.flip(highticks[:-1]))

    return ticks


def calc_moving_diff(user_args, absorbances, num_fields):

    # Calculate moving difference of spectra at different fields
    moving_difference = np.copy(absorbances) * 0.0

    # Divide absorbances by maximum to bring down to more(?) sensible numbers
    absorbances /= np.max(absorbances)

    # No division, raw data as heatmap
    if user_args.division == 0:
        moving_difference = np.copy(absorbances)
    # Subtract previous spectrum from current
    elif user_args.division == 1:
        for it in range(1, num_fields):
            moving_difference[it, :] += absorbances[it, :] - absorbances[it - 1, :]
    # Divide current spectrum by previous spectrum
    elif user_args.division == 2:
        absorbances *= 3
        absorbances += user_args.abs_shift
        for it in range(1, num_fields):
            moving_difference[it, :] = absorbances[it, :] / absorbances[it - 1, :]
        moving_difference[0, :] = moving_difference[1, :]
    # Divide current spectrum by mean of all spectra
    elif user_args.division == 3:
        absorbances += user_args.abs_shift
        moving_difference = np.copy(absorbances)/np.mean(absorbances, axis=0)

    return moving_difference


def plot_lines(user_args, ir_energies, moving_difference, num_fields, fields, e_lower, e_upper):

        # Make figure
        fig, (ax1) = plt.subplots(1, 1, figsize=(5,5))

        inten = np.zeros(num_fields)

        # Plot absorbance vs energy for each field value
        for i in range(1,np.size(fields),5) :
            push = i*10
            scale = user_args.lines
            ax1.plot(ir_energies, moving_difference[i,:]*scale + push, label = str(fields[i]), color = "black")        

        # Set x axis label
        ax1.set_xlabel("Energy (cm$^{-1}$)")

        # Set x axis ticks
        ax1.set_xlim([e_lower, e_upper])

        # Set x limits
        if user_args.x_lims:
            ax1.set_xlim(user_args.x_lims[0], user_args.x_lims[1])

        #Set y axis label
        ax1.set_ylabel("$\\Delta A$ (Arb. Units)")

        # Remove y ticks 
        #ax1.set_yticks([])

        if user_args.xmal:
            ax1.xaxis.set_major_locator(LinearLocator(int(user_args.xmal)))
        if user_args.xmil:
            ax1.xaxis.set_minor_locator(MultipleLocator(user_args.xmil))

        # Remove plot box lines
        ax1.spines["right"].set_visible(False)
        ax1.spines["top"].set_visible(False)

        # Show plot
        plt.show()
        exit()


def get_peaks(absorbances, ir_energies, fields, lower, upper, ss):

    maxlocs = []
    max_fields = []

    for it, field in enumerate(fields):

        # Create window
        if ss > 0.:
            lower += ss
            upper = lower + 3 + ss

        # Find indices which cut IR data at lower and upper
        lind = (np.abs(ir_energies - lower)).argmin()
        uind = (np.abs(ir_energies - upper)).argmin()

        # And absorbances for each field value
        currabs = absorbances[it,lind:uind]

        if np.size(currabs) == 0:
            continue
        else:
            maxlocs.append(lind+np.argmax(currabs))
            max_fields.append(field)

    max_enes = ir_energies[maxlocs]

    return max_enes, max_fields


def get_data_exp(matrix_file, lower=0., upper=900.):
    # Loads in FIRMS matrix file and process for plotting
    # Matrix is written in the file as
    # ENERGY_1 ABS_1 ABS_2 ... ABS_M`
    # ENERGY_2 ABS_1 ABS_2 ... ABS_M
    # ..
    # ENERGY_N ABS_1 ABS_2 ... ABS_M
    # Where 1 - M are the indices for the fields specified in the 3rd headerline of the file
    # And 1 - N are the indices for the infrared energies

    # Load data from file - skip first three headerlines
    data = np.loadtxt(matrix_file, skiprows = 3)

    # Separate data out into infrared energies
    ir_energies = data[:,0]

    # Find indices which cut IR data at lower and upper
    lower = (np.abs(ir_energies - lower)).argmin()
    upper = (np.abs(ir_energies - upper)).argmin()

    ir_energies = ir_energies[lower:upper]

    # And absorbances for each field value
    absorbances = data[lower:upper,1:-1]

    absorbances = np.transpose(absorbances)

    # Now read the header of the file to get the field values
    # The third line of the file contains the field values
    field_str = np.loadtxt(matrix_file, skiprows = 2, max_rows = 1, dtype = str)
    fields = []
    for it, item in enumerate(field_str) :
        if item == '=' :
            fields.append(float(field_str[it + 1]))
    fields = np.asarray(fields)[:-1]

    n_fields = np.size(fields)

    return  ir_energies, absorbances, fields, n_fields


def divided_spectrum_heat(fig, ax1, ax2, lower, upper, title_label = '', ss=0.):

    ax1.clear()
    ax2.clear()

    # Import data
    ir_energies, transmittance, fields, n_fields = get_data_exp("firms_matrix_untreated.txt",lower, upper)

    # Convert to absorbance
    absorbances = trans_to_abs(transmittance)

    # Normalise by average of all spectra
    absorbances = np.divide(absorbances, np.mean(absorbances,axis=0))

    max_enes, max_fields = get_peaks(absorbances, ir_energies, fields, lower, upper, ss)

    # Find largest absolute value of shifted absorbances
    # Use this to make limits in colorbar and colormap symmetric
    cm_min = np.min(absorbances)
    cm_max = np.max(absorbances)

    # Get x and y bounds of each pixel
    xx_bounds, yy_bounds = get_data_bounds(ir_energies, fields[:])

    # Plot shifted data
    pcm = ax1.pcolormesh(xx_bounds, yy_bounds, absorbances)

    ax1.set_xlabel(r"Energy (cm$^{-1}$)")
    ax1.set_ylabel("Magnetic field (T)")

    # Add colour bar with same map as plot and 5 ticks
    cbar = fig.colorbar(mappable = pcm, cax = ax2, ticks = np.linspace(cm_min, cm_max, 5), format= '% .3f')

    #Set colourbar y axis label
    ax2.set_ylabel("Normalised absorbance")

    if upper-lower > 100 :
        step = 45
    else:
        step = 15

    if title_label != "":
        ax1.set_xticks(range(lower, int(upper)+step, step))
        ax1.set_title(label=title_label, loc="right")

    return fields, max_enes, max_fields


def plot_heat(user_args, ir_energies, moving_difference, fields):

    # Create figure
    if user_args.exp:
        fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2, figsize=(5,8.5), gridspec_kw={"width_ratios":[1.25, 0.025], "height_ratios":[1.0/3.0, 1.0, 1.0]})
        fig.delaxes(ax2)
        print(get_ax_size(fig, ax3))
        exit()
    else:
        fig, (ax5, ax6) = plt.subplots(1, 2, figsize=(6,4), num = "FDMR Heatmap", gridspec_kw={"width_ratios":[1.25, 0.025]})

    # Get x and y bounds of each pixel
    xx_bounds, yy_bounds = get_data_bounds(ir_energies, fields[:])

    # Plot data 
    pcm = ax5.pcolormesh(xx_bounds, yy_bounds, moving_difference)

    cm_min = np.min(moving_difference)
    cm_max = np.max(moving_difference)

    # Add colour bar with same map as plot and 5 ticks
    cbar = fig.colorbar(mappable = pcm, cax = ax6, ticks = np.linspace(cm_min, cm_max, 5), format= '% .3f')

    # Make colorbar limits max and min moving_difference values
    # sometimes the max and min ticks dont appear due to rounding issues
    # this fixes that bug...
    ax6.set_ylim([cm_min, cm_max])

    # Set Axis Lables
    ax5.set_xlabel("Energy (cm$^{-1}$)")
    ax5.set_ylabel("Magnetic field (T)")

    # Set x limits
    if user_args.x_lims:
        ax5.set_xlim(user_args.x_lims[0], user_args.x_lims[1])

    # Label color bar
    ax6.set_ylabel("Normalised absorbance", rotation = 90)

    if user_args.xmal:
        ax5.xaxis.set_major_locator(LinearLocator(int(user_args.xmal)))
    if user_args.xmil:
        ax5.xaxis.set_minor_locator(MultipleLocator(user_args.xmil))

    ax5.set_yticks(np.arange(0, 17, 2))
    #ax5.set_yticks(np.linspace(fields[0], fields[-1], 10))


    if user_args.exp:

        ir_energies, transmittance, fields, n_fields = get_data_exp("fdmr_matrix_untreated.txt", 370, 550)

        # Plot 0 T and 16 T spectra
        for it in range(0,n_fields,n_fields-1):
            # Plot transmittance vs energy for each field value
            push = 0#it
            scale = 1
            ax1.plot(ir_energies, transmittance[it][:]*scale + push)

        ax1.set_ylabel("Transmittance \n (arb. units)")
        ax1.legend(['0 T', '16 T'],frameon=False, loc='upper left', handlelength=1, ncol = 1, columnspacing=0.5, handletextpad=0.2, borderaxespad=0.2)

        ax1.set_ylim([0., 2.0])
        ax1.yaxis.set_major_locator(LinearLocator(3))
        ax1.set_xlim([370, 550])
        ax1.xaxis.set_major_locator(LinearLocator(7))
        ax1.xaxis.set_minor_locator(MultipleLocator(10))
        ax1.set_xticklabels([])

        # Plot heatmap for all fields
        fields, maxes, max_fields = divided_spectrum_heat(fig, ax3, ax4, 370, 550)

        ax3.annotate("A",[385, 0.0], color='White', fontsize="larger")
        ax3.annotate("B",[411, 0.0], color='White', fontsize="larger")
        ax3.annotate("C",[431, 0.0], color='White', fontsize="larger")
        ax3.annotate("D",[458, 0.0], color='White', fontsize="larger")
        ax3.annotate("E",[477, 0.0], color='White', fontsize="larger")
        ax3.annotate("F",[523, 0.0], color='White', fontsize="larger")

        # ax3.annotate('A', xy=(400, 16), xytext=(400, 17),
        # ha='center', va='bottom',bbox=dict(boxstyle='square', fc='white'),
        # arrowprops=dict(arrowstyle='-[, widthB=2.0, lengthB=1.5', lw=1.0))

        # ax3.annotate('B', xy=(441, 16), xytext=(441, 17),
        # ha='center', va='bottom',bbox=dict(boxstyle='square', fc='white'),
        # arrowprops=dict(arrowstyle='-[, widthB=2.0, lengthB=1.5', lw=1.0))

        # ax3.annotate('C', xy=(472, 16), xytext=(472, 17),
        # ha='center', va='bottom',bbox=dict(boxstyle='square', fc='white'),
        # arrowprops=dict(arrowstyle='-[, widthB=2.0, lengthB=1.5', lw=1.0))

        # ax3.annotate('D', xy=(520, 16), xytext=(520, 17),
        # ha='center', va='bottom',bbox=dict(boxstyle='square', fc='white'),
        # arrowprops=dict(arrowstyle='-[, widthB=2.0, lengthB=1.5', lw=1.0))

        #ax3.annotate("407 cm$^{-1}$",[407, -3], annotation_clip=False, rotation=-45)
        #ax3.arrow(415, -1, -8, 1, clip_on=False)

        ax3.set_xlabel("")

        ax3.set_xlim([370, 550])

        ax3.xaxis.set_major_locator(LinearLocator(7))
        ax3.xaxis.set_minor_locator(MultipleLocator(10))
        ax3.set_xticklabels([])
        ax3.set_yticks(np.arange(0, 17, 2))

        ax1.set_title(label="(a)", loc="left")
        ax3.set_title(label="(b)", loc="left")
        ax5.set_title(label="(c)", loc="left")


    fig.tight_layout()

    save_name = "heatmap.png"

    plt.savefig(save_name, dpi = 600)

    # Show figure
    plt.show()


#####################################################################################################################################

if __name__ == "__main__":

    # Read user arguments to program
    user_args = read_user_input()

    # Load data from matrix file
    ir_energies, absorbances, fields, mode_numbers, e_lower, e_upper, resolution = get_data(user_args)

    num_fields = np.shape(absorbances)[1]

    absorbances = np.transpose(absorbances)

    moving_difference = calc_moving_diff(user_args, absorbances, num_fields)

    # Just plot absorbance vs energy as a line for each field
    if user_args.lines : 
        plot_lines(user_args, ir_energies, moving_difference, num_fields, fields, e_lower, e_upper)
    # Else make heatmap
    else:
        plot_heat(user_args, ir_energies, moving_difference, fields)

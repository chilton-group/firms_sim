!
!                          ┌─┐┬┬─┐┌┬┐┌─┐    ┌─┐┬┌┬┐
!                          ├┤ │├┬┘│││└─┐    └─┐││││
!                          └  ┴┴└─┴ ┴└─┘────└─┘┴┴ ┴
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                            !
!   firms_sim.f90                                                            !
!                                                                            !
!   This program is a part of firms_sim                                      !
!                                                                            !
!                                                                            !
!                                                                            !
!                                                                            !
!      Authors:                                                              !
!       Jon Kragskow                                                         !
!       Nicholas Chilton                                                     !
!                                                                            !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program firms_sim
USE hdf5
use matrix_tools
use iso_c_binding
implicit none
real(kind = 8), allocatable       :: mode_energies(:), EQ_H_pho(:,:), field_vecs(:,:,:), &
                                     field_strengths(:), total_absorbance(:,:), &
                                     e_axis(:), CF_vals(:), PHO_vals(:), field_angs(:,:), &
                                     delec_mu(:,:), red_masses(:), CFP_polynomials(:,:,:,:), &
                                     dbkq_dq(:,:,:)
complex(kind = 8), allocatable    :: EQ_H_CF(:,:), H_Coup(:,:), EQ_H_total(:,:), &
                                     EQ_H_total_NC(:,:), CF_vecs(:,:)
real(kind = 8)                    :: J, gJ, field_lower, field_upper, field_step, lower_e_bound, &
                                     upper_e_bound, OEF(3), temperature, elec_scale, coupling_scale, &
                                     B_quant, fwhm, linewidth, user_field_vector(3), calibration(2)
character(len = 1000)             :: input_file, polynomials_file, mode_energies_file, CFPs_file, ion, &
                                     single_axis, delec_mu_file, red_mass_file
integer                           :: elec_dim, n_modes, n_coup_modes, n_vib_states, &
                                     pho_dim, coup_dim, n_field_strengths, n_field_vecs, &
                                     ox_state, zcw_val, resolution, ir_grid_size, mode, hdf5_dim
integer, allocatable              :: mode_numbers(:), basis_labels(:,:)
character(len = 500)              :: trans_treat
LOGICAL                           :: mask_trans, nooef, new_cf_energies, mode_calibration

! Set up hdf5 datatypes
INTEGER(HID_T) :: hdf5_complex_datatype, hdf5_data_id, hdf5_file_id, hdf5_group_id
INTEGER :: hdf5_error
TYPE(C_PTR) :: hdf5_ptr
LOGICAL :: hdf5_method, hdf5_kramers
CALL SETUP_HDF5
hdf5_method = .FALSE.
hdf5_kramers = .FALSE.

! Remove old output file
call EXECUTE_COMMAND_LINE('rm -f firms_sim.out')

! Read command line arguments
call PARSE_INPUT_ARGUMENTS(input_file)

! Add header to output file
call MAKE_OUTPUT_FILE

! Read input file
call READ_INPUT_FILE(input_file, polynomials_file, mode_energies_file, n_coup_modes, n_modes,& 
                     mode_numbers, n_vib_states, CFPs_file, field_lower, field_upper,&
                     field_step, zcw_val, single_axis, lower_e_bound, upper_e_bound, &
                     resolution, ion, ox_state, temperature, trans_treat, elec_scale, &
                     coupling_scale, ir_grid_size, mask_trans, nooef, new_cf_energies, fwhm, &
                     delec_mu_file, red_mass_file, user_field_vector, hdf5_method, hdf5_dim,&
                     calibration, mode_calibration)

! Copy input to output
call COPY_INPUT_TO_OUTPUT(input_file)

IF(hdf5_method) then
    elec_dim = hdf5_dim                 ! Set electronic Hamiltonian dimension as user input
    call GET_HDF5_DIMENSION(hdf5_dim)   ! Get hdf5 file dimension from file
ELSE
    ! Calculate quantum numbers, gJ, etc
    call ELECTRONIC_OPTIONS(ion, OEF, ox_state, elec_dim, gJ, J)
END IF

! Set number of phonon states and read phonon energies
call PHONON_OPTIONS(n_coup_modes, mode_energies, mode_energies_file, n_modes, &
                    n_vib_states, pho_dim, calibration, mode_calibration)

! Calculate Gaussian width using FWHM value
call CALC_G_WIDTH(fwhm, linewidth, 'Simulation linewidth')

! Set dimensionality of final Hamiltonian
call COUPLING_OPTIONS(pho_dim, elec_dim, coup_dim)

! Calculate applied field strengths, field vectors for powder averaging etc
call FIELD_OPTIONS(field_lower, field_upper, field_step, field_vecs, field_angs, &
                   field_strengths, n_field_strengths, n_field_vecs, &
                   zcw_val, single_axis, ir_grid_size, user_field_vector)

! Calculate equilibrium CF Hamiltonian with Zeeman-z perturbation to quantise KDs
! If single axis is selected then quantise along specified field instead
IF(hdf5_method) THEN
    call CALC_EQ_CF_HAMILTONIAN_HDF5(hdf5_dim, elec_dim, EQ_H_CF, CF_vecs, CF_vals, single_axis, B_quant)
ELSE
    call CALC_EQ_CF_HAMILTONIAN(CFPs_file, elec_dim, EQ_H_CF, CF_vecs, CF_vals, gJ, single_axis, &
                                nooef, OEF, new_cf_energies, B_quant)
END IF
! Replace CF Hamiltonian with diagonal matrix containing energies
    ! i.e. write CF Hamiltonian in its eigenbasis
    EQ_H_CF = DIAG(CF_vals)

if (pho_dim > 1) then
    ! Calculate equilibrium Phonon Hamiltonian
    call CALC_EQ_PHO_HAMILTONIAN(n_coup_modes, mode_energies, mode_numbers, &
                                 n_vib_states, pho_dim, EQ_H_pho)

    ! Read in CFP polynomials
    IF(.NOT. hdf5_method) THEN
        allocate(CFP_polynomials(n_modes, 3, 13, 3))
        CFP_polynomials = 0.0_8
        call READ_PHONON_CF_POLYNOMIAL(polynomials_file, CFP_polynomials, n_modes)

        allocate(dbkq_dq(n_coup_modes, 3, 13))
        ! Select 1st order term of CFP polynomials for coupling modes
        do mode = 1, n_coup_modes
            dbkq_dq(mode,:,:) = CFP_polynomials(mode_numbers(mode), :, :, 3)
        end do
    END IF

    ! Calculate Coupling Hamiltonians, for both CF and HDF5 method
    call CALC_COUPLING_HAMILTONIAN(coup_dim, n_coup_modes, n_modes, &
                                   mode_numbers, elec_dim, n_vib_states, H_Coup, OEF, &
                                   coupling_scale, CF_vecs, dbkq_dq, hdf5_method, hdf5_dim)
end if

IF(.NOT. hdf5_method) THEN
    ! Generate coupled basis label array
    call LABEL_BASIS(elec_dim, pho_dim, n_coup_modes, n_vib_states, coup_dim, basis_labels)

    ! Calculate total Hamiltonian in "zero" field
    ! input H_CF includes a small biasing field (see CALC_EQ_CF_HAMILTONIAN)
    call CALC_H_EQ_TOTAL(elec_dim, pho_dim, coup_dim, EQ_H_CF, EQ_H_pho, H_Coup, EQ_H_total, &
                         EQ_H_total_NC)

    ! Write zero field information to output file
    call WRITE_ZF_INFO(elec_dim, pho_dim, coup_dim, gJ, CF_vecs, CF_vals, EQ_H_pho, EQ_H_total, &
                       EQ_H_total_NC, basis_labels, B_quant)

    deallocate(EQ_H_total_NC, EQ_H_total)
END IF

! Read in reduced mass and electric dipole information
if (trim(trans_treat) /= "simple") then
    call READ_RED_MASSES(red_mass_file, red_masses, n_modes)
    call READ_DELEC_MU(delec_mu_file, delec_mu, red_masses, n_modes)
end if
    
! Calculate Spectrum by averaging over field vectors for each strength
! Recalculates field dependent part of H_total and diagonalises
! for each strength

call MAIN_SPECTRUM(EQ_H_CF, EQ_H_pho, H_Coup, elec_dim, pho_dim, coup_dim, field_vecs, &
                      field_angs, n_field_strengths, n_field_vecs, gJ, lower_e_bound, &
                      upper_e_bound, total_absorbance, e_axis, resolution, temperature, &
                      basis_labels, trans_treat, elec_scale, ir_grid_size, CF_vecs,&
                      linewidth, delec_mu, red_masses, mode_energies, hdf5_method, hdf5_dim, new_cf_energies)

call SAVE_OUTPUT_DATA(field_strengths, total_absorbance, e_axis, field_lower, field_upper, &
                      field_step, mode_numbers, n_coup_modes)

call print_date_and_time('FNSH')

CALL CLOSE_HDF5

CONTAINS

subroutine PARSE_INPUT_ARGUMENTS(input_file)
    implicit none
    character(len=*) :: input_file

    call GET_COMMAND_ARGUMENT(1, input_file)
    if (input_file == '' .OR. input_file == '-h' .OR. input_file == '--h') then
        write(6,'(A)') 'Please provide input file as argument'
        write(6,'(A)') 'for example input use -input'
        STOP
    end if

    if (input_file == '-input') then
        call WRITE_EXAMPLE_INPUT
        write(6,'(A)') '~~~~             Blank input written to blank_input.dat             ~~~~'
        write(6,'(A)') '!!!!                  Replace BOLD TEXT as appropriate                  !!!!'
        STOP
    end if

    if (input_file == '-subscript') then

        open(77, FILE = 'firms_submit.sh', STATUS = 'UNKNOWN')

            write(77,'(A)') '#!/bin/bash --login'
            write(77,'(A)') '#$ -S /bin/bash'
            write(77,'(A)') '#$ -N firms_job # You can change this'
            write(77,'(A)') '#$ -cwd'
            write(77,'(A)') '#$ -pe smp.pe 4 # You can change this - default is 4 cores'
            write(77,'(A)') '#$ -l short # requests short queue up to 12 cores for 1 hour'
            write(77,'(A)') 'export OMP_NUM_THREADS=$NSLOTS # do NOT CHANGE THIS'
            write(77,'(A)') 'module load compilers/intel/18.0.3'
            write(77,'(A)') 'firms_sim input.dat # Change this if your firms_sim input file is named differently'

        close(77)

        write(6,'(A)') '~~~~         Submission script written to firms_submit.sh  in CWD         ~~~~'

        STOP        
    end if
    
end subroutine PARSE_INPUT_ARGUMENTS

subroutine WRITE_EXAMPLE_INPUT
    implicit none

    open(66, FILE = 'blank_input.dat', STATUS = 'UNKNOWN')

        write(66,'(A)') '****core parameters'
        write(66,'(A)') 'eq cfps file FILENAME'
        write(66,'(A)') 'polynomials file FILENAME'
        write(66,'(A)') 'mode energies file FILENAME'
        write(66,'(A)') 'reduced masses file FILENAME'
        write(66,'(A)') 'dipole derivatives file FILENAME'
        write(66,'(A)') 'ion SYMBOL OXSTATE_NUMBER'
        write(66,'(A)') 'modes NUMBER'
        write(66,'(A)') 'use modes NUMBER(S)'
        write(66,'(A)') 'vib states NUMBER'
        write(66,'(A)') 'field LOW HIGH STEP'
        write(66,'(A)') 'energy LOW HIGH NUM_STEPS'
        write(66,'(A)') 'temperature NUMBER'

        write(66,'(A)') '****optional parameters'
        write(66,'(A)') 'zcw NUMBER'
        write(66,'(A)') 'single axis LETTER (x,y,z)'
        write(66,'(A)') 'nooef'
        write(66,'(A)') 'replace cf energies'
        write(66,'(A)') 'scale electronic NUMBER'
        write(66,'(A)') 'scale coupling NUMBER'
        write(66,'(A)') 'fwhm NUMBER'

    close(66)

end subroutine WRITE_EXAMPLE_INPUT

subroutine MAKE_OUTPUT_FILE
    USE version, only : git_version
    Use OMP_LIB
    implicit none
    character(len = 1000) :: CDUMMY, s
    integer               :: num_threads

    open(66, FILE = 'firms_sim.out', STATUS = 'UNKNOWN', POSITION = 'APPEND')

        write(66,'(A)')  '                            ┌─┐┬┬─┐┌┬┐┌─┐    ┌─┐┬┌┬┐                            '
        write(66,'(A)')  '                            ├┤ │├┬┘│││└─┐    └─┐││││                            '
        write(66,'(A)')  '                            └  ┴┴└─┴ ┴└─┘────└─┘┴┴ ┴                            '
        write(66,'(A)')  
        write(66,'(2A)') '                       Version : ', trim(git_version)
        write(66,'(A)')  '                                  Jon Kragskow                                  '
        write(66,'(A)')  '                                Nicholas Chilton                                '
        write(66,'(A)')  
 
        call print_date_and_time('STRT')
        ! Print number of threads
!$OMP PARALLEL
num_threads = omp_get_num_threads()
!$OMP END PARALLEL
        if (num_threads > 1) then
            s = 's'
        else
            s = ''
        end if

        write(CDUMMY,'(A, I0, 2A)') 'firms_sim is running on ', num_threads, ' thread',trim(adjustl(s))
        call warning(CDUMMY, 66)

#ifdef compilerdebug
        call warning('COMPILED IN DEBUG MODE',66)
        write(66,*)
#else
        write(66,*)
#endif

    close(66)


end subroutine MAKE_OUTPUT_FILE

subroutine COPY_INPUT_TO_OUTPUT(input_file)
    implicit none
    character(len = 500), intent(in) :: input_file
    character(len = 500)             :: line
    integer                          :: reason

    ! Open input file
    open(33, FILE = adjustl(trim(input_file)), STATUS = 'OLD')

    ! Open output file
    open(66, FILE = "firms_sim.out", STATUS = 'UNKNOWN', POSITION = 'APPEND')

    !Write header in output file
    write(66,'(A)') ''
    call SECTION_HEADER('Input File',66)
    write(66,'(A)') ''

    ! Loop through file and copy input to output file
    do
        READ(33,'(A)',IOSTAT = reason) line

        ! End of file
        if (reason < 0) then
            EXIT
        end if

        ! Write input to output if line is not blank
        call lowercase(line)
        if (len_trim(adjustl(line)) /= 0) write(66,'(A)') trim(adjustl(line))

    end do

    ! Close input file
    close(33)

    ! Close output file
    close(66)

end subroutine COPY_INPUT_TO_OUTPUT

pure subroutine LOWERCASE(str)
    ! Converts str to lowercase characters
    implicit none
    integer                           :: i,gap
    character(len = *), intent(inout) :: str
    
    gap = ICHAR('a')-ICHAR('A')
    if(len(str) > 0) then
        do i=1,len(str)
            if(str(i:i) <= 'Z') then
                if(str(i:i) >= 'A') str(i:i) = CHAR(ICHAR(str(i:i)) + gap)
            end if
        end do
    end if

end subroutine LOWERCASE

subroutine READ_INPUT_FILE(input_file, polynomials_file, mode_energies_file, n_coup_modes, &
                           n_modes, mode_numbers, n_vib_states, CFPs_file, field_lower,&
                           field_upper, field_step, zcw_val, single_axis, &
                           lower_e_bound, upper_e_bound, resolution, ion, ox_state, &
                           temperature, trans_treat, elec_scale, coupling_scale, &
                           ir_grid_size, mask_trans, nooef, new_cf_energies, fwhm, &
                           delec_mu_file, red_mass_file, user_field_vector, hdf5_method, hdf5_dim,&
                           calibration, mode_calibration)
    USE constants
    implicit none
    character(len = *), intent(in)      :: input_file
    character(len = *), intent(out)     :: mode_energies_file, polynomials_file, &
                                           CFPs_file, ion, single_axis, &
                                           trans_treat, delec_mu_file, red_mass_file
    character(len = 1000)               :: line, DUMMY, error_message, p_line
    character(len = 1000), allocatable  :: CDummy_array(:)
    real(kind = 8)                      :: RDUMMY
    real(kind = 8), intent(out)         :: field_lower, field_upper, field_step, lower_e_bound,&
                                           upper_e_bound, temperature, elec_scale, coupling_scale,&
                                           fwhm, user_field_vector(3), calibration(2)
    integer, intent(out)                :: n_coup_modes, n_vib_states, ox_state, &
                                           zcw_val, resolution, n_modes, ir_grid_size, hdf5_dim
    integer, intent(out), allocatable   :: mode_numbers(:)
    integer                             :: i, j, reason, num_entries, IDUMMY1, IDUMMY2
    LOGICAL, intent(out)                :: mask_trans, nooef, new_cf_energies, mode_calibration
    LOGICAL                             :: file_exists, hdf5_method
    LOGICAL, allocatable                :: modes_logical(:)

    ! Set defaults

    coupling_scale = 1.0_8
    elec_scale = 1.0_8
    mask_trans = .FALSE.
    nooef = .FALSE.
    new_cf_energies = .FALSE.
    mode_calibration = .FALSE.
    calibration(1) = 1.0_8
    calibration(2) = 0.0_8
    polynomials_file = "CFP_polynomials.dat"
    CFPs_file = "EQ_CFPs.dat"
    mode_energies_file = "mode_energies.dat"
    delec_mu_file = "dipole_derivatives.dat"
    red_mass_file = "reduced_masses.dat"
    trans_treat = "complex"
    ir_grid_size = 1
    single_axis = "no"
    fwhm = 2.0_8
    zcw_val = 5
    hdf5_method = .FALSE.
    hdf5_kramers = .FALSE.
    hdf5_dim = 0

    ! Placeholder defaults
    ion = "XX"
    ox_state = 99999
    n_modes = -1
    n_coup_modes = -1
    field_lower = -1.0_8
    field_upper = -1.0_8
    field_step = 0.0_8
    lower_e_bound  = -1
    upper_e_bound  = -1
    resolution     = -1
    temperature = -1
    n_vib_states = 2

    ! Check input file exists
    INQUIRE(FILE = trim(input_file), EXIST = file_exists)
    if (.NOT. file_exists ) then
        call PRINT_ERROR('Cannot find input file', trim(input_file))
        STOP
    end if

    ! Open input file and read data
    open(33, FILE = input_file, STATUS = 'OLD')

    ! Loop through file and read input sections
    do
        READ(33,'(A)',IOSTAT=reason) line

        ! Check file is not corrupted
        if (reason > 0)  then
            call PRINT_ERROR('Error reading input file','')
        ! Check if EOF has been reached
        else if (reason < 0) then
            EXIT
        else

            ! Skip blank lines
            if (len_trim(line) == 0) CYCLE

            ! Make all of line lowercase
            ! but first keep a copy with the correct case
            ! for filename inputs
            p_line = line
            call LOWERCASE(line)

            !  Skip comment lines
            if (GET_TRIMMED_VAL(line, 1, 1) == '!') CYCLE
            if (GET_TRIMMED_VAL(line, 1, 1) == '#') CYCLE
            if (GET_TRIMMED_VAL(line, 1, 1) == '*') CYCLE

            ! Ion type and oxidation state
            if (GET_TRIMMED_VAL(line, 1, 3) == 'ion') then
                if(hdf5_method) then
                    call PRINT_ERROR('Ion and hdf5 are mutually exclusive',&
                                         'Check input file')
                end if
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 2) then
                    READ(line,*, IOSTAT = reason) DUMMY, ion, ox_state

                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error reading Ion',&
                                         'Check input file')
                    end if
                else 
                    ! Write error message
                    call PRINT_ERROR('Error in ion specification',&
                                     'Check input file')
                end if
            end if

            !HDF5 input
            IF (GET_TRIMMED_VAL(line, 1, 4) == 'hdf5') THEN
                hdf5_method = .TRUE.
                if(trim(ion) /= "XX") then
                    call PRINT_ERROR('Ion and hdf5 are mutually exclusive',&
                                         'Check input file')
                end if
            ! Check correct number of inputs are present
                IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 2) THEN
                    READ(line,*, IOSTAT = reason) DUMMY, hdf5_dim, DUMMY

                ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error reading hdf5 command',&
                                         'Check input file')
                    else if (hdf5_dim < 2) then
                        call PRINT_ERROR('hdf5 dimension must be >= 2',&
                                         'Check input file')
                    end if
                    IF(TRIM(DUMMY(1:1)) == "k" .or. TRIM(DUMMY(1:1)) == "K") THEN
                        hdf5_kramers = .TRUE.
                    ELSE IF(TRIM(DUMMY(1:1)) == "n" .or. TRIM(DUMMY(1:1)) == "N") THEN
                        hdf5_kramers = .FALSE.
                    ELSE
                        CALL PRINT_ERROR('hdf5 command must specify "Kramers" or "Non-Kramers"',&
                                         'Check input file')
                    END IF
                ELSE
                ! Write error message
                    CALL PRINT_ERROR('Error in hdf5 specification',&
                                     'Check input file')
                END IF
            END IF

            ! Number of modes in molecule/system
            if (GET_TRIMMED_VAL(line, 1, 5) == 'modes') then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 1) then
                    READ(line,*, IOSTAT = reason) DUMMY, n_modes

                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error reading modes',&
                                         'Check input file')
                    end if           
                else 
                    ! Write error message
                    call PRINT_ERROR('Error in mode specification',&
                                     'Check input file')
                end if
            end if
    
            ! Modes to consider
            if (INDEX(trim(line),'use modes') /= 0) then
                line = line(10:)

                ! Replace spaces and commas from line to give numbers and ranges
                ! e.g. 10  12-15,  30-35 --> 10 12-15 30-35
                line = replace_space_comma(line)

                ! Read numbers from line and set modes logical True or False to match
                num_entries = count_char(trim(adjustl(line)), ' ') + 1
                allocate(CDummy_array(num_entries))
                ! Allocate array of True/False to include mode(s) in simulation
                allocate(modes_logical(mode_limit))
                modes_logical = .FALSE.
                READ(line, *, IOSTAT = reason) CDummy_array

                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error reading remove mode',&
                                         'Check input file')
                    end if


                do i = 1, num_entries

                        ! If a dash is present in the ith element, then remove it and read two numbers
                        if (count_char(trim(adjustl(CDummy_array(i))), '-') == 1) then
                            
                            CDummy_array(i) = replace_text(trim(adjustl(CDummy_array(i))), '-', ' ', count_char(trim(adjustl(line)), '-'))
                            READ(CDummy_array(i), *, IOSTAT = reason) IDUMMY1, IDUMMY2
                        
                            ! Check file is not corrupted or too short
                            if (reason > 0)  then
                                call PRINT_ERROR('Error reading remove mode',&
                                                 'Check input file')
                            end if

                            ! Set logicals
                            modes_logical(IDUMMY1:IDUMMY2) = .TRUE.

                        ! Else read a single number
                        else
                            READ(CDummy_array(i), *, IOSTAT = reason) IDUMMY1

                            ! Set logical
                            modes_logical(IDUMMY1) = .TRUE.
                        end if
                end do
                ! Count how many modes have been removed
                n_coup_modes = COUNT(modes_logical) 
                allocate(mode_numbers(n_coup_modes))
                deallocate(CDummy_array)
                j = 1
                do i = 1, mode_limit
                    if (modes_logical(i)) then
                        mode_numbers(j) = i
                        j = j + 1
                    end if
                end do
                deallocate(modes_logical)

                if(n_coup_modes > 1) mode_numbers = FLIPUD(mode_numbers)
            end if

            ! Read number of vibrational states
            if (INDEX(trim(line),'vib states') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 2) then
                    READ(line,*, IOSTAT = reason) DUMMY, DUMMY, n_vib_states
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in n_vib_states specification',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in n_vib_states specification',&
                                     'Check input file')
                end if
            end if

            ! Read fwhm value for simulation Gaussians
            if (INDEX(trim(line),'fwhm') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 1) then
                    READ(line,*, IOSTAT = reason) DUMMY, fwhm
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in FWHM specification',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in FWHM specification',&
                                     'Check input file')
                end if
            end if

            ! Read if there is a calibration to the mode energies
            if (INDEX(trim(line),'calibration') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 2) then
                    READ(line,*, IOSTAT = reason) DUMMY, calibration(1), calibration(2)
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in Calibration specification',&
                                         'Check input file')
                    end if

                else
                    ! Write error message
                    call PRINT_ERROR('Error in Calibration specification',&
                                     'Check input file')
                end if
                mode_calibration = .TRUE.
            end if

            ! Field and steps
            if (GET_TRIMMED_VAL(line, 1, 5) == 'field') then

                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 3) then
                    READ(line,*, IOSTAT = reason) DUMMY, field_lower, field_upper, field_step
                    
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error reading fields',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in fields specification',&
                                     'Check input file')
                end if

                ! Check step is non zero
                if (field_step == 0.0_8 .AND. field_upper - field_lower > 0.0_8) then
                    call PRINT_ERROR('Invalid field step value',&
                                     'Check input file')
                end if

                ! If B_max is smaller than B_min swap them
                if (field_upper < field_lower) then
                    RDUMMY = field_upper
                    field_upper = field_lower
                    field_lower = RDUMMY
                end if

                ! If B_min less than zero error out
                if (field_lower < 0.0_8) then
                    call PRINT_ERROR('Invalid field value',&
                                     'Check input file')
                end if

                ! Check field step is able to connect B_max and B_min
                ! If it is not then increase B_max so that it does

                if ((field_upper-field_lower) - (INT((field_upper-field_lower)/field_step) * field_step) > 0.0_8) then
                    write(6,'(A)')        '!!!!       Maximum field adjusted to comply with step size        !!!!'
                    write(6,'(A, F15.7, A)') '!!!!              B_max now = ', field_upper,' T             !!!!'
                end if
            end if

            ! Spectrum energies and steps
            if (GET_TRIMMED_VAL(line, 1, 6) == 'energy') then

                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 3) then
                    READ(line,*, IOSTAT = reason) DUMMY, lower_e_bound, upper_e_bound, resolution
                    
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error reading spectrum energies',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in spectrum energy specification',&
                                     'Check input file')
                end if

                ! Check resolution is non zero
                if (resolution == 0.0_8) then
                    call PRINT_ERROR('Invalid spectrum energy resolution',&
                                     'Check input file')
                end if

                ! If E_max is smaller than E_min swap them
                if (upper_e_bound < lower_e_bound) then
                    RDUMMY = upper_e_bound
                    upper_e_bound = lower_e_bound
                    lower_e_bound = RDUMMY
                end if

                ! If E_min less than zero error out
                if (lower_e_bound < 0.0_8) then
                    call PRINT_ERROR('Invalid spectrum energy value',&
                                     'Check input file')
                end if

            end if


            ! Read fwhm value for peaks
            if (INDEX(trim(line),'zcw') /= 0) then
                    ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 1) then
                    READ(line,*, IOSTAT = reason) DUMMY, zcw_val
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in ZCW specification',&
                                         'Check input file')
                    end if
                else 
                    ! Write error message
                    call PRINT_ERROR('Error in ZCW specification',&
                                     'Check input file')
                end if
            end if

            ! Magnetic field on single axis
            if (INDEX(trim(line), 'single axis') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 2) then
                    READ(line,*, IOSTAT = reason) DUMMY, DUMMY, single_axis
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in single axis specification',&
                                         'Check input file')
                    end if

                else if (CHECK_SPACES(trim(adjustl(line))) == 4) then
                    READ(line,*, IOSTAT = reason) DUMMY, DUMMY, user_field_vector
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in single axis specification',&
                                         'Check input file')
                    end if
                    single_axis = "yes"
                else
                    ! Write error message
                    call PRINT_ERROR('Error in single axis specification',&
                                     'Check input file')
                end if
                if(trim(single_axis) /= "x" .and. trim(single_axis) /= "y" .and. trim(single_axis) /= "z" .and. trim(single_axis) /= "yes") then
                    call PRINT_ERROR('Error in single axis specification',&
                                     'Check input file')
                end if
            end if

            ! Magnetic field on single axis
            if (INDEX(trim(line), 'temperature') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 1) then
                    READ(line,*, IOSTAT = reason) DUMMY, temperature
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in temperature specification',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in temperature specification',&
                                     'Check input file')
                end if
                if (temperature < 0.0_8) then
                    call PRINT_ERROR('Negative temperature specified',&
                                     'Check input file')
                end if
            end if


            ! Scale electronic transition probability
            if (INDEX(trim(line), 'scale electronic') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 2) then
                    READ(line,*, IOSTAT = reason) DUMMY, DUMMY, elec_scale
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in scale electronic specification',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in scale electronic specification',&
                                     'Check input file')
                end if
            end if


            ! Scale spin-phonon coupling values
            if (INDEX(trim(line), 'scale coupling') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 2) then
                    READ(line,*, IOSTAT = reason) DUMMY, DUMMY, coupling_scale
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in scale coupling specification',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in scale coupling specification',&
                                     'Check input file')
                end if
            end if                

            ! Equilibrium CFPs file name
            if (INDEX(trim(line), 'eq cfps file') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 3) then
                    READ(p_line,*, IOSTAT = reason) DUMMY, DUMMY, DUMMY, CFPs_file
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in eq cfps file specification',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in eq cfps file specification',&
                                     'Check input file')
                end if
            end if

            ! CFP polynomials file name
            if (INDEX(trim(line), 'polynomials file') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 2) then
                    READ(p_line,*, IOSTAT = reason) DUMMY, DUMMY, polynomials_file
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in polynomials file specification',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in polynomials file specification',&
                                     'Check input file')
                end if
            end if 

            ! Mode energies file name
            if (INDEX(trim(line), 'mode energies file') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 3) then
                    READ(p_line,*, IOSTAT = reason) DUMMY, DUMMY, DUMMY, mode_energies_file
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in mode energies file specification',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in mode energies file specification',&
                                     'Check input file')
                end if
            end if 

            ! Reduced masses file name
            if (INDEX(trim(line), 'reduced masses file') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 3) then
                    READ(p_line,*, IOSTAT = reason) DUMMY, DUMMY, DUMMY, red_mass_file
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in reduced masses file specification',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in reduced masses file specification',&
                                     'Check input file')
                end if
            end if 

            ! Dipole derivatives file name
            if (INDEX(trim(line), 'dipole derivatives file') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 3) then
                    READ(p_line,*, IOSTAT = reason) DUMMY, DUMMY, DUMMY, delec_mu_file
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in dipole derivatives file specification',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in dipole derivatives file specification',&
                                     'Check input file')
                end if
            end if

            ! Replace CF energies with those from file
            if (INDEX(trim(line),'replace cf energies') /= 0) then
                new_cf_energies = .TRUE.
            end if

            ! Mask off and skip purely electronic and vibrational transitions
            if (INDEX(trim(line),'mask transitions') /= 0) then
                mask_trans = .TRUE.
            end if

            ! If true, OEFs are susbsumed into eq cfps
            if (INDEX(trim(line),'nooef') /= 0) then
                nooef = .TRUE.
            end if

            ! ! Transition probability treatment
            ! if (INDEX(trim(line), 'transition approach') /= 0) then
            !     ! Check correct number of inputs are present
            !     if (CHECK_SPACES(trim(adjustl(line))) == 2) then
            !         READ(line,*, IOSTAT = reason) DUMMY, DUMMY, trans_treat
            !         ! Check file is not corrupted or too short
            !         if (reason > 0)  then
            !             call PRINT_ERROR('Error in transition approach specification',&
            !                              'Check input file')
            !         end if

            !     else 
            !         ! Write error message
            !         call PRINT_ERROR('Error in transition approach specification',&
            !                          'Check input file')
            !     end if
            ! end if

            ! CFP polynomials file name
            if (INDEX(trim(line), 'ir grid') /= 0) then
                ! Check correct number of inputs are present
                if (CHECK_SPACES(trim(adjustl(line))) == 2) then
                    READ(line,*, IOSTAT = reason) DUMMY, DUMMY, ir_grid_size
                    ! Check file is not corrupted or too short
                    if (reason > 0)  then
                        call PRINT_ERROR('Error in ir grid specification',&
                                         'Check input file')
                    end if

                else 
                    ! Write error message
                    call PRINT_ERROR('Error in ir grid specification',&
                                     'Check input file')
                end if
            end if

        end if
    end do

    close(33)

    ! Check for missing or non-physical input variables

    if (trim(adjustl(ion)) == "XX" .AND. .NOT. hdf5_method) then
        call PRINT_ERROR('No ion specified',&
                         'Check input file')
                         
    end if

    if (abs(ox_state) > 4 .AND. .NOT. hdf5_method .AND. trim(ion) /= "spin") then
        call PRINT_ERROR('Unsupported oxidation state/spin',&
                         'Check input file')
    end if

    if (temperature == -1) then
        call PRINT_ERROR('Temperature not specified',&
                         'Check input file')
    end if

    if (n_modes == -1) then
        call PRINT_ERROR('Number of modes not specified',&
                         'Check input file')
    end if

    if (n_coup_modes == -1) then
        call PRINT_ERROR('Coupling modes not specified',&
                         'Check input file')
    end if

    if (field_lower == -1 .OR. field_upper == -1 .OR. field_step == -1) then
        call PRINT_ERROR('Magnetic field not specified',&
                         'Check input file')
    end if

    if (lower_e_bound == -1 .OR. upper_e_bound == -1 .OR. resolution == -1) then
        call PRINT_ERROR('Simulation energy scale not specified',&
                         'Check input file')
    end if        


    ! Check for errors
    do i = 1, n_coup_modes
        if (mode_numbers(i) > n_modes) then
            write(error_message,'(A, I4, A)') 'Invalid mode index ', IDUMMY1,' given in remove modes'
            call PRINT_ERROR(error_message, 'Check input file')
        end if
    end do

    ! Check files exist
    INQUIRE(FILE = trim(CFPs_file), EXIST = file_exists)
    if (.NOT. file_exists .AND. .NOT. hdf5_method) then
        call PRINT_ERROR('Cannot find '//trim(CFPs_file), 'Check input file')
        STOP
    end if

    INQUIRE(FILE = trim(polynomials_file), EXIST = file_exists)
    if (.NOT. file_exists .AND. .NOT. hdf5_method) then
        call PRINT_ERROR('Cannot find '//trim(polynomials_file), 'Check input file')
        STOP
    end if
    
    INQUIRE(FILE = trim(mode_energies_file), EXIST = file_exists)
    if (.NOT. file_exists ) then
        call PRINT_ERROR('Cannot find '//trim(mode_energies_file), 'Check input file')
        STOP
    end if

    INQUIRE(FILE = "tau.hdf5", EXIST = file_exists)
    if (hdf5_method .AND. .NOT. file_exists) then
        call PRINT_ERROR('Cannot find tau.hdf5', 'Check input file')
        STOP
    end if

    if (new_cf_energies) then
        INQUIRE(FILE = 'eq_cf_energies.dat', EXIST = file_exists)
        if (.NOT. file_exists ) then
            call PRINT_ERROR('Cannot find eq_cf_energies.dat', 'Check input file')
            STOP
        end if
    end if

    if (trans_treat /= 'simple') then
        INQUIRE(FILE = red_mass_file, EXIST = file_exists)
        if (.NOT. file_exists ) then
            call PRINT_ERROR('Cannot find '//trim(red_mass_file), 'Check input file')
            STOP
        end if
        INQUIRE(FILE = delec_mu_file, EXIST = file_exists)
        if (.NOT. file_exists ) then
            call PRINT_ERROR('Cannot find '//trim(delec_mu_file), 'Check input file')
            STOP
        end if
    end if

end subroutine READ_INPUT_FILE


subroutine ELECTRONIC_OPTIONS(ion, OEF, ox_state, elec_dim, gJ, J)
    ! Sets OEFs, elec_dim, and gJ using ion name and oxidation state
    implicit none
    real(kind = 8), intent(out)                 :: OEF(3)
    character(len = *), intent(in)              :: ion
    integer, intent(in)                         :: ox_state
    integer, intent(out)                        :: elec_dim
    real(kind = 8)                              :: S, L
    real(kind = 8), intent(out)                 :: gJ, J
    integer                                     :: num_f_elec

    ! Open output file
    open(66, FILE = 'firms_sim.out', STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Write ion data section header
    write(66,'(A)') ''

    IF(ion /= "spin") THEN
        call SECTION_HEADER('Metal Ion Data', 66)
        write(66,'(A)') ''

        ! Write ion symbol
        write(66,'(3A,I0,A)') 'Ion = ',trim(ion),'(',ox_state,')'

        ! Calculate number of f electrons
        if (ion == 'ce') num_f_elec =  5
        if (ion == 'pr') num_f_elec =  6
        if (ion == 'nd') num_f_elec =  7
        if (ion == 'sm') num_f_elec =  8
        if (ion == 'eu') num_f_elec =  9
        if (ion == 'gd') num_f_elec = 10
        if (ion == 'tb') num_f_elec = 11
        if (ion == 'dy') num_f_elec = 12
        if (ion == 'ho') num_f_elec = 13
        if (ion == 'er') num_f_elec = 14
        if (ion == 'tm') num_f_elec = 15
        if (ion == 'yb') num_f_elec = 16

        num_f_elec = num_f_elec - ox_state

        ! Write f electron count
        write(66,'(I0, A)') num_f_elec, ' f electrons'

        ! Calculate S
        if (num_f_elec == 7 .OR. num_f_elec == 14) then
            call PRINT_ERROR('No orbital moment! ','')
        else if (num_f_elec < 7) then
            S = real(num_f_elec, 8) / 2.0_8
        else if (num_f_elec > 7) then
            S = (14.0_8 - num_f_elec)  / 2.0_8
        end if

        ! Write S to output file
        if (S - 0.5_8 == INT(S)) then ! Non Integer S
            write(66,'(A, I0, A)') 'S = ', INT(S*2.0_8),'/2'
        else
            write(66,'(A, I0)') 'S = ', INT(S) ! Integer S
        end if

        ! Calculate L
        if (num_f_elec == 1 .OR. num_f_elec == 6 .OR. num_f_elec == 8 .OR. num_f_elec == 13) then
            L = 3.0_8
        else if (num_f_elec == 2 .OR. num_f_elec == 5 .OR. num_f_elec == 9 .OR. num_f_elec == 12) then
            L = 5.0_8
        else if (num_f_elec == 3 .OR. num_f_elec == 4 .OR. num_f_elec == 10 .OR. num_f_elec == 11) then
            L = 6.0_8
        end if

        ! Write L to output file
        write(66,'(A, I0)') 'L = ', INT(L)

        ! Calculate J
        if (num_f_elec < 7) then
            J = ABS(L - S)
        else if (num_f_elec > 7) then
            J = L + S
        end if

        ! Write J to output file
        if (INT(J) == INT(J*2.0_8)/2) then ! Non Integer J
            write(66,'(A, I0, A)') 'J = ', INT(J*2.0_8),'/2'
        else
            write(66,'(A, I0)') 'J = ', INT(J) ! Integer J
        end if

        ! Calculate elec_dim
        elec_dim = NINT(2.0_8 * J) + 1

        ! Calculate gJ (Lande g factor) for a given L, S, J
        gJ = 1.5_8 + (S*(S + 1.0_8) - L*(L + 1.0_8))/(2.0_8*J*(J + 1.0_8))

        ! Write gJ to output file
        write(66,'(A, F7.5)') 'gJ = ', gJ
        write(66,*)

        ! Set operator equivalent factors
        if (ion == 'ce' .AND. ox_state == 3) then  
            OEF(1) =    -2.0_8/35.0_8
            OEF(2) =    2.0_8/315.0_8
            OEF(3) =    0.0_8
        else if (ion == 'pr' .AND. ox_state == 3) then  
            OEF(1) =    -52.0_8/2475.0_8
            OEF(2) =    -4.0_8/5445.0_8
            OEF(3) =    272.0_8/4459455.0_8
        else if (ion == 'nd' .AND. ox_state == 3) then  
            OEF(1) =    -7.0_8/1089.0_8
            OEF(2) =    -136.0_8/467181.0_8
            OEF(3) =    -1615.0_8/42513471.0_8
        else if (ion == 'pm' .AND. ox_state == 3) then  
            OEF(1) =    14.0_8/1815.0_8
            OEF(2) =    952.0_8/2335905.0_8
            OEF(3) =    2584.0_8/42513471.0_8
        else if (ion == 'sm' .AND. ox_state == 3) then  
            OEF(1) =    13.0_8/315.0_8
            OEF(2) =    26.0_8/10395.0_8
            OEF(3) =    0.0_8
        else if (ion == 'eu' .AND. ox_state == 3) then  
            call PRINT_ERROR('Eu3+ has J=0 and hence no mJ states','')
        else if (ion == 'gd' .AND. ox_state == 3) then  
            call PRINT_ERROR('Gd3+ has no orbital angular momentum','')
        else if (ion == 'tb' .AND. ox_state == 3) then  
            OEF(1) =    -1.0_8/99.0_8
            OEF(2) =    2.0_8/16335.0_8
            OEF(3) =    -1.0_8/891891.0_8
        else if (ion == 'dy' .AND. ox_state == 3) then  
            OEF(1) =    -2.0_8/315.0_8
            OEF(2) =    -8.0_8/135135.0_8
            OEF(3) =    4.0_8/3864861.0_8
        else if (ion == 'ho' .AND. ox_state == 3) then  
            OEF(1) =    -1.0_8/450.0_8
            OEF(2) =    -1.0_8/30030.0_8
            OEF(3) =    -5.0_8/3864861.0_8
        else if (ion == 'er' .AND. ox_state == 3) then  
            OEF(1) =    4.0_8/1575.0_8
            OEF(2) =    2.0_8/45045.0_8
            OEF(3) =    8.0_8/3864861.0_8
        else if (ion == 'tm' .AND. ox_state == 3) then  
            OEF(1) =    1.0_8/99.0_8
            OEF(2) =    8.0_8/49005.0_8
            OEF(3) =    -5.0_8/891891.0_8
        else if (ion == 'yb' .AND. ox_state == 3) then  
            OEF(1) =    2.0_8/63.0_8
            OEF(2) =    -2.0_8/1155.0_8
            OEF(3) =    4.0_8/27027.0_8
        else
            call PRINT_ERROR('!!!!                Atom and oxidation state not supported!                 !!!!','')
        end if
    ELSE
        gJ = 2.0_8
        J = ox_state*0.5_8
        elec_dim = ox_state+1
        OEF(:) = 1.0_8
    END IF

    ! Close output file
    close(66)

end subroutine ELECTRONIC_OPTIONS

subroutine PHONON_OPTIONS(n_coup_modes, mode_energies, mode_energies_file, &
                          n_modes, n_vib_states, pho_dim, calibration, mode_calibration)
    implicit none
    integer, intent(in)                      :: n_coup_modes, n_modes, n_vib_states
    integer, intent(out)                     :: pho_dim
    integer                                  :: j
    real(kind = 8), intent(out), allocatable :: mode_energies(:)
    real(kind = 8), intent(in)               :: calibration(2)
    character(len = *), intent(in)           :: mode_energies_file
    logical, intent(in)                      :: mode_calibration

    ! e.g. If n_vib_states == 2 then n=0 and n=1 states used

    pho_dim = n_vib_states ** n_coup_modes

    ! Allocate and zero mode_energies array
    allocate(mode_energies(n_modes))
    mode_energies = 0.0_8

    ! Read mode energies
    open(33, FILE = mode_energies_file, STATUS = 'OLD')
        !skip energy header
        READ(33,*)

        do j = 1, n_modes
            READ(33,*) mode_energies(j)
        end do

    close(33)

    if(mode_calibration) mode_energies = calibration(1)*mode_energies + calibration(2)

end subroutine PHONON_OPTIONS

subroutine CALC_G_WIDTH(FWHM, g_width, section)
    implicit none
    real(kind = 8)                 :: FWHM
    real(kind = 8), intent(out)    :: g_width
    character(len = *)              :: section

! Open output file
    open(66, FILE = "firms_sim.out", STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Calculate Gaussian width using FWHM in cm-1
! $\sigma = \frac{\text{FWHM}}{2\sqrt{2\log(2)}}$
    g_width = FWHM/(2.0_8*sqrt(2.0_8*log(2.0_8)))

! Write FWHM info to output file
    write(66,'(A)') ''
    call SECTION_HEADER(trim(section),66)

    write(66,*)
    write(66,'(A, F8.3, A)') 'FWHM            = ', FWHM, ' cm^-1'
    write(66,'(A, F8.3, A)') 'Gaussian Width  = ', g_width, ' cm^-1'

! Close output file
    close(66)
end subroutine CALC_G_WIDTH

subroutine COUPLING_OPTIONS(pho_dim, elec_dim, coup_dim)
    implicit none
    integer, intent(in)  :: pho_dim, elec_dim
                            
    integer, intent(out) :: coup_dim

    if (pho_dim == 0) then
        coup_dim = elec_dim
    else
        coup_dim = elec_dim * pho_dim
    end if

end subroutine COUPLING_OPTIONS

subroutine FIELD_OPTIONS(field_lower, field_upper, field_step, field_vecs, field_angs, &
                         field_strengths, n_field_strengths, n_field_vecs, &
                         zcw_val, single_axis, ir_grid_size, user_field_vector)
    use constants
    real(kind = 8), intent(in)               :: field_lower, field_upper, field_step, user_field_vector(3)
    real(kind = 8), intent(out), allocatable :: field_strengths(:), field_vecs(:,:,:), &
                                                field_angs(:,:)
    real(kind = 8), allocatable              :: raw_vecs(:,:)
    integer, intent(out)                     :: n_field_vecs, n_field_strengths
    integer, intent(in)                      :: zcw_val, ir_grid_size
    integer                                  :: i
    character(len = *)                       :: single_axis

    ! Calculate field strengths using, upper, lower and step values
    
    if (field_upper == field_lower) then
        n_field_strengths = 1
    else 
        n_field_strengths = 1 + INT((field_upper - field_lower) / field_step)
    end if

    allocate(field_strengths(n_field_strengths))
    field_strengths = 0.0_8

    field_strengths(1) = field_lower

    if (n_field_strengths > 1) then
        do i = 2, n_field_strengths
            field_strengths(i) = field_strengths(i - 1) + field_step
            ! Manually set zero field to small field
            if (field_strengths(i) == 0.0_8) then
                field_strengths(i) = 0.00002
            end if
        end do
    end if

    ! Set field vectors to one direction
    if (trim(single_axis) == 'x') then
        allocate(raw_vecs(1,3), field_angs(1,2))
        field_angs(1,:)     = [0.0_8, pi/2]
        raw_vecs(1,:)       = [1.0_8, 0.0_8, 0.0_8]
        n_field_vecs      = 1
    else if (trim(single_axis) == 'y') then
        allocate(raw_vecs(1,3), field_angs(1,2))
        field_angs(1,:)     = [pi/2, pi/2]
        raw_vecs(1,:)       = [0.0_8, 1.0_8, 0.0_8]
        n_field_vecs      = 1
    else if (trim(single_axis) == 'z') then
        allocate(raw_vecs(1,3), field_angs(1,2))
        field_angs(1,:)     = [0.0_8, 0.0_8]
        raw_vecs(1,:)       = [0.0_8, 0.0_8, 1.0_8]
        n_field_vecs      = 1
    else if (trim(single_axis) == 'yes') then
        allocate(raw_vecs(1,3), field_angs(1,2))
        field_angs(1,:)     = [atan2(user_field_vector(2),user_field_vector(1)), acos(user_field_vector(3)/sqrt(user_field_vector(1)**2+user_field_vector(2)*2+user_field_vector(3)**2))]
        raw_vecs(1,:)       = user_field_vector
        n_field_vecs      = 1
    else
        ! or calculate field vectors for averaging using ZCW scheme
        call ZCW(zcw_val, raw_vecs, n_field_vecs, field_angs)
    end if

    ! Apply raw_vecs to field strengths to give field_vecs
    allocate(field_vecs(n_field_strengths, n_field_vecs, 3))
    field_vecs = 0.0_8
    do i = 1, n_field_strengths
        field_vecs(i,:,:) = raw_vecs * field_strengths(i)
    end do

    ! Open output file
    open(66, FILE = 'firms_sim.out', STATUS = 'UNKNOWN', POSITION = 'APPEND')

        ! Write equilbrium electronic structure section header
        write(66,'(A)') ''
        call SECTION_HEADER('Magnetic Field Information', 66)

        write(66,'(A)') ''   
        call SUB_SECTION_HEADER('Magnetic Field Strengths (T)', 66)


        do i = 1, n_field_strengths
            write(66,'(F8.4)') field_strengths(i)
        end do
        write(66,'(A)') ''   


        write(66,'(A,I0,A)') 'Each field strength is powder-averaged over ', n_field_vecs, ' positions'
        write(66,'(A)') ''   


    close(66)


end subroutine FIELD_OPTIONS

subroutine ZCW(m, VecDir, NumDir, angles)
    use constants
    implicit none
    integer                                   :: i, c(3)
    integer, intent(in)                       :: m
    integer, intent(out)                      :: NumDir
    integer, allocatable                      :: g(:)
    real(kind = 8), allocatable, intent(out)  :: VecDir(:,:), angles(:,:)
    real(kind = 8)                            :: a, b
    
    allocate(g(m+3))
    c(1) = -1
    c(2) = 1
    c(3) = 1
    g(1) = 8
    g(2) = 13

    do i=3,m+3
        g(i) = g(i-1) + g(i-2)
    end do

    NumDir = g(m+3)

    allocate(VecDir(NumDir,3), angles(NumDir,2))

    do i=1,NumDir
        a = (2.0_8*pi/real(c(3),8))*mod(real(i-1,8)*real(g(m+1),8)/NumDir,1.0_8)
        b = acos(real(c(1),8)*((real(c(2),8)*mod(real(i-1,8)/NumDir,1.0_8))-1.0_8))
        angles(i,1) = a
        angles(i,2) = b
        VecDir(i,1) = sin(b)*cos(a)
        VecDir(i,2) = sin(b)*sin(a)
        VecDir(i,3) = cos(b)
    end do

    deallocate(g)

end subroutine ZCW


subroutine CALC_EQ_CF_HAMILTONIAN(CFP_file, elec_dim, EQ_H_CF, CF_vecs, CF_vals, gJ, &
                                  single_axis, nooef, OEF, new_cf_energies, B_quant)
    USE stev_ops
    implicit none
    character(len = *), intent(in)              :: CFP_file, single_axis
    integer, intent(in)                         :: elec_dim
    complex(kind = 8), intent(out), allocatable :: EQ_H_CF(:,:)
    real(kind = 8), intent(in)                  :: gJ, OEF(3)
    real(kind = 8), intent(out)                 :: B_quant
    real(kind = 8), allocatable                 :: CFPs(:,:), CF_vals(:), Gvec(:,:), Gval(:)
    complex(kind = 8), allocatable              :: stevens_ops(:,:,:,:), CF_vecs(:,:), H_ZEE(:,:), Zee(:,:,:)
    LOGICAL, intent(in)                         :: nooef, new_cf_energies
    REAL(KIND = 8)                              :: Gmat(3,3),quant_field(3)
    integer                                     :: k,l

    ! Allocate CFPs array
    allocate(CFPs(3, 13))
    CFPs = 0.0_8

    ! Read in Equilibrium CFPs
    call READ_CFPs(CFP_file, CFPs)

    ! Calculate stevens operators
    call GET_STEVENS_OPERATORS(elec_dim - 1, stevens_ops)

    ! Allocate and zero Hamiltonian array
    allocate(EQ_H_CF(elec_dim, elec_dim), H_ZEE(elec_dim, elec_dim))
    EQ_H_CF = (0.0_8, 0.0_8)
    H_ZEE = (0.0_8, 0.0_8)

    ! Add small Zeeman perturbation to quantise kd_percents
    B_quant = 0.000001_8 ! Tesla

    if (single_axis == 'x') then
        call CALC_H_ZEE(B_quant, 0.0_8, 0.0_8, gJ, H_ZEE)
    else if (single_axis == 'y') then
        call CALC_H_ZEE(0.0_8, B_quant, 0.0_8, gJ, H_ZEE)
    else if (single_axis == 'z') then
        call CALC_H_ZEE(0.0_8, 0.0_8, B_quant, gJ, H_ZEE)
    else if (trim(single_axis) == 'yes') then
        quant_field = user_field_vector/sqrt(user_field_vector(1)**2 + user_field_vector(2)**2 + user_field_vector(3)**2)
        call CALC_H_ZEE(quant_field(1), quant_field(2), quant_field(3), gJ, H_ZEE)
    else
        ALLOCATE(Zee(3,elec_dim,elec_dim))
        call CALC_H_ZEE(1.0_8, 0.0_8, 0.0_8, gJ, Zee(1,:,:))
        call CALC_H_ZEE(0.0_8, 1.0_8, 0.0_8, gJ, Zee(2,:,:))
        call CALC_H_ZEE(0.0_8, 0.0_8, 1.0_8, gJ, Zee(3,:,:))
        DO k = 1,3
            DO l = 1,3
                Gmat(k,l) = 2.0_8*real(Zee(k,1,2)*Zee(l,2,1) &
                                      + Zee(k,2,1)*Zee(l,1,2) &
                                      + Zee(k,1,1)*Zee(l,1,1) &
                                      + Zee(k,2,2)*Zee(l,2,2),8)
            END DO
        END DO
        call DIAGONALISE('U',Gmat,Gvec,Gval)
        call CALC_H_ZEE(B_quant*Gvec(1,maxloc(Gval,1)), &
                        B_quant*Gvec(2,maxloc(Gval,1)), &
                        B_quant*Gvec(3,maxloc(Gval,1)), gJ, H_ZEE)
        DEALLOCATE(Zee)
    end if

    ! Calculate CF Hamiltonian 
    if (nooef) then ! OEFs subsumed into parameters
        EQ_H_CF = H_CF(CFPs, stevens_ops, 6) + H_ZEE
    else ! OEFs must be included explicitly
        EQ_H_CF = H_CF(CFPs, stevens_ops, OEF, 6) + H_ZEE
    end if

    ! Allocate arrays for eigenvectors and eigenvalues
    allocate(CF_vecs(elec_dim, elec_dim), CF_vals(elec_dim))
    CF_vecs  = (0.0_8, 0.0_8)
    CF_vals  = 0.0_8

    ! Diagonalise CF Hamiltonian
    call DIAGONALISE('U',EQ_H_CF,CF_vecs,CF_vals)

    ! Set lowest eigenvalue to zero
    CF_vals = CF_vals - CF_vals(1)

    if (new_cf_energies) then
        call READ_CF_SHIFT(CF_vals)
        ! Add on small zeeman perturbation to avoid degeneracies
        CF_vals = CF_vals + real(DIAG(H_ZEE), 8)
    end if

end subroutine CALC_EQ_CF_HAMILTONIAN

subroutine CALC_EQ_CF_HAMILTONIAN_HDF5(hdf5_dim, elec_dim, EQ_H_CF, CF_vecs, CF_vals, single_axis, B_quant)
    USE stev_ops
    USE constants
    implicit none
    character(len = *), intent(in)              :: single_axis
    integer, intent(in)                         :: elec_dim, hdf5_dim
    complex(kind = 8), intent(out), allocatable :: EQ_H_CF(:,:)
    real(kind = 8), intent(out)                 :: B_quant
    real(kind = 8), allocatable                 :: Zero_field_EQ_evals(:), CF_vals(:), Gvec(:,:),Gval(:)
    complex(kind = 8), allocatable              :: CF_vecs(:,:), H_ZEE(:,:),SpinMat(:,:,:),AngMomMat(:,:,:)
    real(kind = 8)                              :: quant_field(3),Gmat(3,3)
    integer                                     :: i,k,l

    ! Allocate and zero Hamiltonian array
    allocate(EQ_H_CF(hdf5_dim, hdf5_dim), H_ZEE(hdf5_dim, hdf5_dim))
    EQ_H_CF = (0.0_8, 0.0_8)
    H_ZEE = (0.0_8, 0.0_8)

    ! Read in Equilibrium Hamiltonian
    CALL READ_EQ_HAMILTONIAN_HDF5(hdf5_dim,Zero_field_EQ_evals)
    CALL READ_ANGMOM_HDF5(hdf5_dim,SpinMat,AngMomMat)

    ! Add small Zeeman perturbation to quantise kd_percents
    B_quant = 0.000001_8 ! Tesla
    quant_field = 0.0_8
    if (trim(single_axis) == 'x') then
        quant_field(1) = B_quant
    else if (trim(single_axis) == 'y') then
        quant_field(2) = B_quant
    else if (trim(single_axis) == 'z') then
        quant_field(3) = B_quant
    else if (trim(single_axis) == 'yes') then
        quant_field = B_quant*user_field_vector/sqrt(user_field_vector(1)**2 + user_field_vector(2)**2 + user_field_vector(3)**2)
    else
        DO k = 1,3
            DO l = 1,3
                Gmat(k,l) = 2.0_8*real((AngMomMat(k,1,2) + ge*SpinMat(k,1,2))*(AngMomMat(l,2,1) + ge*SpinMat(l,2,1)) &
                                      + (AngMomMat(k,2,1) + ge*SpinMat(k,2,1))*(AngMomMat(l,1,2) + ge*SpinMat(l,1,2)) &
                                      + (AngMomMat(k,1,1) + ge*SpinMat(k,1,1))*(AngMomMat(l,1,1) + ge*SpinMat(l,1,1)) &
                                      + (AngMomMat(k,2,2) + ge*SpinMat(k,2,2))*(AngMomMat(l,2,2) + ge*SpinMat(l,2,2)),8)
            END DO
        END DO
        call DIAGONALISE('U',Gmat,Gvec,Gval)
        quant_field(:) = B_quant*Gvec(:,maxloc(Gval,1))
        open(66, FILE = 'firms_sim.out', STATUS = 'UNKNOWN', POSITION = 'APPEND')
            write(66,'(A,3F8.4)') 'Quantisation axis for hdf5 input is:', Gvec(:,maxloc(Gval,1))
        close(66)
    end if

    ! Calculate CF Hamiltonian
    DO i = 1,hdf5_dim
        EQ_H_CF(i,i) = Zero_field_EQ_evals(i)
    END DO
    H_ZEE = muB*(quant_field(1)*(AngMomMat(1,:,:) + ge*SpinMat(1,:,:)) &
                        + quant_field(2)*(AngMomMat(2,:,:) + ge*SpinMat(2,:,:)) &
                        + quant_field(3)*(AngMomMat(3,:,:) + ge*SpinMat(3,:,:)) &
                        )
    EQ_H_CF = EQ_H_CF + H_ZEE

    ! Allocate arrays for eigenvectors and eigenvalues
    allocate(CF_vecs(hdf5_dim, hdf5_dim), CF_vals(hdf5_dim))
    CF_vecs  = (0.0_8, 0.0_8)
    CF_vals  = 0.0_8

    ! Diagonalise CF Hamiltonian
    call DIAGONALISE('U',EQ_H_CF,CF_vecs,CF_vals)

    ! Set lowest eigenvalue to zero
    CF_vals = CF_vals - CF_vals(1)

end subroutine CALC_EQ_CF_HAMILTONIAN_HDF5

subroutine READ_CF_SHIFT(CF_vals)
    implicit none
    real(kind = 8), intent(out) :: CF_vals(:)
    integer                     :: i

    open(33, FILE = 'eq_cf_energies.dat', STATUS='UNKNOWN')

        do i = 1, SIZE(CF_vals)
            READ(33, *) CF_vals(i)
        end do

    close(33)

end subroutine READ_CF_SHIFT

subroutine READ_CFPs(CFP_file, CFPs)
    implicit none
    real(kind = 8), intent(out)    :: CFPs(:,:)
    character(len = *), intent(in) :: CFP_file
    real(kind=8)                   :: DUMMY
    integer                        :: i
    LOGICAL                        :: PHI

    ! Reads in crystal field parameters from file
    ! can be as a list of parameters or in phi input style
    ! i.e 1 k q param

    ! Check if first line contains a 1 on its own, if it does then read using phi input style
    open(33, FILE = trim(CFP_file), STATUS = 'old')

        READ(33,*) DUMMY

        if (DUMMY == 1.0_8) then
            PHI = .TRUE.
        else
            PHI = .FALSE.
        end if

        REWIND(33)

        if (PHI) then 

            do i = 1, 5
                READ(33, *) DUMMY, DUMMY, DUMMY, CFPs(1,i)
            end do

            do i = 1, 9
                READ(33, *) DUMMY, DUMMY, DUMMY, CFPs(2,i)
            end do

            do i = 1, 13
                READ(33, *) DUMMY, DUMMY, DUMMY, CFPs(3,i)
            end do

        else

            do i = 1, 5
                READ(33, *) CFPs(1,i)
            end do

            do i = 1, 9
                READ(33, *) CFPs(2,i)
            end do

            do i = 1, 13
                READ(33, *) CFPs(3,i)
            end do

        end if

    close(33)

end subroutine READ_CFPs

subroutine CALC_EQ_PHO_HAMILTONIAN(n_coup_modes, mode_energies, mode_numbers, &
                                   n_vib_states, pho_dim, EQ_H_pho_summed)
    USE matrix_tools
    ! Calculates equilibrium phonon Hamiltonian
    ! where all modes are coupled together
    implicit none
    integer, intent(in)                       :: n_coup_modes, n_vib_states, pho_dim, mode_numbers(:)
    integer                                   :: j, n
    real(kind = 8), intent(in)                :: mode_energies(:)
    real(kind = 8), allocatable               :: EQ_H_pho(:,:,:)
    real(kind = 8), intent(out), allocatable  :: EQ_H_pho_summed(:,:)


    ! Allocate and zero phonon Hamiltonian and temporary array for uncoupled Hamiltonians
    allocate(EQ_H_pho(n_coup_modes, n_vib_states, n_vib_states),&
             EQ_H_pho_summed(pho_dim, pho_dim))
    EQ_H_pho = 0.0_8
    EQ_H_pho_summed = 0.0_8

    ! Calculate phonon Hamiltonian
    ! Diagonal matrix whose entries are the energies of each state

    do j = 1, n_coup_modes
        do n = 1, n_vib_states
            EQ_H_pho(j, n, n) = (real(n-1, 8)+0.5_8) * mode_energies(mode_numbers(j))
        end do
    end do

    if (n_coup_modes > 1) then
        call COUPLE_MODE_ARRAY(EQ_H_pho, EQ_H_pho_summed, n_coup_modes, n_vib_states)
    else
        EQ_H_pho_summed = EQ_H_pho(1,:,:)
    end if

    deallocate(EQ_H_pho)

end subroutine CALC_EQ_PHO_HAMILTONIAN

subroutine COUPLE_MODE_ARRAY(matrix, coupled_matrix, n_coup_modes, n_vib_states)
    ! Couples together multiple real phonon operators using kronecker product
    ! with identity. 
    ! Parameters:
    ! matrix :
    !   Single phonon operator - different for each phonon
    ! coupled_matrix
    !   n_coup_modes number of phonon operators coupled together with
    !   kronecker product
    ! n_coup_modes
    !   Number of coupled modes
    ! n_vib_states
    !   Number of vibrational states, assumed to be the same for each mode
    USE matrix_tools
    implicit none
    real(kind = 8), intent(in)   :: matrix(:,:,:)
    real(kind = 8), intent(out)  :: coupled_matrix(:,:)
    integer, intent(in)          :: n_coup_modes, n_vib_states
    integer                      :: ident_right_size, ident_left_size, j
    integer, allocatable         :: lhs(:), rhs(:)

    ! Arrays to keep track of size of identity arrays kroneckered either side of a given Hamiltonian
    allocate(lhs(n_coup_modes))
    lhs  = [(j, j = 0, n_coup_modes - 1)]

    allocate(rhs(n_coup_modes))
    rhs  = [(j, j = n_coup_modes - 1, 0, -1)]

    do j = 1, n_coup_modes
        ident_right_size = 0
        ident_left_size  = 0

        ! Create left and right identities
        ! If first or last mode then no need for left or right identities
        if (j /= 1) then
            ident_left_size  = n_vib_states ** lhs(j)
        end if
        if (j /= n_coup_modes) then
            ident_right_size = n_vib_states ** rhs(j)
        end if

        ! First mode then kronecker from right with phonon identity
        if (j == 1) then
            coupled_matrix = coupled_matrix + KRON_PROD(matrix(j,:,:), IDENTITY(ident_right_size))

        ! Last mode then kronecker from left with phonon identity, but nothing from right
        else if (j == n_coup_modes) then
            coupled_matrix = coupled_matrix + KRON_PROD(IDENTITY(ident_left_size), matrix(j,:,:))

        ! All other mode_numbers kronecker from left with  phonon identities
        ! and from right with remaining phonon identities
        else
            coupled_matrix = coupled_matrix + KRON_PROD(KRON_PROD(IDENTITY(ident_left_size), matrix(j,:,:)), IDENTITY(ident_right_size))
        end if
    end do
end subroutine COUPLE_MODE_ARRAY

subroutine COUPLE_COMPLEX_MODE_ARRAY(matrix, coupled_matrix, n_coup_modes, &
                                     n_vib_states)
    ! Couples together multiple complex phonon Hamiltonians using kronecker product
    ! with identity
    ! Parameters:
    ! matrix :
    !   Single phonon Hamiltonian - SAME for each mode
    ! coupled_matrix
    !   n_coup_modes number of phonon Hamiltonians coupled together with
    !   kronecker product
    ! n_coup_modes
    !   Number of coupled modes
    ! n_vib_states
    !   Number of vibrational states, assumed to be the same for each mode
    USE matrix_tools
    implicit none
    complex(kind = 8), intent(in)   :: matrix(:,:)
    complex(kind = 8), intent(out)  :: coupled_matrix(:,:,:)
    integer, intent(in)          :: n_coup_modes, n_vib_states
    integer                      :: ident_right_size, ident_left_size, j
    integer, allocatable         :: lhs(:), rhs(:)

    ! Arrays to keep track of size of identity arrays kroneckered either side
    ! of a given Hamiltonian
    allocate(lhs(n_coup_modes))
    lhs  = [(j, j = 0, n_coup_modes - 1)]

    allocate(rhs(n_coup_modes))
    rhs  = [(j, j = n_coup_modes - 1, 0, -1)]

    do j = 1, n_coup_modes
        ident_right_size = 0
        ident_left_size  = 0

        ! Create left and right identities
        ! If first or last mode then no need for left or right identities
        if (j /= 1) then
            ident_left_size  = n_vib_states ** lhs(j)
        end if
        if (j /= n_coup_modes) then
            ident_right_size = n_vib_states ** rhs(j)
        end if

        ! First mode then kronecker from right with phonon identity
        if (j == 1) then
            coupled_matrix(j,:,:) = KRON_PROD(matrix, IDENTITY(ident_right_size))

        ! Last mode then kronecker from left with phonon identity, but nothing from right
        else if (j == n_coup_modes) then
            coupled_matrix(j,:,:) = KRON_PROD(IDENTITY(ident_left_size), matrix)

        ! All other mode_numbers kronecker from left phonon identities
        ! and from right with remaining phonon identities
        else
            coupled_matrix(j,:,:) = KRON_PROD(KRON_PROD(IDENTITY(ident_left_size), matrix), IDENTITY(ident_right_size))
        end if
    end do
end subroutine COUPLE_COMPLEX_MODE_ARRAY

subroutine COUPLE_INTEGER_MODE_ARRAY(matrix, coupled_matrix, n_coup_modes, n_vib_states)
    USE matrix_tools
    implicit none
    integer, intent(in)   :: matrix(:,:,:)
    integer, intent(out)  :: coupled_matrix(:,:,:)
    integer, intent(in)   :: n_coup_modes, n_vib_states
    integer               :: ident_right_size, ident_left_size, j
    integer, allocatable  :: lhs(:), rhs(:)

    ! Arrays to keep track of size of I_IDENTITY arrays kroneckered either side of a given Hamiltonian
    allocate(lhs(n_coup_modes))
    lhs  = [(j, j = 0, n_coup_modes - 1)]

    allocate(rhs(n_coup_modes))
    rhs  = [(j, j = n_coup_modes - 1, 0, -1)]

    if (n_coup_modes == 1) then
        coupled_matrix(1,:,:) = matrix(1,:,:)
        RETURN
    end if

    do j = 1, n_coup_modes
        ident_right_size = 0
        ident_left_size  = 0

        ! Create left and right identities
        ! If first or last mode then no need for left or right identities
        if (j /= 1) then
            ident_left_size  = n_vib_states ** lhs(j)
        end if
        if (j /= n_coup_modes) then
            ident_right_size = n_vib_states ** rhs(j)
        end if

        ! First mode then kronecker from right with phonon I_IDENTITY
        if (j == 1) then
            coupled_matrix(j,:,:) = KRON_PROD(matrix(j,:,:), I_IDENTITY(ident_right_size))
        ! Last mode then kronecker from left with phonon I_IDENTITY, but nothing from right
        else if (j == n_coup_modes) then
            coupled_matrix(j,:,:) = KRON_PROD(I_IDENTITY(ident_left_size), matrix(j,:,:))

        ! All other mode_numbers kronecker from left phonon identities
        ! and from right with remaining phonon identities
        else
            coupled_matrix(j,:,:) = KRON_PROD(KRON_PROD(I_IDENTITY(ident_left_size), matrix(j,:,:)), I_IDENTITY(ident_right_size))
        end if
    end do
end subroutine COUPLE_INTEGER_MODE_ARRAY

subroutine CALC_COUPLING_HAMILTONIAN(coup_dim, n_coup_modes, n_modes, &
                                     mode_numbers, elec_dim, n_vib_states, &
                                     H_coup, OEF, coupling_scale, eq_CF_vecs, &
                                     dbkq_dq, hdf5_method, hdf5_dim)
    ! Calculates spin-phonon coupling Hamiltonian to first order in
    ! normal mode coordinate
    USE matrix_tools
    USE stev_ops
    implicit none
    integer, intent(in)                          :: n_coup_modes, mode_numbers(:), coup_dim, &
                                                    n_vib_states, elec_dim, n_modes, hdf5_dim
    integer                                      :: j, pho_dim
    complex(kind = 8), intent(in)                :: eq_CF_vecs(:,:)
    complex(kind = 8), intent(out), allocatable  :: H_coup(:,:)
    complex(kind = 8), allocatable               :: H_coup_e(:,:), stevens_ops(:,:,:,:), &
                                                    pho_part(:,:), H_coup_p(:,:,:), H_coup_e_big(:,:)
    real(kind = 8), intent(in)                   :: OEF(3), coupling_scale, dbkq_dq(:,:,:)
    logical,intent(in)                           :: hdf5_method

    ! Allocate and zero coupling Hamiltonian
    ! and its vibrational and electronic parts
    pho_dim = n_vib_states**n_coup_modes
    allocate(H_coup(coup_dim, coup_dim), &
             H_coup_p(n_coup_modes, pho_dim, pho_dim), &
             H_coup_e(elec_dim,elec_dim))
    
    H_coup = (0.0_8, 0.0_8)
    H_coup_p = (0.0_8, 0.0_8)
    H_coup_e = (0.0_8, 0.0_8)

    IF(.NOT. hdf5_method) THEN
        ! Calculate stevens operators
        call GET_STEVENS_OPERATORS(elec_dim - 1, stevens_ops)
    END IF

    ! Calculate single mode phonon coupling Hamiltonian - same for each mode
    allocate(pho_part(n_vib_states, n_vib_states))
    pho_part = (0.0_8, 0.0_8)
    call GET_Q_MATRIX(n_vib_states, pho_part)

    IF(hdf5_method) ALLOCATE(H_coup_e_big(hdf5_dim,hdf5_dim))

    if(n_coup_modes > 1) then

        ! Calculate all possible coupled phonon matrices
        call COUPLE_COMPLEX_MODE_ARRAY(pho_part, H_coup_p, n_coup_modes, n_vib_states)

        ! Loop over each coupled mode and calculate coupling Hamiltonian
        do j = 1, n_coup_modes
            IF(hdf5_method) THEN
                CALL READ_PHONON_MATRIX_HDF5(hdf5_dim,mode_numbers(j),H_coup_e_big)
                H_coup_e_big = H_coup_e_big * coupling_scale
                call EXPECTATION('F', eq_CF_vecs, H_coup_e_big)
                H_coup_e(1:elec_dim,1:elec_dim) = H_coup_e_big(1:elec_dim,1:elec_dim)
            ELSE
                ! Calculate CF part of coupling Hamiltonian for current mode
                H_coup_e = H_CF(dbkq_dq(j,:,:), stevens_ops, OEF, 6)  * coupling_scale
                ! and move to equilibrium CF eigenbasis
                call EXPECTATION('F', eq_CF_vecs, H_coup_e)
            END IF

            ! Form kronecker product of electronic and vibrational parts
            H_coup = H_coup + KRON_PROD(H_coup_e, H_coup_p(j,:,:))

        end do

    else
        IF(hdf5_method) THEN
            CALL READ_PHONON_MATRIX_HDF5(hdf5_dim,mode_numbers(1),H_coup_e_big)
                H_coup_e_big = H_coup_e_big * coupling_scale
                call EXPECTATION('F', eq_CF_vecs, H_coup_e_big)
                H_coup_e(1:elec_dim,1:elec_dim) = H_coup_e_big(1:elec_dim,1:elec_dim)
        ELSE
            ! Calculate CF part of coupling Hamiltonian
            H_coup_e = H_CF(dbkq_dq(mode_numbers(1),:,:), stevens_ops, OEF, 6)  * coupling_scale
            ! and move to equilibrium CF eigenbasis
            call EXPECTATION('F', eq_CF_vecs, H_coup_e)
        END IF
        

        ! Form kronecker product of electronic and vibrational parts
        H_coup = KRON_PROD(H_coup_e, pho_part)

    end if

    deallocate(H_coup_p, H_coup_e)
    IF(hdf5_method) DEALLOCATE(H_coup_e_big)

    ! Write coupling Hamiltonian to file
    ! call WRITE_ARRAY(H_coup, 'H_coup.dat')

end subroutine CALC_COUPLING_HAMILTONIAN

subroutine LABEL_BASIS(elec_dim, pho_dim, n_coup_modes, n_vib_states, coup_dim, &
                       basis_labels)
    USE matrix_tools
    integer, intent(in)              :: elec_dim, pho_dim, n_coup_modes, n_vib_states,&
                                        coup_dim
    integer                          :: i, j, n
    integer,allocatable              :: elec(:,:), phonon(:,:,:), total(:,:,:), tot_pho(:,:,:),&
                                        big_pho(:,:,:), big_elec(:,:)
    integer,allocatable, intent(out) :: basis_labels(:,:)

    allocate(phonon(n_coup_modes,n_vib_states,n_vib_states),&
             elec(elec_dim,elec_dim),&
             total(n_coup_modes+1,coup_dim,coup_dim),&
             basis_labels(coup_dim,n_coup_modes+1),&
             tot_pho(n_coup_modes,pho_dim,pho_dim),&
             big_elec(coup_dim, coup_dim),&
             big_pho(n_coup_modes,coup_dim,coup_dim))

    elec = 0
    phonon = 0
    n = 0
    do i = 1, elec_dim - 1, 2
        n = n + 1
        elec(i, i) = -n
        elec(i + 1, i + 1) = n
    end do

    do i = 2,n_vib_states
        phonon(:,i,i) = i-1 
    end do

    ! Get electronic labels in big basis
    big_elec = KRON_PROD(elec,I_IDENTITY(pho_dim))

    ! Get phonon labels in total phonon basis - no electronic part!
    call COUPLE_INTEGER_MODE_ARRAY(phonon, tot_pho, n_coup_modes, n_vib_states)

    ! Get phonon labels in big basis - now including electronic part
    do j = 1,n_coup_modes
        big_pho(j,:,:) = KRON_PROD(I_IDENTITY(elec_dim), tot_pho(j,:,:))
    end do

    ! Copy electronic labels into 1st column of basis labels
    basis_labels(:,1) = DIAG(big_elec)

    ! Copy phonon labels into remaining columns of basis labels
    ! Mode 1 labels go into 2nd column, mode 2 into 3rd, etc.
    do j = 1,n_coup_modes
        basis_labels(:, j + 1) = DIAG(big_pho(j,:,:))
    end do

    ! Flip phonon labels of each electronic state to match flipped input mode numbers from user
    ! This gets around the backwards nature of the kronecker product
    do j = 1, coup_dim
        basis_labels(j,2:) = FLIPUD(basis_labels(j,2:))
    end do

    !call write_array(array=basis_labels, file='basis_labels.dat')

    deallocate(big_elec, big_pho, tot_pho, phonon)

end subroutine LABEL_BASIS

subroutine CALC_H_EQ_TOTAL(elec_dim, pho_dim, coup_dim, EQ_H_CF, EQ_H_pho, H_Coup, &
                           H_total, H_total_NC)
    USE matrix_tools
    implicit none
    integer, intent(in)                          :: elec_dim, pho_dim, coup_dim
    real(kind = 8), intent(in)                   :: EQ_H_pho(:,:)
    complex(kind = 8), intent(in)                :: H_Coup(:,:), EQ_H_CF(:,:)
    complex(kind = 8), allocatable , intent(out) :: H_total(:,:), H_total_NC(:,:)

    ! Allocate zero field total Hamiltonian
    allocate(H_total(coup_dim, coup_dim), &
             H_total_NC(coup_dim, coup_dim))

    ! Form zero field total Hamiltonian with no coupling Hamiltonian
    ! HCF+HZEE+HPHO
    if (pho_dim > 1) then 
        H_total_NC = KRON_PROD(EQ_H_CF, C_IDENTITY(pho_dim)) + KRON_PROD(IDENTITY(elec_dim),&
                               EQ_H_pho)
        H_total = H_total_NC + H_Coup
    else
        H_total_NC = EQ_H_CF
        H_total = H_total_NC
    end if

end subroutine CALC_H_EQ_TOTAL


subroutine WRITE_ZF_INFO(elec_dim, pho_dim, coup_dim, gJ, CF_vecs, CF_vals, EQ_H_pho, &
                         EQ_H_total, EQ_H_total_NC, basis_labels, B_quant)
    USE matrix_tools
    ! Writes information on zero-field states to output file
    ! Phonon energies
    ! CF energies, state compositions, magnetic moments, and <Jz>
    ! Coupled/composite state energies and compositions
    implicit none
    integer, intent(in)                          :: elec_dim, pho_dim, coup_dim, &
                                                    basis_labels(:, :)
    integer                                      :: i, j, row, col, num_bars
    real(kind = 8), intent(in)                   :: CF_vals(:), gJ, EQ_H_pho(:,:), B_quant
    real(kind = 8), allocatable                  :: mJ(:), cf_percents(:,:), state_mu_z(:), &
                                                    state_labels(:), &
                                                    tot_vals(:), tot_vals_NC(:), &
                                                    tot_percents_NC(:,:), tot_percents(:,:)
    complex(kind = 8), intent(in)                :: CF_vecs(:,:), EQ_H_total(:,:), &
                                                    EQ_H_total_NC(:,:)
    complex(kind = 8), allocatable               :: tot_vecs(:,:), tot_vecs_NC(:,:)
    character(len = 10000)                       :: FMT, FMTT, state_labels_char, output_char

    ! Calculate all the information that will be put in the output file

    ! Calculate Magnetic moment (along z) of each state
    call CALC_MAGNETIC_MOMENT(elec_dim, gJ, CF_vecs, state_mu_z)

    ! Make vector of mJ values
    ! -mJ to +mJ
    allocate(mJ(elec_dim))
    mJ = 0.0_8
    do i = 1,elec_dim
        mJ(i) = -real(elec_dim-1, 8)/2.0_8 + (i-1)*1.0_8
    end do

    ! Calculate <Jz> values of each state
    allocate(state_labels(elec_dim))
    state_labels = 0.0_8
    do i = 1,elec_dim
        do j = 1,elec_dim
            state_labels(i) = state_labels(i) + mJ(j)*(ABS(real(CF_vecs(j,i),8))**2 + ABS(aimag(CF_vecs(j, i)))**2)
        end do
    end do

    ! Calculate % composition of crystal field states
    allocate(cf_percents(elec_dim, elec_dim))
    do row = 1, elec_dim
        cf_percents(row,:) = ABS(real(CF_vecs(:,row),8))**2 + ABS(aimag(CF_vecs(:,row)))**2
    end do

    ! Allocate array for phonon eigenvalues
    allocate(PHO_vals(pho_dim))
    PHO_vals = 0.0_8

    ! Get Phonon eigenvalues 
    call DIAGONALISE('U',EQ_H_pho, PHO_vals)
    PHO_vals = PHO_vals - PHO_vals(1)

    ! Allocate arrays for total eigenvectors and eigenvalues without coupling
    allocate(tot_vecs_NC(coup_dim, coup_dim), &
             tot_vals_NC(coup_dim), &
             tot_percents_NC(coup_dim, coup_dim))

    tot_vecs_NC     = (0.0_8, 0.0_8)
    tot_vals_NC     = 0.0_8
    tot_percents_NC = 0.0_8

    ! Diagonalise H_total without coupling
    call DIAGONALISE('U', EQ_H_total_NC, tot_vecs_NC, tot_vals_NC)
    tot_vals_NC = tot_vals_NC - tot_vals_NC(1)

    ! Percentage composition of total eigenvectors without coupling
    do row = 1, coup_dim
        tot_percents_NC = ABS(tot_vecs_NC)**2
    end do

    ! Allocate arrays for total eigenvectors and eigenvalues with coupling
    allocate(tot_vecs(coup_dim, coup_dim), &
             tot_vals(coup_dim), &
             tot_percents(coup_dim, coup_dim))

    tot_vecs     = (0.0_8, 0.0_8)
    tot_vals     = (0.0_8, 0.0_8)
    tot_percents = 0.0_8

    ! Diagonalise H_total with coupling
    call DIAGONALISE('U', EQ_H_total, tot_vecs, tot_vals)
    tot_vals = tot_vals - tot_vals(1)

    ! Percentage composition of total eigenvectors with coupling
    do row = 1, coup_dim
        tot_percents = ABS(tot_vecs)**2
    end do

    ! Open output file
    open(66, FILE = 'firms_sim.out', STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Write equilbrium electronic structure section header
    write(66,'(A)') ''
    call SECTION_HEADER('Equilibrium electronic structure', 66)
    write(66,'(A)') ''
    write(66,'(A)') 'A very small magnetic field has been applied to quantise the states'
    write(66,'(A, F15.7, A)') 'B_quant = ', B_quant, ' T'
    write(66,'(A)') ''

    ! Write <J_z> values to output file  
    call SUB_SECTION_HEADER('<J_z> Values', 66)
    do row = 1, elec_dim
        write(66,'(A, I2, A, F15.7)') 'State ', row,' <J_z> = ',state_labels(row)
    end do
    write(66,*) 

    ! Write magnetic moment values to output file
    call SUB_SECTION_HEADER('z-Magnetic Moment (mu_B)', 66)
    do row = 1, elec_dim
        write(66,'(A, I2, A, F15.7)') 'State ', row,' mu = ',state_mu_z(row)
    end do
    write(66,*) 

    ! Crystal Field eigenvalues - these have been replaced!
    call SUB_SECTION_HEADER('Equilibrium Crystal Field Eigenvalues (cm^-1)', 66)
    do row = 1, elec_dim
        write(66,'(A, I2, A, F15.7)') 'State ', row,' E = ', CF_vals(row) - CF_vals(1)
    end do
    write(66,*) 

    ! Crystal Field eigenvectors

    ! % composition
    call SUB_SECTION_HEADER('Equilibrium Crystal Field Vectors (percentages)', 66)

    ! Formatting for mJ labels used in % composition table
    write(FMTT,*) '(',('F5.1,A,', i = 1, elec_dim),')'
    write(state_labels_char,trim(FMTT)) (mJ(i),'  ',i = 1, elec_dim)
    
    ! Write mJ labels for table
    write(output_char,'(2A)') ' mJ =     ', trim(state_labels_char)
    write(66,'(A)') trim(output_char)

    ! Formatting for the number of underscores
    num_bars = len(trim(adjustl(output_char))) + 1
    write(66,'(A)') repeat('‾',num_bars)

    ! Formatting for % compositions
    write(FMT, '(A, I0, A)') '(A, I2, ', elec_dim,'F7.2)'

    ! Write % compositions
    do row = 1, elec_dim
        write(66,FMT) 'State ', row, (100.0_8*cf_percents(row,col),col = 1, elec_dim)
    end do

    deallocate(cf_percents)

! Write equilbrium Vibrational Modes section header
    write(66,'(A)') ''
    call SECTION_HEADER('Equilibrium Vibrational Modes', 66)
    write(66,'(A)') ''

    if (pho_dim > 1) then
        call SUB_SECTION_HEADER('Equilibrium Phonon Eigenvalues (cm^-1)', 66)
        do row = 1, pho_dim
            write(66,'(A, I4, A, F15.7)') 'State ', row,' E = ', PHO_vals(row)
        end do

    else
        write(66,'(A)') '                          No modes are used for coupling                        '
    end if

! Write equilbrium electronic structure section header
    write(66,'(A)') ''
    call SECTION_HEADER('Zero Field Total Hamiltonian', 66)
    write(66,'(A)') ''

    write(66,'(A, I0, A)') 'Total Hamiltonian contains ', coup_dim,' states'
    write(66,'(A)') ''
    write(66,'(A)') 'A very small magnetic field has been applied to quantise the states'
    write(66,'(A, F15.7, A)') 'B_quant = ', B_quant, ' T'
    write(66,'(A)') ''

    ! Total eigenvalues without coupling
    call SUB_SECTION_HEADER('Zero Field Eigenvalues of total Hamiltonian without coupling term (cm^-1)', 66)
    do row = 1, coup_dim
        write(66,'(A, I5, A, F15.7)') 'State ', row,' E = ', tot_vals_NC(row)
    end do
    write(66,'(A)') ''

    ! Total eigenvalues with coupling
    call SUB_SECTION_HEADER('Zero Field Eigenvalues of total Hamiltonian with coupling term (cm^-1)', 66)
    do row = 1, coup_dim
        write(66,'(A, I5, A, F15.7)') 'State ', row,' E = ', tot_vals(row)
    end do
    write(66,'(A)') ''

    ! Total eigenvectors without coupling
    write(66,'(A)') '           Zero Field Total Hamiltonian Vectors without coupling term           '
    call SUB_SECTION_HEADER('Percentage contribution of |KD,n_j,...> states', 66)
    write(66,'(A)') '           Each column is an eigenvector           '

    ! Header line of table
    write(FMT,*) '(A,',('A, I0,',j = 1, n_coup_modes), ')'
    write(66,FMT) 'KD',(' n',j, j = 1, n_coup_modes)

    ! Formatting for % compositions
    write(FMT, '(A, I0, A, I0, A)') '(SP, I2, SS,', n_coup_modes, 'I3, A,', coup_dim, 'F7.2)'

    ! Write % compositions
    do row = 1, coup_dim
        write(66,FMT) (basis_labels(row,col), col = 1, n_coup_modes+1), '  ', (100.0_8*tot_percents_NC(row, col), col = 1, coup_dim)
    end do
    write(66,'(A)') ''

    ! Total eigenvectors without coupling
    write(66,'(A)') '           Zero Field Total Hamiltonian Vectors with coupling term           '
    call SUB_SECTION_HEADER('Percentage contribution of |KD,n_j,...> states', 66)
    write(66,'(A)') '           Each column is an eigenvector           '

    ! Header line of table
    write(FMT,*) '(A,',('A, I0,',j = 1, n_coup_modes), ')'
    write(66,FMT) 'KD',(' n',j, j = 1, n_coup_modes)

    ! Formatting for % compositions
    write(FMT, '(A, I0, A, I0, A)') '(SP, I2, SS,', n_coup_modes, 'I3, A,', coup_dim, 'F7.2)'

    ! Write % compositions
    do row = 1, coup_dim
        write(66,FMT) (basis_labels(row,col), col = 1, n_coup_modes+1), '  ', (100.0_8*tot_percents(row, col), col = 1, coup_dim)
    end do
    write(66,'(A)') ''

    ! Close output file
    close(66)

end subroutine WRITE_ZF_INFO

subroutine READ_RED_MASSES(red_mass_file, red_masses, n_modes)
    ! Read reduced masses from output file
    ! File structured as 
    ! mode 1 reduced mass
    ! mode 2 reduced mass
    ! ...
    ! mode 3N-6 reduced mass
    ! Outputted from gaussian_freq
    implicit none
    character(len = *), intent(in)           :: red_mass_file
    real(kind = 8), intent(out), allocatable :: red_masses(:)
    integer, intent(in)                      :: n_modes
    integer                                  :: j, reason

    allocate(red_masses(n_modes))
    red_masses = 0.0_8

    ! Open file
    open(33, FILE = trim(red_mass_file), STATUS = 'OLD')

        ! Loop through file
        do j= 1, n_modes

            ! Read and store in units of amu
            READ(33, *, IOSTAT=reason) red_masses(j)

            ! Check file is not corrupted or too short
            if (reason > 0)  then
                call PRINT_ERROR('Error reading '//trim(red_mass_file), 'Check file')
            else if (reason < 0) then
                call PRINT_ERROR('Error Reached end of  '//trim(red_mass_file)//' too soon', 'Check file')
            end if

        end do
        
    ! Close file
    close(33)

end subroutine READ_RED_MASSES

subroutine READ_DELEC_MU(delec_mu_file, delec_mu, red_masses, n_modes)
    ! Read intensity from output file
    ! and square root to get magnitude of dipole derivative vector
    ! File structured as 
    ! mode 1 intensity
    ! mode 2 intensity
    ! ...
    ! mode 3N-6 intensity
    ! Outputted from gaussian_freq
    implicit none
    character(len = *), intent(in)           :: delec_mu_file
    real(kind = 8), intent(out), allocatable :: delec_mu(:,:)
    real(kind = 8), intent(in)               :: red_masses(:)
    integer, intent(in)                      :: n_modes
    integer                                  :: j, reason

    allocate(delec_mu(n_modes,3))
    delec_mu = 0.0_8

    ! Open file
    open(33, FILE = trim(delec_mu_file), STATUS = 'OLD')

        ! Loop through file
        do j = 1, n_modes

            ! Read and store intensity = (dmu/dQ)^2 [km mol-1]
            READ(33, *, IOSTAT=reason) delec_mu(j,1), delec_mu(j,2), delec_mu(j,3)

            ! Multiply by  1/sqrt(42.2561) to convert from km^1/2 mol-1/2 --> D angstrom^-1 amu-1/2
            ! and then by sqrt of reduced mass to remove mass weighting
            ! and by 10**10 to convert D angstrom^-1 --> D m^-1
            ! Then finally multiply by 1/299792458 x 10^-21 to convert [D] to [C m]
            ! which gives final units of [C]
            delec_mu(j,:) = delec_mu(j,:) * sqrt(red_masses(j)) * 1D10 / sqrt(42.2561) * 1.0_8/(299792458.0_8) * 1D-21

            ! Check file is not corrupted or too short
            if (reason > 0)  then
                call PRINT_ERROR('Error reading '//trim(delec_mu_file), 'Check file')
            else if (reason < 0) then
                call PRINT_ERROR('Error Reached end of  '//trim(delec_mu_file)//' too soon', 'Check file')
            end if

        end do
    ! Close file
    close(33)

end subroutine READ_DELEC_MU

subroutine CALC_MAGNETIC_MOMENT(elec_dim, gJ, CF_vecs, state_mu)
    USE matrix_tools
    USE constants, ONLY : muB
    implicit none
    real(kind = 8), allocatable, intent(out)   :: state_mu(:)
    real(kind = 8), intent(in)                 :: gJ
    complex(kind = 8), intent(in)              :: CF_vecs(:,:)
    complex(kind = 8), allocatable             :: JZ(:,:)
    complex(kind = 8), allocatable             :: pos(:,:)
    integer, intent(in)                        :: elec_dim
    integer                                    :: i

! Calculate JZ operator
    allocate(JZ(elec_dim, elec_dim))
    JZ = (0.0_8, 0.0_8)
    do i = 1,elec_dim
        JZ(i,i) = -0.5_8*(elec_dim - 1) + (i-1)*1.0_8
    end do

! Allocate array for magnetic moment values
    allocate(state_mu(elec_dim), pos(elec_dim, elec_dim))
    state_mu = 0.0_8
    pos      = 0.0_8

! Calculate magnetic moment of each state
! Use Zeeman Hamiltonian in Z direction and change to eigenbasis of crystal field Hamiltonian
    pos = -muB*gJ*JZ
    call EXPECTATION('F',CF_vecs,pos)
    do i = 1, elec_dim
        state_mu(i) = -real(pos(i,i),8)
    end do

end subroutine CALC_MAGNETIC_MOMENT

subroutine MAIN_SPECTRUM(EQ_H_CF, EQ_H_pho, H_Coup, elec_dim, pho_dim, coup_dim, &
                            field_vecs, field_angs, n_field_strengths, n_field_vecs, gJ, &
                            lower_e_bound, upper_e_bound, total_absorbance, e_axis, resolution,&
                            temperature, basis_labels, trans_treat, elec_scale,ir_grid_size, &
                            CF_vecs, linewidth, delec_mu, red_masses, mode_energies, &
                            hdf5_method, hdf5_dim, new_cf_energies)
    USE matrix_tools
    USE stev_ops
    USE OMP_LIB
    USE constants
    implicit none
    real(kind = 8), allocatable               ::  EQ_H_pho_BB(:,:), tot_vals(:), pops(:), &
                                                  absorbance(:), trans_energies(:)
    real(kind = 8)                            ::  e_step, curr_field(3), curr_angs(2)
    real(kind = 8), allocatable               ::  EQ_H_pho(:,:), B(:,:), elec_mu2(:,:), mag_mu2(:,:), new_CF_Vals(:)
    real(kind = 8), intent(in)                ::  lower_e_bound, upper_e_bound, &
                                                  field_vecs(:,:,:), temperature, gJ, &
                                                  field_angs(:,:), elec_scale, linewidth, &
                                                  delec_mu(:,:), red_masses(:), mode_energies(:)
    real(kind = 8), intent(out), allocatable  ::  total_absorbance(:,:), e_axis(:)
    complex(kind = 8), allocatable            ::  EQ_H_CF_BB(:,:), H_ZEE(:,:), H_total(:,:), &
                                                  tot_vecs(:,:), mag_mu_x_bb(:,:), mag_mu_y_bb(:,:), mag_mu_z_bb(:,:), &
                                                  elec_mu_x_bb(:,:), elec_mu_y_bb(:,:), elec_mu_z_bb(:,:), &
                                                  mag_mu_x_bb_copy(:,:), mag_mu_y_bb_copy(:,:), mag_mu_z_bb_copy(:,:), &
                                                  elec_mu_x_bb_copy(:,:), elec_mu_y_bb_copy(:,:), elec_mu_z_bb_copy(:,:), &
                                                  AngMomMat(:,:,:), SpinMat(:,:,:)
    complex(kind = 8), intent(in)             ::  H_Coup(:,:), CF_vecs(:,:)
    complex(kind = 8), intent(inout)          ::  EQ_H_CF(:,:)
    integer, intent(in)                       ::  elec_dim, pho_dim, coup_dim, &
                                                  n_field_strengths, n_field_vecs, &
                                                  resolution, ir_grid_size, hdf5_dim
    integer                                   ::  i, fval, fvec, k, num_trans, basis_labels(:,:)
    integer, allocatable                      ::  trans_indices(:,:)
    character(len = *)                        ::  trans_treat
    logical, intent(in)                       ::  hdf5_method, new_cf_energies

    ! Get CF and PHO Hamiltonians in the coupled basis
    allocate(EQ_H_pho_BB(coup_dim,coup_dim))
    EQ_H_pho_BB  = (0.0_8, 0.0_8)
    EQ_H_pho_BB  = KRON_PROD(IDENTITY(elec_dim), EQ_H_pho)

    deallocate(EQ_H_PHO)

    ! Create array of energies for absorbances
    allocate(e_axis(resolution))
    e_step = (upper_e_bound - lower_e_bound)/real(resolution,8)
    e_axis = [(lower_e_bound+e_step*real(i,8), i = 0, resolution - 1)]

    ! Create array of total absorbances and single absorbances
    allocate(total_absorbance(n_field_strengths, resolution))
    total_absorbance = 0.0_8

    ! Calculate magnetic dipole moment in x, y, and z directions in CF eigenbasis
    IF(hdf5_method) THEN
        CALL READ_ANGMOM_HDF5(hdf5_dim,SpinMat,AngMomMat)
        call CALC_MAG_MU_HDF5(hdf5_dim, elec_dim, coup_dim, pho_dim, &
                         cf_vecs, mag_mu_x_bb, mag_mu_y_bb, mag_mu_z_bb, SpinMat, AngMomMat)
    ELSE
        call CALC_MAG_MU(INT(2*J), elec_dim, coup_dim, pho_dim, &
                         cf_vecs, mag_mu_x_bb, mag_mu_y_bb, mag_mu_z_bb)
    END IF

    ! Calculate electric dipole moment in x, y, and z directions in CF eigenbasis
    call CALC_ELEC_MU(delec_mu, mode_energies, red_masses, n_coup_modes, n_vib_states, &
                      pho_dim, mode_numbers, elec_dim, coup_dim, cf_vecs, &
                      elec_mu_x_bb, elec_mu_y_bb, elec_mu_z_bb)

    ! Read replacement eigenvalues if needed for hdf5 method
    if(hdf5_method .AND. new_cf_energies) then
        allocate(new_CF_Vals(elec_dim))
        call READ_CF_SHIFT(new_CF_Vals)
        EQ_H_CF(1:elec_dim,1:elec_dim) = DIAG(new_CF_Vals)
    end if

!$OMP PARALLEL SHARED(e_axis, gJ, field_vecs, EQ_H_CF, H_Coup, temperature), &
!$OMP& SHARED(n_field_strengths, total_absorbance, n_field_vecs, pho_dim, EQ_H_pho_BB), &
!$OMP& SHARED(upper_e_bound, lower_e_bound, coup_dim, linewidth), &
!$OMP& SHARED(trans_treat, elec_scale, ir_grid_size, basis_labels, red_masses, delec_mu), &
!$OMP& SHARED(mode_energies, mag_mu_x_bb, mag_mu_y_bb, mag_mu_z_bb, elec_mu_x_bb, elec_mu_y_bb, elec_mu_z_bb), &
!$OMP& SHARED(hdf5_method, hdf5_dim, SpinMat, AngMomMat, new_CF_Vals)

    !$OMP do SCHEDULE(DYNAMIC,1), &
    !$OMP PRIVATE(num_trans, trans_energies, fval, fvec), & 
    !$OMP& PRIVATE(curr_field, tot_vecs, tot_vals, H_total, H_ZEE, EQ_H_CF_BB, pops), &
    !$OMP& PRIVATE(absorbance, trans_indices, curr_angs, B, mag_mu2, elec_mu2), &
    !$OMP& PRIVATE(mag_mu_x_bb_copy, mag_mu_y_bb_copy, mag_mu_z_bb_copy, elec_mu_x_bb_copy, elec_mu_y_bb_copy, elec_mu_z_bb_copy)
    
    do fval = 1, n_field_strengths
        do fvec = 1, n_field_vecs

            allocate(EQ_H_CF_BB(coup_dim,coup_dim), &
                     H_total(coup_dim, coup_dim), tot_vals(coup_dim), &
                     tot_vecs(coup_dim, coup_dim), pops(coup_dim), &
                     absorbance(resolution), B(coup_dim, coup_dim))
            allocate(trans_indices(coup_dim**2, 2))
            allocate(trans_energies(coup_dim**2))
            trans_energies = 0.0_8
            num_trans = 0

            ! Set arrays to zero
            tot_vecs       = (0.0_8, 0.0_8)
            B              = (0.0_8, 0.0_8)
            tot_vals       = 0.0_8
            H_total        = (0.0_8, 0.0_8)
            EQ_H_CF_BB     = (0.0_8, 0.0_8)
            pops           = 0.0_8
            trans_indices  = 0
            absorbance     = 0.0_8

            ! Calculate zeeman Hamiltonian
            curr_field = field_vecs(fval, fvec, :)
            curr_angs  = field_angs(fvec, :)
            IF(hdf5_method) THEN
                allocate(H_ZEE(hdf5_dim, hdf5_dim))
                H_ZEE = (0.0_8, 0.0_8)
                H_ZEE = muB*(curr_field(1)*(AngMomMat(1,:,:) + ge*SpinMat(1,:,:)) &
                                    + curr_field(2)*(AngMomMat(2,:,:) + ge*SpinMat(2,:,:)) &
                                    + curr_field(3)*(AngMomMat(3,:,:) + ge*SpinMat(3,:,:)) &
                                    )
            ELSE
                allocate(H_ZEE(elec_dim, elec_dim))
                H_ZEE = (0.0_8, 0.0_8)
                call CALC_H_ZEE(curr_field(1), curr_field(2), curr_field(3), gJ, H_ZEE)
            END IF

            ! and move to "zero" field electronic eigenbasis
            call EXPECTATION('F', CF_vecs, H_ZEE)

            ! Add CF and ZEE Hamiltonians and move to coupled basis
            ! |CF,n>
            EQ_H_CF_BB  = KRON_PROD(EQ_H_CF(1:elec_dim,1:elec_dim) + H_ZEE(1:elec_dim,1:elec_dim) , C_IDENTITY(pho_dim))

            ! Form total Hamiltonian in coupled basis
            ! |CF,n>
            if (pho_dim > 1) then
                H_total = EQ_H_CF_BB + EQ_H_pho_BB + H_Coup
            else
                H_total = EQ_H_CF_BB
            end if

            ! Diagonalise total Hamiltonian in coupled basis
            !|CF,n> --> |\Psi>
            call DIAGONALISE('U', H_total, tot_vecs, tot_vals)
            tot_vals = tot_vals - tot_vals(1)

            ! Calculate Boltzmann populations of energy levels
            call CALC_POPS(tot_vals, pops, temperature)

            ! Calculate transition energies
            call CALC_TRANS_ENERGIES(tot_vals, upper_e_bound, lower_e_bound, trans_energies, &
                                     trans_indices, num_trans, pops)

            ! Calculate magnetic and electric transition dipole moments in eigenbasis of total Hamiltonian
            allocate(mag_mu_x_bb_copy(coup_dim, coup_dim), &
                     mag_mu_y_bb_copy(coup_dim, coup_dim), &
                     mag_mu_z_bb_copy(coup_dim, coup_dim), &
                     elec_mu_x_bb_copy(coup_dim, coup_dim), &
                     elec_mu_y_bb_copy(coup_dim, coup_dim), &
                     elec_mu_z_bb_copy(coup_dim, coup_dim))

            ! Copy from clean
            mag_mu_x_bb_copy = mag_mu_x_bb
            mag_mu_y_bb_copy = mag_mu_y_bb
            mag_mu_z_bb_copy = mag_mu_z_bb
            elec_mu_x_bb_copy = elec_mu_x_bb
            elec_mu_y_bb_copy = elec_mu_y_bb
            elec_mu_z_bb_copy = elec_mu_z_bb

            ! and then move to eigenbasis of total Hamiltonian
            call EXPECTATION('F', tot_vecs, mag_mu_x_bb_copy)
            call EXPECTATION('F', tot_vecs, mag_mu_y_bb_copy)
            call EXPECTATION('F', tot_vecs, mag_mu_z_bb_copy)

            ! and then move to eigenbasis of total Hamiltonian
            call EXPECTATION('F', tot_vecs, elec_mu_x_bb_copy)
            call EXPECTATION('F', tot_vecs, elec_mu_y_bb_copy)
            call EXPECTATION('F', tot_vecs, elec_mu_z_bb_copy)

            ! Magnetic dipole moment operator squared in units of J T-1
            ! For unpolarised radiation in Voight geometry
            ! From Stoll paper on EPR transition intensities
            mag_mu2 = UNPOLARISED_MU2(mag_mu_x_bb_copy, mag_mu_y_bb_copy, mag_mu_z_bb_copy, curr_field)

            ! Electric dipole moment operator squared in units of 
            ! For unpolarised radiation in Voight geometry
            ! From Stoll paper on EPR transition intensities
            elec_mu2 = UNPOLARISED_MU2(elec_mu_x_bb_copy, elec_mu_y_bb_copy, elec_mu_z_bb_copy, curr_field)

            ! Calculate einstein coefficients in uncoupled basis and then move to coupled basis
            call CALC_EINSTEIN_B(B, mag_mu2, elec_mu2, coup_dim, elec_scale)

            ! Calculate current spectrum
            call CALC_SPECTRUM(trans_energies, trans_indices, num_trans, pops, &
                               B, absorbance, e_axis, linewidth, n_coup_modes)

            ! Add to total absorbance for this field value
            total_absorbance(fval, :) = total_absorbance(fval, :) + absorbance

            if (single_axis /= 'no' .AND. fval == 1 .AND. .NOT. hdf5_method) call WRITE_TRANSITION_INFO(trans_energies, trans_indices, basis_labels, tot_vecs)

            deallocate(EQ_H_CF_BB, H_ZEE, H_total, tot_vals, tot_vecs, pops, &
                       absorbance, trans_indices, trans_energies, B, mag_mu2, elec_mu2, &
                       mag_mu_x_bb_copy, mag_mu_y_bb_copy, mag_mu_z_bb_copy, elec_mu_x_bb_copy,&
                       elec_mu_y_bb_copy, elec_mu_z_bb_copy)

        end do
    end do
    !$OMP end do NOWAIT 
!$OMP END PARALLEL 

total_absorbance = total_absorbance / n_field_vecs

! normalise absorbance at each field position

! do fval = 1, n_field_strengths
!     total_absorbance(fval,:) = total_absorbance(fval,:)/SUM(total_absorbance(fval,:))
! end do

end subroutine MAIN_SPECTRUM

subroutine WRITE_TRANS_ENERGIES(trans_energies, num_trans, n_field_strengths, n_field_vecs)
    implicit none
    integer, intent(in)          :: num_trans(:,:), n_field_strengths, n_field_vecs
    integer                      :: fval, fvec, tnum, fvi
    real(kind = 8), intent(in)   :: trans_energies(:,:,:)
    character(len = 100)         :: fmt, file_name

    write(fmt, '(A, I0, A)') '(',n_field_vecs, 'E47.36E4)'

    fvi = 0

    do fval = 1, n_field_strengths
        write(file_name, '(A, I0, A)') 'trans_energies_',fval,'.dat' 
        open(33, FILE = file_name, STATUS='UNKNOWN')
            fvi = fvi + 1
            do tnum = 1, num_trans(fval,fvi)
                write(33,fmt) (trans_energies(fval, fvec, tnum), fvec = 1, n_field_vecs)
            end do
        close(33)
    end do

end subroutine WRITE_TRANS_ENERGIES


recursive subroutine CALC_MAG_MU(two_J, elec_dim, coup_dim, pho_dim, &
                                 CF_vecs, mu_x_bb, mu_y_bb, mu_z_bb)
    ! Calculates magnetic dipole transition probabilities
    USE constants
    implicit none
    integer, intent(in)                         :: two_J, elec_dim, coup_dim, pho_dim
    integer                                     :: row
    complex(kind = 8)                           :: ii
    complex(kind = 8), allocatable              :: JZ(:,:), JP(:,:), JM(:,:)
    complex(kind = 8), allocatable              :: mu_x(:,:), mu_y(:,:), mu_z(:,:)
    complex(kind = 8), allocatable, intent(out) :: mu_x_bb(:,:), mu_y_bb(:,:), mu_z_bb(:,:)
    complex(kind = 8), intent(in)               :: CF_vecs(:,:)

    ii = (0.0_8, 1.0_8)

    call CALC_JZPM(two_J, JZ, JP, JM)

    allocate(mu_x(elec_dim, elec_dim),&
                mu_y(elec_dim, elec_dim),&
                mu_z(elec_dim, elec_dim))
    
    mu_x = (0.0_8, 0.0_8)
    mu_y = (0.0_8, 0.0_8)
    mu_z = (0.0_8, 0.0_8)

    ! Magnetic dipole moment operator in units of sqrt(J T-1)

    mu_x = gJ * muB_joules * 0.5_8 * (JP + JM)
    mu_y = gJ * muB_joules * 1.0_8/(2.0_8*ii) * (JP - JM)
    mu_z = gJ * muB_joules * JZ

    ! Move mu_x, mu_y, and mu_z to CF eigenbasis from |mJ> basis
    call EXPECTATION('F', CF_vecs, mu_x)
    call EXPECTATION('F', CF_vecs, mu_y)
    call EXPECTATION('F', CF_vecs, mu_z)

    ! Then kronecker with identity to give mu_xyz in |CF, n>
    allocate(mu_x_bb(coup_dim, coup_dim),&
             mu_y_bb(coup_dim, coup_dim),&
             mu_z_bb(coup_dim, coup_dim))
    
    mu_x_bb = (0.0_8, 0.0_8)
    mu_y_bb = (0.0_8, 0.0_8)
    mu_z_bb = (0.0_8, 0.0_8)

    mu_x_bb = KRON_PROD(mu_x, IDENTITY(pho_dim))
    mu_y_bb = KRON_PROD(mu_y, IDENTITY(pho_dim))
    mu_z_bb = KRON_PROD(mu_z, IDENTITY(pho_dim))

    deallocate(mu_x, mu_y, mu_z)

end subroutine CALC_MAG_MU

recursive subroutine CALC_MAG_MU_HDF5(hdf5_dim, elec_dim, coup_dim, pho_dim, &
                                 CF_vecs, mu_x_bb, mu_y_bb, mu_z_bb, AngMomMat, SpinMat)
    ! Calculates magnetic dipole transition probabilities
    USE constants
    implicit none
    integer, intent(in)                         :: hdf5_dim, elec_dim, coup_dim, pho_dim
    integer                                     :: row
    complex(kind = 8)                           :: ii
    complex(kind = 8), allocatable              :: JZ(:,:), JP(:,:), JM(:,:)
    complex(kind = 8), allocatable              :: mu_x(:,:), mu_y(:,:), mu_z(:,:)
    complex(kind = 8), allocatable, intent(out) :: mu_x_bb(:,:), mu_y_bb(:,:), mu_z_bb(:,:)
    complex(kind = 8), intent(in)               :: CF_vecs(:,:), AngMomMat(:,:,:), SpinMat(:,:,:)

    ii = (0.0_8, 1.0_8)

    allocate(mu_x(hdf5_dim, hdf5_dim),&
                mu_y(hdf5_dim, hdf5_dim),&
                mu_z(hdf5_dim, hdf5_dim))
    
    mu_x = (0.0_8, 0.0_8)
    mu_y = (0.0_8, 0.0_8)
    mu_z = (0.0_8, 0.0_8)

    ! Magnetic dipole moment operator in units of sqrt(J T-1)
    mu_x = muB_joules*(AngMomMat(1,:,:) + ge*SpinMat(1,:,:))
    mu_y = muB_joules*(AngMomMat(2,:,:) + ge*SpinMat(2,:,:))
    mu_z = muB_joules*(AngMomMat(3,:,:) + ge*SpinMat(3,:,:))

    ! Move mu_x, mu_y, and mu_z to CF eigenbasis from |mJ> basis
    call EXPECTATION('F', CF_vecs, mu_x)
    call EXPECTATION('F', CF_vecs, mu_y)
    call EXPECTATION('F', CF_vecs, mu_z)

    ! Then kronecker with identity to give mu_xyz in |CF, n>
    allocate(mu_x_bb(coup_dim, coup_dim),&
             mu_y_bb(coup_dim, coup_dim),&
             mu_z_bb(coup_dim, coup_dim))
    
    mu_x_bb = (0.0_8, 0.0_8)
    mu_y_bb = (0.0_8, 0.0_8)
    mu_z_bb = (0.0_8, 0.0_8)

    mu_x_bb = KRON_PROD(mu_x(1:elec_dim,1:elec_dim), IDENTITY(pho_dim))
    mu_y_bb = KRON_PROD(mu_y(1:elec_dim,1:elec_dim), IDENTITY(pho_dim))
    mu_z_bb = KRON_PROD(mu_z(1:elec_dim,1:elec_dim), IDENTITY(pho_dim))

    deallocate(mu_x, mu_y, mu_z)

end subroutine CALC_MAG_MU_HDF5


recursive subroutine CALC_ELEC_MU(delec_mu, mode_energies, red_masses, n_coup_modes, n_vib_states, &
                                pho_dim, mode_numbers, elec_dim, coup_dim, cf_vecs, mu_x_bb, mu_y_bb, mu_z_bb)
    ! Calculate electric transition dipole moment
    USE constants, ONLY : pi, light, hbar, amu_to_kg
    implicit none
    integer, intent(in)                          :: n_coup_modes, n_vib_states, pho_dim, mode_numbers(:), &
                                                    elec_dim, coup_dim
    integer                                      :: i, j 
    real(kind = 8), intent(in)                   :: delec_mu(:,:), mode_energies(:), red_masses(:)
    real(kind = 8), allocatable                  :: single_quantum(:,:), &
                                                    mu_x(:,:), mu_y(:,:), mu_z(:,:), temp(:,:), &
                                                    mu_x_all(:,:,:), mu_y_all(:,:,:), mu_z_all(:,:,:)
    complex(kind = 8), allocatable, intent(out)  :: mu_x_bb(:,:), mu_y_bb(:,:), mu_z_bb(:,:)
    complex(kind = 8), intent(in)                :: CF_vecs(:,:)

    ! Matrix of allowed or forbidden transitions in Harmonic approximation
    ! result of raising and lowering operators
    ! delta n = +- 1
    ! if n_f > n_i then element = sqrt(n_f)
    ! if n_f < n_i then element = sqrt(n_i)
    ! so the square root of the bigger number
    allocate(single_quantum(n_vib_states,n_vib_states))
    single_quantum = 0.0_8
    do i = 1, n_vib_states
        do j = 1, n_vib_states
            if (ABS(j - i) == 1) then
                single_quantum(j,i) = SQRT(real(MAX(i, j)-1, 8))
            end if 
        end do
    end do
    ! Include revelant factors
    if (n_coup_modes > 1) then

        allocate(temp(n_vib_states, n_vib_states),&
                 mu_x_all(n_coup_modes, n_vib_states, n_vib_states),&
                 mu_y_all(n_coup_modes, n_vib_states, n_vib_states),&
                 mu_z_all(n_coup_modes, n_vib_states, n_vib_states))
                 
        temp = 0.0_8
        mu_x_all = 0.0_8
        mu_y_all = 0.0_8
        mu_z_all = 0.0_8
        
        do j = 1, n_coup_modes

            ! ! Only need to calculate constant terms once for each mode
            temp = single_quantum*SQRT(0.25*hbar &
                 / (red_masses(mode_numbers(j))*amu_to_kg*mode_energies(mode_numbers(j))*light*pi))

            ! Then multiply by dipole derivative for correct direction and store for each mode
            mu_x_all(j, :, :) = temp * delec_mu(mode_numbers(j),1)
            mu_y_all(j, :, :) = temp * delec_mu(mode_numbers(j),2)  
            mu_z_all(j, :, :) = temp * delec_mu(mode_numbers(j),3)

        end do

        ! Couple all modes together using identities
        allocate(mu_x(pho_dim, pho_dim),&
                 mu_y(pho_dim, pho_dim),&
                 mu_z(pho_dim, pho_dim))

        mu_x = 0.0_8
        mu_y = 0.0_8
        mu_z = 0.0_8         

        call COUPLE_MODE_ARRAY(mu_x_all, mu_x, n_coup_modes, n_vib_states)
        call COUPLE_MODE_ARRAY(mu_y_all, mu_y, n_coup_modes, n_vib_states)
        call COUPLE_MODE_ARRAY(mu_z_all, mu_z, n_coup_modes, n_vib_states)

        ! Then kronecker with identity to give mu_xyz in |CF, n> basis
        allocate(mu_x_bb(coup_dim, coup_dim),&
                 mu_y_bb(coup_dim, coup_dim),&
                 mu_z_bb(coup_dim, coup_dim))
        mu_x_bb = 0.0_8
        mu_y_bb = 0.0_8
        mu_z_bb = 0.0_8

        mu_x_bb = KRON_PROD(IDENTITY(elec_dim), mu_x)
        mu_y_bb = KRON_PROD(IDENTITY(elec_dim), mu_y)
        mu_z_bb = KRON_PROD(IDENTITY(elec_dim), mu_z)

        deallocate(mu_x_all, mu_y_all, mu_z_all)

    else

        ! n.b. for single mode pho_dim=n_vib_states
        allocate(temp(pho_dim, pho_dim),&
                 mu_x(pho_dim, pho_dim),&
                 mu_y(pho_dim, pho_dim),&
                 mu_z(pho_dim, pho_dim))

        temp = 0.0_8
        mu_x = 0.0_8
        mu_y = 0.0_8
        mu_z = 0.0_8

        ! Only need to calculate constant terms once for each mode
        temp = single_quantum*SQRT(0.25*hbar &
             / (red_masses(mode_numbers(1))*amu_to_kg*mode_energies(mode_numbers(1))*light*pi))

        ! Then multiply by dipole derivative for correct direction and store for each mode
        mu_x = temp * delec_mu(mode_numbers(1),1)
        mu_y = temp * delec_mu(mode_numbers(1),2)
        mu_z = temp * delec_mu(mode_numbers(1),3)

        ! Then kronecker with identity to give mu_xyz in |CF, n> basis
        allocate(mu_x_bb(coup_dim, coup_dim),&
                 mu_y_bb(coup_dim, coup_dim),&
                 mu_z_bb(coup_dim, coup_dim))

        mu_x_bb = 0.0_8
        mu_y_bb = 0.0_8
        mu_z_bb = 0.0_8

        mu_x_bb = KRON_PROD(IDENTITY(elec_dim), mu_x)
        mu_y_bb = KRON_PROD(IDENTITY(elec_dim), mu_y)
        mu_z_bb = KRON_PROD(IDENTITY(elec_dim), mu_z)

    end if

    deallocate(single_quantum, mu_x, mu_y, mu_z, temp)

end subroutine CALC_ELEC_MU

function UNPOLARISED_MU2(mu_x, mu_y, mu_z, B) result(mu)
    complex(kind = 8), intent(in)   :: mu_x(:,:), mu_y(:,:), mu_z(:,:)
    real(kind = 8)      :: n0(3)
    real(kind = 8), intent(in)      :: B(3)
    complex(kind = 8), allocatable  :: mu(:,:)
    
    allocate(mu(SIZE(mu_x,1),SIZE(mu_x,2)))
    mu = (0.0_8, 0.0_8)

    n0 = B / norm2(B)

    ! Calculate Mu using Stoll expression for powder averaged unpolarised light
    ! PRL 114, 010801 (2015)
    ! Called D_un,V ^ pow

    ! mu = ABS(mu_x)**2 + ABS(mu_y)**2 + ABS(mu_z)**2 + (ABS(mu_x*n0(1))**2 + ABS(mu_y*n0(2))**2 + ABS(mu_z*n0(3))**2)

    ! This way saves 6*dim*dim sqrt operations!
    mu = mu + real(mu_x, 8)**2 + AIMAG(mu_x)**2
    mu = mu + real(mu_y, 8)**2 + AIMAG(mu_y)**2
    mu = mu + real(mu_z, 8)**2 + AIMAG(mu_z)**2
    mu = mu + real(mu_x*n0(1), 8)**2 + AIMAG(mu_x*n0(1))**2
    mu = mu + real(mu_y*n0(2), 8)**2 + AIMAG(mu_y*n0(2))**2
    mu = mu + real(mu_z*n0(3), 8)**2 + AIMAG(mu_z*n0(3))**2
    mu = mu * 0.25

end function UNPOLARISED_MU2

subroutine CALC_EINSTEIN_B(B, mag_mu2, elec_mu2, coup_dim, elec_scale)
    USE constants, ONLY : pi, mu0, ep0, hbar, light
    implicit none
    integer, intent(in)            :: coup_dim
    real(kind = 8), allocatable    :: B_M(:,:), B_E(:,:)
    real(kind = 8), intent(in)     :: elec_mu2(:,:), elec_scale, mag_mu2(:,:)
    real(kind = 8), intent(out)    :: B(:,:)

    ! Allocate B_M and B_E
    allocate(B_E(coup_dim, coup_dim), B_M(coup_dim, coup_dim))
    B_E = 0.0_8
    B_M = 0.0_8

    ! Calculate magnetic einstein coefficients
    B_M = 2.0_8 * pi * mu0 / (3.0_8 * hbar**2 * light**2) * mag_mu2 * elec_scale

    ! Calculate electric einstein coefficients
    B_E = 2.0_8 * pi / (3.0_8 * hbar**2 * ep0 * light**2) * elec_mu2 

    ! Write largest matrix elements of elec_mu and mu_mag, and of einstein coefficients
    ! write(6,*) MAXVAL(ABS(B_M)), MAXVAL(ABS(B_E))
    ! write(6,*) minval(ABS(B_M)), minval(ABS(B_E))
    ! stop

    ! Sum electric and magnetic parts
    B = B_E + B_M

    deallocate(B_E, B_M)

end subroutine CALC_EINSTEIN_B

recursive subroutine CALC_H_ZEE(Bx, By, Bz, gJ, H_ZEE)
    use constants
    implicit none
    real(kind = 8), intent(in)      :: Bx, By, Bz, gJ
    complex(kind = 8), allocatable  :: JP(:,:), JM(:,:), JZ(:,:)
    complex(kind = 8), intent(out)  :: H_ZEE(:,:)
    complex(kind = 8)               :: ii

    ii = (0.0_8, 1.0_8)

    call CALC_JZPM(elec_dim - 1, JZ, JP, JM)

    H_ZEE = muB*gJ*(0.5_8*(JP+JM)*Bx + (1.0_8/(2.0_8*ii))*(JP-JM)*By + JZ*Bz)

    ! open(70, FILE = 'zeeman.txt', status = 'UNKNOWN')

    ! do row = 1, SIZE(JZ,1)
    !     write(70, '(8F15.7)') (real(JZ(row, col),8), col = 1, 8)
    ! end do

    ! close(70)

    deallocate(JZ, JP, JM)

end subroutine CALC_H_ZEE

recursive subroutine CALC_JZPM(two_J, JZ, JP, JM)
    implicit none
    integer, intent(in)                         :: two_J
    integer                                     :: i, k
    real(kind = 8)                              :: mJ
    complex(kind = 8), allocatable, intent(out) :: JZ(:,:), JP(:,:), JM(:,:)

    allocate(JZ(two_J + 1, two_J + 1), &
             JP(two_J + 1, two_J + 1), &
             JM(two_J + 1, two_J + 1))

    JZ = (0.0_8, 0.0_8)
    JP = (0.0_8, 0.0_8)
    JM = (0.0_8, 0.0_8)
    
    do i = 1,two_J + 1
        JZ(i,i) = -0.5_8*real(two_J, 8) + real(i-1, 8)*1.0_8
        do k = 1, two_J + 1
            mJ = -0.5_8*real(two_J, 8) + real(k-1, 8)*1.0_8
            if (i == k+1) JP(i,k) = SQRT(0.5_8*real(two_J, 8)&
                                  * (0.5_8*real(two_J, 8)+1)-mJ*(mJ+1.0_8))
            if (i == k-1) JM(i,k) = SQRT(0.5_8*real(two_J, 8)&
                                  * (0.5_8*real(two_J, 8)+1)-mJ*(mJ-1.0_8))
        end do
    end do

end subroutine CALC_JZPM
 
recursive subroutine CALC_POPS(energies, pops, temperature)
    USE constants
    real(kind = 8), intent(in)    :: energies(:), temperature
    real(kind = 8), intent(out)   :: pops(:)
    real(kind = 8)                :: Z
    integer                       :: j

    ! Set partition function and pops to zero
    Z    = 0.0_8
    pops = 0.0_8

    ! Calculate patition function
    do j = 1,SIZE(energies) 
        Z = Z + exp(-energies(j)/(kB * temperature))
    end do

    ! Calculate population
    do j = 1,SIZE(energies) 
        pops(j) = exp(-energies(j)/(kB * temperature))/Z
    end do

end subroutine CALC_POPS

recursive subroutine CALC_TRANS_ENERGIES(energies, upper_e_bound, lower_e_bound, &
                                         trans_energies, trans_indices, num_trans, pops)
    implicit none
    real(kind = 8), intent(in)    :: energies(:), upper_e_bound, lower_e_bound, pops(:)
    real(kind = 8), intent(out)   :: trans_energies(:)
    real(kind = 8)                :: diff
    integer, intent(out)          :: trans_indices(:,:)
    integer, intent(out)          :: num_trans
    integer                       :: i, f

    num_trans = 0
    do f = 1, SIZE(energies)
        do i = 1, SIZE(energies)
            diff = energies(f) - energies(i)
            if (diff > 0.0_8 .AND. diff > lower_e_bound .AND. diff < upper_e_bound .AND. pops(i) > 0.0001_8) then
                num_trans = num_trans + 1
                trans_energies(num_trans) = diff
                trans_indices(num_trans,1) = i
                trans_indices(num_trans,2) = f
            end if
        end do
    end do

end subroutine  CALC_TRANS_ENERGIES

recursive subroutine CALC_SPECTRUM(trans_energies, trans_indices, num_trans, pops, &
                                   B, absorbance, e_axis, width, n_coup_modes)
    implicit none
    real(kind = 8), intent(in)    :: B(:,:), trans_energies(:), pops(:), e_axis(:), width
    real(kind = 8), intent(out)   :: absorbance(:)
    integer, intent(in)           :: trans_indices(:,:), num_trans, &
                                     n_coup_modes
    integer                       :: j, k, i

    ! Loop over transitions
    do k = 1, num_trans
        ! Loop over entire energy range
        do j = 1, SIZE(e_axis)
            absorbance(j) = absorbance(j) + GAUSSIAN(pops(trans_indices(k,1))&
                                    * B(trans_indices(k,2),trans_indices(k,1)),&
                                        trans_energies(k), width, e_axis(j) &
                                        )
        end do
    end do

end subroutine CALC_SPECTRUM


subroutine WRITE_TRANSITION_INFO(trans_energies, trans_indices, basis_labels, tot_vecs)
    implicit none
    integer, intent(in)           :: trans_indices(:,:), basis_labels(:,:)
    integer                       :: j
    real(kind = 8), intent(in)    :: trans_energies(:)
    complex(kind = 8), intent(in) :: tot_vecs(:,:)
    character(len = 500)          :: FMT, header

    ! Make header 
    write(header, '(A)')  'Transitions at first field strength'

    open(66, FILE='firms_sim.out', STATUS='UNKNOWN', POSITION='APPEND')

        call SUB_SECTION_HEADER(header, 66)

        write(66,'(A)') 'States listed have the largest contribution to the eigenvectors of Htotal'
        write(66,*) 

        write(FMT,*) '(A,',('A, I0,',j = 1, n_coup_modes), 'A,',('A, I0,',j = 1, n_coup_modes),')'
        write(66,FMT) '      ENERGY          :  KD',(' n',j, j = 1, n_coup_modes), ' -->  KD',(' n',j, j = 1, n_coup_modes)

        ! Make format identifier for energies and state labels
        write(FMT, '(A, I0, A, I0, A)') '(F15.6, A, SP, I3, SS, ', SIZE(basis_labels,2)-1, 'I3, A, SP, I3, SS, ', SIZE(basis_labels,2)-1, 'I3)'

        do j = 1, SIZE(trans_indices)

            if (trans_indices(j,1) == 0) exit

            write(66,FMT) trans_energies(j), ' cm^-1 : ', basis_labels(MAXLOC(ABS(tot_vecs(:,trans_indices(j,1)))**2),:), ' --> ', basis_labels(MAXLOC(ABS(tot_vecs(:,trans_indices(j,2)))**2),:)

        end do

    close(66)


end subroutine WRITE_TRANSITION_INFO

subroutine SAVE_OUTPUT_DATA(field_strengths, total_absorbance, e_axis, field_lower, &
                            field_upper, field_step, mode_numbers, n_coup_modes)
    implicit none
    real(kind = 8), intent(in) :: field_strengths(:),  total_absorbance(:,:), e_axis(:), &
                                  field_lower, field_upper, field_step
    integer, intent(in)        :: mode_numbers(:), n_coup_modes
    integer                    :: row, col, i
    character(len = 100)       :: FMT
    character(len = 200)       :: filename

    ! Delete old data file
    if (n_coup_modes == 1) then
        write(filename,'(A, I0, A)') 'data_',mode_numbers(1),'.dat'
        call EXECUTE_COMMAND_LINE('rm -f data.dat '//trim(filename))
    else
        write(FMT, *) '(A, I0',(', A, I0',i = 2, n_coup_modes),', A)'
        ! Need to go backwards here because we flip the modes at the start!
        write(filename,FMT) 'data_',mode_numbers(n_coup_modes),('_',mode_numbers(i),i = n_coup_modes - 1, 1, -1),'.dat'
        call EXECUTE_COMMAND_LINE('rm -f data.dat '//trim(filename))
    end if

! Open output file
    open(66, FILE = trim(filename), STATUS = 'UNKNOWN')

        write(FMT, '(A, I0, A)') '(',SIZE(mode_numbers),'I5)'

        write(66,'(3F15.7)') field_lower, field_upper, field_step
        write(66,FMT) mode_numbers
        write(66,*) lower_e_bound, upper_e_bound, resolution

        write(FMT, '(A, I0, A)') '(',SIZE(field_strengths),'E47.36E4)'

        ! Rows run over infrared energies, columns over field values
        do row = 1, SIZE(e_axis)
            write(66, trim(FMT)) (total_absorbance(col, row), col = 1, SIZE(field_strengths))
        end do

    close(66)

end subroutine SAVE_OUTPUT_DATA

function GAUSSIAN(A, B, C, X) result(curve)
    ! Calculates the value of the gaussian with height A, position B, and width C
    ! at position X
    implicit none
    real(kind = 8), intent(in) :: A, B, C, X
    real(kind = 8)             :: curve

    curve = EXP(-(X-B)**2/(2*C**2)) *  A

end function GAUSSIAN

subroutine READ_PHONON_CF_POLYNOMIAL(polynomials_file, cf_parameter_array, n_modes)
    ! Reads in CFP_polynomials.dat
    implicit none
    integer, intent(in)                       :: n_modes
    integer                                   :: q, IDUM, j, col, reason
    real(kind = 8), allocatable, intent(out)  :: cf_parameter_array(:,:,:,:)
    character(len=*), intent(in)              :: polynomials_file

! Read polynomial fits of delta CFPs i.e. the equilibrium CFPs have been removed
!  OEFs are not included (so these are the "real" CFPs)
!  The cf_parameter_array is laid out as
!  (n_modes, num_ranks, num_orders, num_coefficients)
!  Where n_modes is the number of vibrational modes
!  num_ranks and num_orders are the k and q values of the crystal field (1,1) = B_2^-2; (2,3) = B_4^-2 etc.
!    n.b. that elements which contain non-existant parameters such as cf_parameter_array(1, 1, 12) are zero
!  num_coefficients is the number of coefficients in the 3rd order polynomial fit where (1) = a of ax^3 etc

! The array is laid out as follows
    allocate(cf_parameter_array(n_modes,3, 13, 3))
    cf_parameter_array = 0.0_8
    
! Open file
    open(33,FILE = trim(polynomials_file), STATUS = 'OLD')
    ! Loop over modes and read in coefficients for each parameter
        do j = 1,n_modes
            read(33, *, IOSTAT=reason) 
    
        ! Check file is not corrupted or too short
                if (reason > 0)  then
                    call PRINT_ERROR('!!!!                   Error reading CFP_polynomials.dat                    !!!!','')
                else if (reason < 0) then
                    call PRINT_ERROR('!!!!           Error: Reached end of CFP_polynomials.dat too soon           !!!!','')
                end if  
        ! k = 2 q = -2 -> 2
            do q = 1, 5                
                read(33,*, IOSTAT = reason) IDUM, IDUM, (cf_parameter_array(j,1,q,col), col=1, 3)

            ! Check file is not corrupted or too short
                if (reason > 0)  then
                    call PRINT_ERROR('!!!!                   Error reading CFP_polynomials.dat                    !!!!','')
                else if (reason < 0) then
                    call PRINT_ERROR('!!!!           Error: Reached end of CFP_polynomials.dat too soon           !!!!','')
                end if  
            end do

        ! k = 4 q = -4 -> 4
            do q = 1, 9
                read(33,*, IOSTAT = reason) IDUM, IDUM, (cf_parameter_array(j,2,q,col), col=1, 3)

            ! Check file is not corrupted or too short
                if (reason > 0)  then
                    call PRINT_ERROR('!!!!                   Error reading CFP_polynomials.dat                    !!!!','')
                else if (reason < 0) then
                    call PRINT_ERROR('!!!!           Error: Reached end of CFP_polynomials.dat too soon           !!!!','')
                end if  
            end do

        ! k = 6 q = -6 -> 6
            do q = 1, 13
                read(33,*, IOSTAT = reason) IDUM, IDUM, (cf_parameter_array(j,3,q,col), col=1, 3)

            ! Check file is not corrupted or too short
                if (reason > 0)  then
                    call PRINT_ERROR('!!!!                   Error reading CFP_polynomials.dat                    !!!!','')
                else if (reason < 0) then
                    call PRINT_ERROR('!!!!           Error: Reached end of CFP_polynomials.dat too soon           !!!!','')
                end if  
            end do  

        end do
! Close CFP_polynomials.dat
    close(33)

end subroutine READ_PHONON_CF_POLYNOMIAL

subroutine GET_Q_MATRIX(n_vib_states, pho_mat)
    USE matrix_tools
    implicit none
    integer, intent(in)            :: n_vib_states
    integer                        :: n, m
    complex(kind = 8), intent(out) :: pho_mat(:,:)

    pho_mat = (0.0_8, 0.0_8)

    do n = 0, n_vib_states - 1
        do m = 0, n_vib_states - 1
            if (n == m - 1) then
                pho_mat(n + 1, m + 1) = SQRT(real(m,8))
            else if (n == m + 1) then
                pho_mat(n + 1, m + 1) = SQRT((real(m,8)+1.0_8))
            end if
        end do
    end do

end subroutine GET_Q_MATRIX

subroutine PRINT_ERROR(message, message_2)
    ! Print date and time followed by error message
    ! then abort
    implicit none
    character(len = 10)            :: DATE, TIME
    character(len = *), intent(in) :: message, message_2

! Open output file
    open(66, FILE = 'firms_sim.out', STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Get date and time
    call DATE_AND_TIME(DATE = DATE,TIME = TIME)

! Write error message(s) to output file
    write(66,'(A)') trim(message)
    if (message_2 /= '') write(66,'(A)') trim(message_2)

! Write abort time and date to output file
    write(66,'(13A)')'-----------          firms_sim aborts at ',TIME(1:2),':',TIME(3:4),':',TIME(5:6),' on ',DATE(5:6),'-',DATE(7:8),'-',DATE(1:4),'         -----------'

! Close output file and stop program
    close(66)
    STOP 1

end subroutine PRINT_ERROR

subroutine SECTION_HEADER(title, unit)
    ! Writes a section header to unit
    ! Three lines
    ! ---------
    ! - title -
    ! ---------
    implicit none
    character(len = *), intent(in) :: title
    integer, intent(in)            :: unit
    integer                        :: num_l_spaces, num_r_spaces

    num_l_spaces = (70 - len_trim(title)) / 2
    num_r_spaces = (70 - len_trim(title)) / 2

    if (modulo(len_trim(title),2) /= 0) then
        num_l_spaces = num_l_spaces + 1

    end if

    write(unit,'(A)')   repeat('-',80)
    write(unit, '(5A)') repeat('-',5), repeat(' ',num_l_spaces), trim(title), &
                        repeat(' ',num_r_spaces), repeat('-',5)
    write(unit,'(A)')   repeat('-',80)

end subroutine SECTION_HEADER

subroutine SUB_SECTION_HEADER(title, unit)
    ! Writes a subsection section header to unit
    ! Two lines
    !   title
    !   ‾‾‾‾‾
    implicit none
    character(len = *), intent(in) :: title
    integer, intent(in)            :: unit

    integer                        :: num_l_spaces, num_r_spaces

    num_l_spaces = (80 - len_trim(title)) / 2
    num_r_spaces = (80 - len_trim(title)) / 2

    if (modulo(len_trim(title),2) /= 0) then
        num_l_spaces = num_l_spaces + 1

    end if

    write(unit, '(3A)') repeat(' ',num_l_spaces), &
                        trim(title), &
                        repeat(' ',num_r_spaces)

    write(unit,'(3A)')  repeat(' ',num_l_spaces), &
                        repeat('‾',len_trim(title)), &
                        repeat(' ',num_r_spaces)

end subroutine SUB_SECTION_HEADER

subroutine warning(title, unit)
    ! Writes a warning header to unit
    ! Three lines
    ! *********
    ! * title *
    ! *********
    implicit none
    character(len = *), intent(in) :: title
    integer, intent(in)            :: unit

    integer                        :: num_l_spaces, num_r_spaces

    num_l_spaces = (70 - len_trim(title)) / 2
    num_r_spaces = (70 - len_trim(title)) / 2

    if (modulo(len_trim(title),2) /= 0) then
        num_l_spaces = num_l_spaces + 1

    end if

    write(unit,'(A)')   repeat('*',80)
    write(unit, '(5A)') repeat('*',5), repeat(' ',num_l_spaces), trim(title), &
                        repeat(' ',num_r_spaces), repeat('*',5)
    write(unit,'(A)')   repeat('*',80)

end subroutine warning


function GET_TRIMMED_VAL(str, loc_1, loc_2) result(str_out)
    !  Returns the substring located at loc_1:loc_2 of str after
    !  str has been trimmed and adjustled
    implicit none
    integer, intent(in)                :: loc_1, loc_2
    character(len = *), intent(in)     :: str
    character(len = loc_2 - loc_1 + 1) :: str_out
    character(len = len(str))          :: str_tmp

    str_tmp = trim(adjustl(str))

    if (loc_2 - loc_1 + 1 > len_trim(adjustl(str))) then
        ! write(6,'(A)') 'Error in function GET_TRIMMED_VAL'
        ! write(6,'(A)') 'Line is:'
        ! write(6,'(A)') str
        ! write(6,'(A)') 'ABORTING'
        ! STOP
        str_out = ''
    end if

    str_out = str_tmp(loc_1:loc_2)

end function GET_TRIMMED_VAL

function CHECK_SPACES(str) result(num_spaces)
    implicit none
    character(len = *), intent(in) :: str
    character(len = len(str))      :: keep_str
    integer                        :: num_spaces
                                                                  
    ! Move extra spaces and commas
    keep_str = replace_space_comma(str)

    ! Now count number of spaces
    num_spaces = count_char(keep_str, ' ')

end function CHECK_SPACES

function count_char (str, expr) result(num)
    ! counts number of times expr occurs in str
    implicit none
    integer                        :: i, num
    character(len = *), intent(in) :: str, expr
    
    num = 0
    do i=1,len(str)-(len(expr)-1)
        if (str(i:i+len(expr)-1) == expr) then
            num = num + 1
        end if
    end do

end function count_char

function replace_space_comma(str) result(out_str)
    implicit none
    character(len = *)        :: str
    character(len = len(str)) :: out_str

    out_str = str

    ! Replace triple space with single space
    ! out_str = Replace_Text(trim(adjustl(out_str)),'   ',' ', count_char(trim(adjustl(out_str)), '   '))
    
    ! Replace double space with single space
    ! out_str = Replace_Text(trim(adjustl(out_str)),'  ',' ', count_char(trim(adjustl(out_str)), '  '))

    ! Replace ' ,'
    out_str = Replace_Text(trim(adjustl(out_str)),' ,',',', &
                           count_char(trim(adjustl(out_str)), ' ,'))

    ! Replace ','
    out_str = Replace_Text(trim(adjustl(out_str)),',',' ', &
                           count_char(trim(adjustl(out_str)), ','))

    ! Replace triple space with single space
    out_str = Replace_Text(trim(adjustl(out_str)),'   ',' ', &
                           count_char(trim(adjustl(out_str)), '   '))
    
    ! Replace double space with single space
    out_str = Replace_Text(trim(adjustl(out_str)),'  ',' ', &
                           count_char(trim(adjustl(out_str)), '  '))

end function replace_space_comma

function replace_text(str, old_expr, new_expr, num_occ) result(str_mod)
    ! Replaces old_expr in str with new_expr
    implicit none
    integer                                                                 :: i, num_occ
    character(len = *), intent(in)                                          :: str, old_expr, &
                                                                               new_expr
    character(len = len(str))                                               :: str_keep
    character(len = len(str)+(num_occ*ABS(len(old_expr) - len(new_expr))))  :: str_mod

    str_mod = str

        do i=1,len(str_keep)-(len(old_expr)-1)
            str_keep = str_mod
            if (str_keep(i:i+len(old_expr)-1) == old_expr) then
                str_mod = str_keep(:i-1)//new_expr//str_keep(i+len(old_expr):)
            end if
        end do

end function replace_text

subroutine error_header(title, unit)
    ! Writes an error header to unit
    ! Three lines
    ! !!!!!!!!!
    ! ! title !
    ! !!!!!!!!!
    implicit none
    character(len = *), intent(in) :: title
    integer, intent(in)            :: unit
    integer                        :: n_l_spaces, n_r_spaces, width

    if (len_trim(title) > 60) then
        width = len_trim(title) + 20
    else
        width = 70
    end if

    n_l_spaces = (width - len_trim(title)) / 2
    n_r_spaces = (width - len_trim(title)) / 2

    if (modulo(len_trim(title),2) /= 0) then
        n_l_spaces = n_l_spaces + 1
        width = width
    end if

    write(unit,'(A)')   repeat('!',width+10)
    write(unit, '(5A)') repeat('!',5), repeat(' ',n_l_spaces), trim(title), &
                        repeat(' ',n_r_spaces), repeat('!',5)
    write(unit,'(A)')   repeat('!',width+10)

end subroutine error_header

subroutine print_date_and_time(FLAG)
    ! Print date and time using intrinsic
    ! Two different flags for different uses
    ! Start, Finish
    implicit none
    character(len = 10)           :: DATE, TIME
    character(len = *), intent(in) :: FLAG

! Open output file
    if (FLAG=='FNSH') open(66, FILE = "firms_sim.out", STATUS = 'UNKNOWN', POSITION = 'APPEND')

! Get date and time
    call DATE_AND_TIME(DATE = DATE,TIME = TIME)

! Write start or finish message to file
! if finish then stop program
    if (trim(FLAG) == 'STRT') then 
        write(66,'(13A)')'-----------        firms_sim begins at ',TIME(1:2),':',&
                          TIME(3:4),':',TIME(5:6),' on ',DATE(7:8),'-',&
                          DATE(5:6),'-',DATE(1:4),'        -----------'
        write(66,'(A)') ''
    else if (trim(FLAG) == 'FNSH') then
        write(66,'(A)') ''
        write(66,'(13A)')'-----------    firms_sim ends normally at ',TIME(1:2),':',&
                          TIME(3:4),':',TIME(5:6),' on ',DATE(7:8),'-',&
                          DATE(5:6),'-',DATE(1:4),'    -----------'
        close(66)
        STOP
    end if

! Close output file
    if (FLAG=='FNSH') close(66)

end subroutine print_date_and_time

SUBROUTINE SETUP_HDF5
    IMPLICIT NONE
    INTEGER(8) :: real_size, real_complex_size
    
    real_size = (storage_size(1_8, 8) / 8)
    real_complex_size =  real_size * 2_8
    
    ! Initialize FORTRAN interface.
    CALL h5open_f(hdf5_error)

    ! Create the memory data type.
    CALL H5Tcreate_f(H5T_COMPOUND_F, real_complex_size, hdf5_complex_datatype,hdf5_error)
    CALL H5Tinsert_f( hdf5_complex_datatype, "r", &
       0_8, h5kind_to_type(8,H5_REAL_KIND), hdf5_error)
    CALL H5Tinsert_f( hdf5_complex_datatype, "i", &
       real_size, h5kind_to_type(8,H5_REAL_KIND), hdf5_error)
    
END SUBROUTINE SETUP_HDF5

SUBROUTINE CLOSE_HDF5
    IMPLICIT NONE
    CALL H5Tclose_f(hdf5_complex_datatype, hdf5_error)
END SUBROUTINE

SUBROUTINE GET_HDF5_DIMENSION(dimension)
    IMPLICIT NONE
    INTEGER, INTENT(OUT) :: dimension
    INTEGER(HID_T)   :: hdf5_data_id, hdf5_file_id, hdf5_dspace_id
    INTEGER(HSIZE_T), dimension(1) :: hdf5_check_dims,hdf5_check_maxdims
    INTEGER :: hdf5_error
    ! Open the file
    CALL H5Fopen_f("tau.hdf5", H5F_ACC_RDONLY_F, hdf5_file_id, hdf5_error)
    ! Read EQ Eigenstate Energies
    CALL H5Dopen_f(hdf5_file_id, "energies", hdf5_data_id, hdf5_error)
    CALL H5Dget_space_f(hdf5_data_id,hdf5_dspace_id,hdf5_error)
    CALL H5Sget_simple_extent_dims_f(hdf5_dspace_id, hdf5_check_dims, hdf5_check_maxdims, hdf5_error)
    dimension = hdf5_check_dims(1)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    CALL H5Fclose_f(hdf5_file_id, hdf5_error)
END SUBROUTINE GET_HDF5_DIMENSION

SUBROUTINE READ_EQ_HAMILTONIAN_HDF5(dimension,vals)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: dimension
    REAL(KIND = 8), ALLOCATABLE, INTENT(OUT)    :: vals(:)
    INTEGER(HSIZE_T), DIMENSION(1:1) :: dim
    REAL(kind=8) :: energies(1:dimension)
    INTEGER(HID_T)   :: hdf5_data_id, hdf5_file_id, hdf5_dspace_id
    INTEGER(HSIZE_T), dimension(1) :: hdf5_check_dims,hdf5_check_maxdims
    INTEGER :: hdf5_error, i
    dim = dimension
    allocate(vals(dimension))
    ! Open the file
    CALL H5Fopen_f("tau.hdf5", H5F_ACC_RDONLY_F, hdf5_file_id, hdf5_error)
    ! Read EQ Eigenstate Energies
    CALL H5Dopen_f(hdf5_file_id, "energies", hdf5_data_id, hdf5_error)
    CALL H5Dget_space_f(hdf5_data_id,hdf5_dspace_id,hdf5_error)
    CALL H5Sget_simple_extent_dims_f(hdf5_dspace_id, hdf5_check_dims, hdf5_check_maxdims, hdf5_error)
    IF(hdf5_check_dims(1) /= dim(1)) THEN
        CALL PRINT_ERROR('Given dimension does not match HDF5 file', 'Check file')
    END IF
    CALL H5Dread_f(hdf5_data_id, H5T_NATIVE_DOUBLE, energies, dim, hdf5_error)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    vals = energies
    CALL H5Fclose_f(hdf5_file_id, hdf5_error)
    ! Symmetrise eigenvalues if a Kramers system
    IF(hdf5_kramers) THEN
        DO i = 1,dimension-1,2
            vals(i) = (vals(i)+vals(i+1))*0.5_8
            vals(i+1) = vals(i)
        END DO
    END IF
END SUBROUTINE READ_EQ_HAMILTONIAN_HDF5

SUBROUTINE READ_ANGMOM_HDF5(dimension,spin,angmom)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: dimension
    COMPLEX(KIND = 8), ALLOCATABLE, INTENT(OUT)    :: spin(:,:,:), angmom(:,:,:)
    INTEGER(HID_T)   :: hdf5_data_id, hdf5_file_id, hdf5_group_id
    INTEGER :: hdf5_error
    COMPLEX(KIND=8), DIMENSION(1:dimension,1:dimension), TARGET :: hdf5_data_buffer
    TYPE(C_PTR) :: hdf5_ptr

    allocate(spin(3,dimension,dimension),angmom(3,dimension,dimension))

    ! Open the file, group and the dataset
    CALL H5Fopen_f("tau.hdf5", H5F_ACC_RDONLY_F, hdf5_file_id, hdf5_error)
    CALL H5Gopen_f(hdf5_file_id,"spin",hdf5_group_id,hdf5_error)
    CALL H5Dopen_f(hdf5_group_id, "x", hdf5_data_id, hdf5_error)
    
    ! Read the data
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    spin(1,:,:) = transpose(hdf5_data_buffer)
    
    ! Close the dataset
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    
    ! Rinse and repeat
    CALL H5Dopen_f(hdf5_group_id, "y", hdf5_data_id, hdf5_error)
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    spin(2,:,:) = transpose(hdf5_data_buffer)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    CALL H5Dopen_f(hdf5_group_id, "z", hdf5_data_id, hdf5_error)
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    spin(3,:,:) = transpose(hdf5_data_buffer)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    
    ! Close the group
    CALL H5Gclose_f(hdf5_group_id, hdf5_error)

    ! Repeat for angmom
    CALL H5Gopen_f(hdf5_file_id,"angmom",hdf5_group_id,hdf5_error)
    CALL H5Dopen_f(hdf5_group_id, "x", hdf5_data_id, hdf5_error)
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    angmom(1,:,:) = transpose(hdf5_data_buffer)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    CALL H5Dopen_f(hdf5_group_id, "y", hdf5_data_id, hdf5_error)
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    angmom(2,:,:) = transpose(hdf5_data_buffer)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    CALL H5Dopen_f(hdf5_group_id, "z", hdf5_data_id, hdf5_error)
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    angmom(3,:,:) = transpose(hdf5_data_buffer)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)
    CALL H5Gclose_f(hdf5_group_id, hdf5_error)
    
    ! Close the file
    CALL H5Fclose_f(hdf5_file_id, hdf5_error)

    ! Symmetrise spin and angmom operators
    spin(1,:,:) = (spin(1,:,:) + transpose(conjg(spin(1,:,:))))*0.5_8
    spin(2,:,:) = (spin(2,:,:) + transpose(conjg(spin(2,:,:))))*0.5_8
    spin(3,:,:) = (spin(3,:,:) + transpose(conjg(spin(3,:,:))))*0.5_8
    angmom(1,:,:) = (angmom(1,:,:) + transpose(conjg(angmom(1,:,:))))*0.5_8
    angmom(2,:,:) = (angmom(2,:,:) + transpose(conjg(angmom(2,:,:))))*0.5_8
    angmom(3,:,:) = (angmom(3,:,:) + transpose(conjg(angmom(3,:,:))))*0.5_8
END SUBROUTINE READ_ANGMOM_HDF5

SUBROUTINE READ_PHONON_MATRIX_HDF5(dimension,mode_num,phonon_matrix)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: dimension, mode_num
    COMPLEX(KIND = 8), INTENT(OUT)    :: phonon_matrix(:,:)
    INTEGER(HID_T)   :: hdf5_data_id, hdf5_file_id, hdf5_group_id
    INTEGER :: hdf5_error, i
    COMPLEX(KIND=8), DIMENSION(1:dimension,1:dimension), TARGET :: hdf5_data_buffer
    TYPE(C_PTR) :: hdf5_ptr
    CHARACTER(len=10) :: mode_char

    ! Open the file and group
    CALL H5Fopen_f("tau.hdf5", H5F_ACC_RDONLY_F, hdf5_file_id, hdf5_error)
    CALL H5Gopen_f(hdf5_file_id,"couplings",hdf5_group_id,hdf5_error)

    ! Read data for mode_num
    WRITE(mode_char,'(I10)') mode_num
    mode_char = ADJUSTL(mode_char)
    CALL H5Dopen_f(hdf5_group_id, TRIM(mode_char), hdf5_data_id, hdf5_error)
    hdf5_ptr = C_LOC(hdf5_data_buffer(1,1))
    CALL H5Dread_f(hdf5_data_id, hdf5_complex_datatype, hdf5_ptr, hdf5_error)
    phonon_matrix(:,:) = transpose(hdf5_data_buffer)
    CALL H5Dclose_f(hdf5_data_id, hdf5_error)

    ! Close group
    CALL H5Gclose_f(hdf5_group_id, hdf5_error)
    
    ! Close the file
    CALL H5Fclose_f(hdf5_file_id, hdf5_error)
END SUBROUTINE READ_PHONON_MATRIX_HDF5

end program firms_sim

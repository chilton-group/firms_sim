MODULE STEV_OPS 
    USE, INTRINSIC :: iso_fortran_env
    USE matrix_tools
    IMPLICIT NONE
    
    INTERFACE GET_STEVENS_OPERATORS
    ! Creates Stevens operators and returns Stevens operators with
    !  with or without the angular momentum matrices
        MODULE PROCEDURE STEV_DP
        MODULE PROCEDURE STEV_QP
        MODULE PROCEDURE STEV_WITH_ANG_OPS_DP
        MODULE PROCEDURE STEV_WITH_ANG_OPS_QP
    END INTERFACE GET_STEVENS_OPERATORS

    INTERFACE STEVENS2
        ! Creates 2nd rank Stevens operators
        MODULE PROCEDURE STEVENS2_DP
        MODULE PROCEDURE STEVENS2_QP
    END INTERFACE STEVENS2

    INTERFACE STEVENS4
        ! Creates 4th rank Stevens operators
        MODULE PROCEDURE STEVENS4_DP
        MODULE PROCEDURE STEVENS4_QP
    END INTERFACE STEVENS4

    INTERFACE STEVENS6
        ! Creates 6th rank Stevens operators
        MODULE PROCEDURE STEVENS6_DP
        MODULE PROCEDURE STEVENS6_QP
    END INTERFACE STEVENS6

    INTERFACE H_CF
        ! Calculates Crystal Field Hamiltonian
        MODULE PROCEDURE H_CF_NOOEF_DP
        MODULE PROCEDURE H_CF_NOOEF_QP
        MODULE PROCEDURE H_CF_DP
        MODULE PROCEDURE H_CF_QP
    END INTERFACE H_CF

    INTERFACE STE_2_WYB
        !Converts Stevens parameters to Wybourne
        MODULE PROCEDURE STE_2_WYB_DP
        MODULE PROCEDURE STE_2_WYB_QP
    END INTERFACE STE_2_WYB

    ! INTERFACE ROTATE_CFP
    !     ! Rotate Crystal Field Parameters
    !     MODULE PROCEDURE ROTATE_CFP_DP
    !     MODULE PROCEDURE ROTATE_CFP_QP
    ! END INTERFACE ROTATE_CFP

    ! INTERFACE STRENGTH_S
    !     ! Calculate strength parameter S
    !     MODULE PROCEDURE STRENGTH_S_DP
    !     MODULE PROCEDURE STRENGTH_S_QP
    ! END INTERFACE STRENGTH_S

    ! INTERFACE STRENGTH_SK
    !     ! Calculate strength parameter Sk
    !     MODULE PROCEDURE STRENGTH_SK_DP
    !     MODULE PROCEDURE STRENGTH_SK_QP
    ! END INTERFACE STRENGTH_SK

    ! INTERFACE STRENGTH_SQ
    !     ! Calculate strength parameter Sq
    !     MODULE PROCEDURE STRENGTH_SQ_DP
    !     MODULE PROCEDURE STRENGTH_SQ_QP
    ! END INTERFACE STRENGTH_SQ

CONTAINS

    SUBROUTINE STEV_QP(twoJ, StevensOperators)
        !Calculate and return stevens operators at quadruple precision
        IMPLICIT NONE
        INTEGER, INTENT(IN)                            :: twoJ
        INTEGER                                        :: i, k
        REAL(KIND = 16)                                :: mJ
        COMPLEX(KIND = 16), ALLOCATABLE, INTENT(OUT)   :: StevensOperators(:,:,:,:)
        COMPLEX(KIND = 16), ALLOCATABLE                :: JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        ALLOCATE(JP(twoJ+1,twoJ+1),JM(twoJ+1,twoJ+1),J2(twoJ+1,twoJ+1),JZ(twoJ+1,twoJ+1),StevensOperators(3, 13,twoJ+1,twoJ+1))
    
        JP               = (0.0_16,0.0_16)
        JM               = (0.0_16,0.0_16)
        J2               = (0.0_16,0.0_16)
        JZ               = (0.0_16,0.0_16)
        StevensOperators = (0.0_16,0.0_16)
    
        DO i = 1,twoJ+1
            JZ(i,i) = -0.5_16*twoJ + (i-1)*1.0_16
            J2(i,i) = 0.5_16*twoJ*(0.5_16*twoJ+1)
            DO k = 1,twoJ+1
                mJ = -0.5_16*twoJ + (k-1)*1.0_16
                IF (i == k+1) JP(i,k) = SQRT(0.5_16*twoJ*(0.5_16*twoJ+1)-mJ*(mJ+1.0_16))
                IF (i == k-1) JM(i,k) = SQRT(0.5_16*twoJ*(0.5_16*twoJ+1)-mJ*(mJ-1.0_16))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,StevensOperators(1, 1,:,:),StevensOperators(1, 2,:,:),StevensOperators(1, 3,:,:),StevensOperators(1, 4,:,:),StevensOperators(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,StevensOperators(2, 1,:,:),StevensOperators(2, 2,:,:),StevensOperators(2, 3,:,:),StevensOperators(2, 4,:,:),StevensOperators(2, 5,:,:),StevensOperators(2, 6,:,:),StevensOperators(2, 7,:,:),StevensOperators(2, 8,:,:),StevensOperators(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,StevensOperators(3, 1,:,:),StevensOperators(3, 2,:,:),StevensOperators(3, 3,:,:),StevensOperators(3, 4,:,:),StevensOperators(3, 5,:,:),StevensOperators(3, 6,:,:),StevensOperators(3, 7,:,:),StevensOperators(3, 8,:,:),StevensOperators(3, 9,:,:),StevensOperators(3, 10,:,:),StevensOperators(3, 11, :,:),StevensOperators(3, 12,:,:),StevensOperators(3, 13,:,:))
        
    END SUBROUTINE STEV_QP

    SUBROUTINE STEV_DP(twoJ, StevensOperators)
        !Calculate and return stevens operators at double precision
        IMPLICIT NONE
        INTEGER, INTENT(IN)                           :: twoJ
        INTEGER                                       :: i, k
        REAL(KIND = 8)                                :: mJ
        COMPLEX(KIND = 8), ALLOCATABLE, INTENT(OUT)   :: StevensOperators(:,:,:,:)
        COMPLEX(KIND = 8), ALLOCATABLE                :: JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        !Allocate arrays
        ALLOCATE(JP(twoJ+1,twoJ+1),JM(twoJ+1,twoJ+1),J2(twoJ+1,twoJ+1),JZ(twoJ+1,twoJ+1),StevensOperators(3, 13,twoJ+1,twoJ+1))
    
        JP               = (0.0_8,0.0_8)
        JM               = (0.0_8,0.0_8)
        J2               = (0.0_8,0.0_8)
        JZ               = (0.0_8,0.0_8)
        StevensOperators = (0.0_8,0.0_8)
    
        DO i = 1,twoJ+1
            JZ(i,i) = -0.5_8*twoJ + (i-1)*1.0_8
            J2(i,i) = 0.5_8*twoJ*(0.5_8*twoJ+1)
            DO k = 1,twoJ+1
                mJ = -0.5_8*twoJ + (k-1)*1.0_8
                IF (i == k+1) JP(i,k) = SQRT(0.5_8*twoJ*(0.5_8*twoJ+1)-mJ*(mJ+1.0_8))
                IF (i == k-1) JM(i,k) = SQRT(0.5_8*twoJ*(0.5_8*twoJ+1)-mJ*(mJ-1.0_8))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,StevensOperators(1, 1,:,:),StevensOperators(1, 2,:,:),StevensOperators(1, 3,:,:),StevensOperators(1, 4,:,:),StevensOperators(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,StevensOperators(2, 1,:,:),StevensOperators(2, 2,:,:),StevensOperators(2, 3,:,:),StevensOperators(2, 4,:,:),StevensOperators(2, 5,:,:),StevensOperators(2, 6,:,:),StevensOperators(2, 7,:,:),StevensOperators(2, 8,:,:),StevensOperators(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,StevensOperators(3, 1,:,:),StevensOperators(3, 2,:,:),StevensOperators(3, 3,:,:),StevensOperators(3, 4,:,:),StevensOperators(3, 5,:,:),StevensOperators(3, 6,:,:),StevensOperators(3, 7,:,:),StevensOperators(3, 8,:,:),StevensOperators(3, 9,:,:),StevensOperators(3, 10,:,:),StevensOperators(3, 11, :,:),StevensOperators(3, 12,:,:),StevensOperators(3, 13,:,:))
        
    END SUBROUTINE STEV_DP

    SUBROUTINE STEV_WITH_ANG_OPS_QP(twoJ, StevensOperators, JP, JM, J2, JZ)
        !Calculate and return stevens operators at quadruple precision
        !Also returns angular momentum operators
        IMPLICIT NONE
        INTEGER, INTENT(IN)                            :: twoJ
        INTEGER                                        :: i, k
        REAL(KIND = 16)                                :: mJ
        COMPLEX(KIND = 16), ALLOCATABLE, INTENT(OUT)   :: StevensOperators(:,:,:,:), JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        ALLOCATE(JP(twoJ+1,twoJ+1),JM(twoJ+1,twoJ+1),J2(twoJ+1,twoJ+1),JZ(twoJ+1,twoJ+1),StevensOperators(3, 13,twoJ+1,twoJ+1))
    
        JP               = (0.0_16,0.0_16)
        JM               = (0.0_16,0.0_16)
        J2               = (0.0_16,0.0_16)
        JZ               = (0.0_16,0.0_16)
        StevensOperators = (0.0_16,0.0_16)
    
        DO i = 1,twoJ+1
            JZ(i,i) = -0.5_16*twoJ + (i-1)*1.0_16
            J2(i,i) = 0.5_16*twoJ*(0.5_16*twoJ+1)
            DO k = 1,twoJ+1
                mJ = -0.5_16*twoJ + (k-1)*1.0_16
                IF (i == k+1) JP(i,k) = SQRT(0.5_16*twoJ*(0.5_16*twoJ+1)-mJ*(mJ+1.0_16))
                IF (i == k-1) JM(i,k) = SQRT(0.5_16*twoJ*(0.5_16*twoJ+1)-mJ*(mJ-1.0_16))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,StevensOperators(1, 1,:,:),StevensOperators(1, 2,:,:),StevensOperators(1, 3,:,:),StevensOperators(1, 4,:,:),StevensOperators(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,StevensOperators(2, 1,:,:),StevensOperators(2, 2,:,:),StevensOperators(2, 3,:,:),StevensOperators(2, 4,:,:),StevensOperators(2, 5,:,:),StevensOperators(2, 6,:,:),StevensOperators(2, 7,:,:),StevensOperators(2, 8,:,:),StevensOperators(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,StevensOperators(3, 1,:,:),StevensOperators(3, 2,:,:),StevensOperators(3, 3,:,:),StevensOperators(3, 4,:,:),StevensOperators(3, 5,:,:),StevensOperators(3, 6,:,:),StevensOperators(3, 7,:,:),StevensOperators(3, 8,:,:),StevensOperators(3, 9,:,:),StevensOperators(3, 10,:,:),StevensOperators(3, 11, :,:),StevensOperators(3, 12,:,:),StevensOperators(3, 13,:,:))
        
    END SUBROUTINE STEV_WITH_ANG_OPS_QP

    SUBROUTINE STEV_WITH_ANG_OPS_DP(twoJ, StevensOperators, JP, JM, J2, JZ)
        !Calculate and return stevens operators at double precision
        !Also returns angular momentum operators
        IMPLICIT NONE
        INTEGER, INTENT(IN)                            :: twoJ
        INTEGER                                        :: i, k
        REAL(KIND = 8)                                 :: mJ
        COMPLEX(KIND = 8), ALLOCATABLE, INTENT(OUT)    :: StevensOperators(:,:,:,:), JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        !Allocate arrays
        ALLOCATE(JP(twoJ+1,twoJ+1),JM(twoJ+1,twoJ+1),J2(twoJ+1,twoJ+1),JZ(twoJ+1,twoJ+1),StevensOperators(3, 13,twoJ+1,twoJ+1))
    
        JP               = (0.0_8,0.0_8)
        JM               = (0.0_8,0.0_8)
        J2               = (0.0_8,0.0_8)
        JZ               = (0.0_8,0.0_8)
        StevensOperators = (0.0_8,0.0_8)
    
        DO i = 1,twoJ+1
            JZ(i,i) = -0.5_8*twoJ + (i-1)*1.0_8
            J2(i,i) = 0.5_8*twoJ*(0.5_8*twoJ+1)
            DO k = 1,twoJ+1
                mJ = -0.5_8*twoJ + (k-1)*1.0_8
                IF (i == k+1) JP(i,k) = SQRT(0.5_8*twoJ*(0.5_8*twoJ+1)-mJ*(mJ+1.0_8))
                IF (i == k-1) JM(i,k) = SQRT(0.5_8*twoJ*(0.5_8*twoJ+1)-mJ*(mJ-1.0_8))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,StevensOperators(1, 1,:,:),StevensOperators(1, 2,:,:),StevensOperators(1, 3,:,:),StevensOperators(1, 4,:,:),StevensOperators(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,StevensOperators(2, 1,:,:),StevensOperators(2, 2,:,:),StevensOperators(2, 3,:,:),StevensOperators(2, 4,:,:),StevensOperators(2, 5,:,:),StevensOperators(2, 6,:,:),StevensOperators(2, 7,:,:),StevensOperators(2, 8,:,:),StevensOperators(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,StevensOperators(3, 1,:,:),StevensOperators(3, 2,:,:),StevensOperators(3, 3,:,:),StevensOperators(3, 4,:,:),StevensOperators(3, 5,:,:),StevensOperators(3, 6,:,:),StevensOperators(3, 7,:,:),StevensOperators(3, 8,:,:),StevensOperators(3, 9,:,:),StevensOperators(3, 10,:,:),StevensOperators(3, 11, :,:),StevensOperators(3, 12,:,:),StevensOperators(3, 13,:,:))
        
    END SUBROUTINE STEV_WITH_ANG_OPS_DP

    SUBROUTINE STEVENS2_DP(JP,JM,J2,JZ,O2M2,O2M1,O20,O21,O22)
        !Creates 2nd rank Stevens operators at double precision
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)   :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 8), INTENT(OUT)  :: O2M2(:,:),O2M1(:,:),O20(:,:),O21(:,:),O22(:,:)
        COMPLEX(KIND = 8)               :: ii
        
        ii = (0.0_8, 1.0_8)

        O2m2 = -0.5_8  * ii * (MAT_POWER(JP,2) - MAT_POWER(JM,2))
        O2m1 = -0.25_8 * ii * (ALT_MATMUL(JZ,(JP-JM)) + ALT_MATMUL((JP-JM),JZ))
        O20  =  3.0_8  *       MAT_POWER(JZ, 2) - J2
        O21  =  0.25_8 *      (ALT_MATMUL(JZ,(JP+JM)) + ALT_MATMUL((JP+JM),JZ))
        O22  =  0.5_8  *      (MAT_POWER(JP,2) + MAT_POWER(JM,2))

    END SUBROUTINE STEVENS2_DP
    

    SUBROUTINE STEVENS2_QP(JP,JM,J2,JZ,O2M2,O2M1,O20,O21,O22)
        !Creates 2nd rank Stevens operators at quadruple precision
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)   :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 16), INTENT(OUT)  :: O2M2(:,:),O2M1(:,:),O20(:,:),O21(:,:),O22(:,:)
        COMPLEX(KIND = 16)               :: ii
        
        ii = (0.0_16, 1.0_16)

        O2m2 = -0.5_16  * ii * (MAT_POWER(JP,2) - MAT_POWER(JM,2))
        O2m1 = -0.25_16 * ii * (ALT_MATMUL(JZ,(JP-JM)) + ALT_MATMUL((JP-JM),JZ))
        O20  =  3.0_16  *       MAT_POWER(JZ, 2) - J2
        O21  =  0.25_16 *      (ALT_MATMUL(JZ,(JP+JM)) + ALT_MATMUL((JP+JM),JZ))
        O22  =  0.5_16  *      (MAT_POWER(JP,2) + MAT_POWER(JM,2))

    END SUBROUTINE STEVENS2_QP
        
    SUBROUTINE STEVENS4_DP(JP,JM,J2,JZ,O4M4,O4M3,O4M2,O4M1,O40,O41,O42,O43,O44)
        !Creates 4th rank Stevens operators at double precision
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)    :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 8), INTENT(INOUT) :: O4M4(:,:),O4M3(:,:),O4M2(:,:),O4M1(:,:),O40(:,:),O41(:,:),O42(:,:),O43(:,:),O44(:,:)
        COMPLEX(KIND = 8)                :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                          :: J
        COMPLEX(KIND = 8)                :: ii
        
        ii = (0.0_8, 1.0_8)

        IDENT = (0.0_8,0.0_8)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_8,0.0_8)
        END DO
    
        O4m4 = -0.5_8  * ii * (MAT_POWER(JP,4) - MAT_POWER(JM,4))
        O4m3 = -0.25_8 * ii * (ALT_MATMUL(JZ, (MAT_POWER(JP,3) - MAT_POWER(JM,3))) + ALT_MATMUL((MAT_POWER(JP,3) - MAT_POWER(JM,3)), JZ))
        O4m2 = -0.25_8 * ii * (ALT_MATMUL(7.0_8 * MAT_POWER(JZ,2) - J2 - 5.0_8 * IDENT, MAT_POWER(JP, 2) - MAT_POWER(JM, 2)) + ALT_MATMUL(MAT_POWER(JP, 2) - MAT_POWER(JM, 2), 7.0_8 * MAT_POWER(JZ,2) - J2 - 5.0_8 * IDENT))
        O4m1 = -0.25_8 * ii * (ALT_MATMUL(7.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2, JZ)  - JZ, JP - JM) + ALT_MATMUL(JP - JM, 7.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2, JZ)  - JZ))
        O40  = 35.0_8  * MAT_POWER(JZ,4) - 30.0_8 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) + 25.0_8*MAT_POWER(JZ,2) + 3.0_8 * MAT_POWER(J2,2) - 6.0_8 * J2
        O41  =  0.25_8 *     (ALT_MATMUL(7.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2, JZ) - JZ, JP + JM) + ALT_MATMUL(JP + JM, 7.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2, JZ) - JZ))
        O42  =  0.25_8 *      (ALT_MATMUL(7.0_8 * MAT_POWER(JZ,2) - J2 - 5.0_8 * IDENT, MAT_POWER(JP, 2) + MAT_POWER(JM, 2)) + ALT_MATMUL(MAT_POWER(JP, 2) + MAT_POWER(JM, 2), 7.0_8 * MAT_POWER(JZ,2) - J2 - 5.0_8 * IDENT))
        O43  =  0.25_8 *      (ALT_MATMUL(JZ, (MAT_POWER(JP,3) + MAT_POWER(JM,3))) + ALT_MATMUL((MAT_POWER(JP,3) + MAT_POWER(JM,3)), JZ))
        O44  =  0.5_8  *      (MAT_POWER(JP,4) + MAT_POWER(JM,4))

    END SUBROUTINE STEVENS4_DP

    SUBROUTINE STEVENS4_QP(JP,JM,J2,JZ,O4M4,O4M3,O4M2,O4M1,O40,O41,O42,O43,O44)
        !Creates 4th rank Stevens operators at quadruple precision
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)    :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 16), INTENT(INOUT) :: O4M4(:,:),O4M3(:,:),O4M2(:,:),O4M1(:,:),O40(:,:),O41(:,:),O42(:,:),O43(:,:),O44(:,:)
        COMPLEX(KIND = 16)                :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                           :: J
        COMPLEX(KIND = 16)                :: ii
        
        ii = (0.0_16, 1.0_16)

        IDENT = (0.0_16,0.0_16)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_16,0.0_16)
        END DO
    
        O4m4 = -0.5_16  * ii * (MAT_POWER(JP,4) - MAT_POWER(JM,4))
        O4m3 = -0.25_16 * ii * (ALT_MATMUL(JZ, (MAT_POWER(JP,3) - MAT_POWER(JM,3))) + ALT_MATMUL((MAT_POWER(JP,3) - MAT_POWER(JM,3)), JZ))
        O4m2 = -0.25_16 * ii * (ALT_MATMUL(7.0_16 * MAT_POWER(JZ,2) - J2 - 5.0_16 * IDENT, MAT_POWER(JP, 2) - MAT_POWER(JM, 2)) + ALT_MATMUL(MAT_POWER(JP, 2) - MAT_POWER(JM, 2), 7.0_16 * MAT_POWER(JZ,2) - J2 - 5.0_16 * IDENT))
        O4m1 = -0.25_16 * ii * (ALT_MATMUL(7.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2, JZ) - JZ, JP - JM) + ALT_MATMUL(JP - JM, 7.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2, JZ)  - JZ))
        O40  = 35.0_16  * MAT_POWER(JZ,4) - 30.0_16 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) + 25.0_16*MAT_POWER(JZ,2) + 3.0_16 * MAT_POWER(J2,2) - 6.0_16 * J2
        O41  =  0.25_16 *      (ALT_MATMUL(7.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2, JZ) - JZ, JP + JM) + ALT_MATMUL(JP + JM, 7.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2, JZ) - JZ))
        O42  =  0.25_16 *      (ALT_MATMUL(7.0_16 * MAT_POWER(JZ,2) - J2 - 5.0_16 * IDENT, MAT_POWER(JP, 2) + MAT_POWER(JM, 2)) + ALT_MATMUL(MAT_POWER(JP, 2) + MAT_POWER(JM, 2), 7.0_16 * MAT_POWER(JZ,2) - J2 - 5.0_16 * IDENT))
        O43  =  0.25_16 *      (ALT_MATMUL(JZ, (MAT_POWER(JP,3) + MAT_POWER(JM,3))) + ALT_MATMUL((MAT_POWER(JP,3) + MAT_POWER(JM,3)), JZ))
        O44  =  0.5_16  *      (MAT_POWER(JP,4) + MAT_POWER(JM,4))
    
    END SUBROUTINE STEVENS4_QP

    SUBROUTINE STEVENS6_DP(JP,JM,J2,JZ,O6M6,O6M5,O6M4,O6M3,O6M2,O6M1,O60,O61,O62,O63,O64,O65,O66)
        !Creates 6th rank Stevens operators at double precision
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)  :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 8), INTENT(OUT) :: O6M6(:,:),O6M5(:,:),O6M4(:,:),O6M3(:,:),O6M2(:,:),O6M1(:,:),O60(:,:),O61(:,:),O62(:,:),O63(:,:),O64(:,:),O65(:,:),O66(:,:)
        COMPLEX(KIND = 8)              :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                        :: J
        COMPLEX(KIND = 8)              :: ii
        
        ii = (0.0_8, 1.0_8)
    
        IDENT = (0.0_8,0.0_8)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_8,0.0_8)
        END DO
    
        O6M6 =  -0.5_8  * ii * (MAT_POWER(JP,6) - MAT_POWER(JM,6))
        O6M5 =  -0.25_8 * ii * (ALT_MATMUL((MAT_POWER(JP,5) - MAT_POWER(JM,5)),Jz) + ALT_MATMUL(JZ, (MAT_POWER(JP,5) - MAT_POWER(JM,5))))
        O6M4 =  -0.25_8 * ii * (ALT_MATMUL(MAT_POWER(JP,4) - MAT_POWER(JM,4),(11.0_8 * MAT_POWER(JZ,2) - J2 - 38.0_8 * IDENT)) + ALT_MATMUL((11.0_8 * MAT_POWER(JZ,2) - J2 - 38.0_8 * IDENT), MAT_POWER(JP,4) - MAT_POWER(JM,4)))
        O6M3 =  -0.25_8 * ii * (ALT_MATMUL(MAT_POWER(JP,3) - MAT_POWER(JM,3),(11.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2,JZ) - 59.0_8*JZ)) + ALT_MATMUL((11.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2,JZ) - 59.0_8 * JZ), MAT_POWER(JP,3) - MAT_POWER(JM,3)))
        O6M2 =  -0.25_8 * ii * (ALT_MATMUL(MAT_POWER(JP,2) - MAT_POWER(JM,2),(33.0_8 * MAT_POWER(JZ,4) - 18.0_8 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_8 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_8 * J2 + 102.0_8 * IDENT)) + ALT_MATMUL((33.0_8 * MAT_POWER(JZ,4) - 18.0_8 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_8 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_8 * J2 + 102.0_8 * IDENT), MAT_POWER(JP,2) - MAT_POWER(JM,2)))
        O6M1 =  -0.25_8 * ii * (ALT_MATMUL(JP - JM, (33.0_8 * MAT_POWER(JZ,5) - 30.0_8 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_8 * MAT_POWER(JZ, 3) + 5.0_8 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_8 * ALT_MATMUL(J2, JZ) + 12.0_8 * JZ)) + ALT_MATMUL((33.0_8 * MAT_POWER(JZ,5) - 30.0_8 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_8 * MAT_POWER(JZ, 3) + 5.0_8 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_8 * ALT_MATMUL(J2, JZ) + 12.0_8 * JZ), JP - JM))
        O60  =   231.0_8 * MAT_POWER(JZ, 6) - 315.0_8 * ALT_MATMUL(J2, MAT_POWER(Jz, 4)) + 735.0_8 * MAT_POWER(JZ, 4) + 105.0_8 * ALT_MATMUL(MAT_POWER(J2,2), MAT_POWER(JZ, 2)) - 525.0_8 * ALT_MATMUL(J2, MAT_POWER(JZ, 2)) + 294.0_8 * MAT_POWER(JZ, 2) - 5.0_8 * MAT_POWER(J2, 3) + 40.0_8 * MAT_POWER(J2, 2) - 60.0_8 * J2
        O61  =   0.25_8 *      (ALT_MATMUL(JP + JM, (33.0_8 * MAT_POWER(JZ,5) - 30.0_8 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_8 * MAT_POWER(JZ, 3) + 5.0_8 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_8 * ALT_MATMUL(J2, JZ) + 12.0_8 * JZ)) + ALT_MATMUL((33.0_8 * MAT_POWER(JZ,5) - 30.0_8 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_8 * MAT_POWER(JZ, 3) + 5.0_8 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_8 * ALT_MATMUL(J2, JZ) + 12.0_8 * JZ), JP + JM))
        O62  =   0.25_8 *      (ALT_MATMUL(MAT_POWER(JP,2) + MAT_POWER(JM,2),(33.0_8 * MAT_POWER(JZ,4) - 18.0_8 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_8 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_8 * J2 + 102.0_8 * IDENT)) + ALT_MATMUL((33.0_8 * MAT_POWER(JZ,4) - 18.0_8 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_8 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_8 * J2 + 102.0_8 * IDENT), MAT_POWER(JP,2) + MAT_POWER(JM,2)))
        O63  =   0.25_8 *      (ALT_MATMUL(MAT_POWER(JP,3) + MAT_POWER(JM,3),(11.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2,JZ) - 59.0_8*JZ)) + ALT_MATMUL((11.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2,JZ) - 59.0_8 * JZ), MAT_POWER(JP,3) + MAT_POWER(JM,3)))
        O64  =   0.25_8 *      (ALT_MATMUL(MAT_POWER(JP,4) + MAT_POWER(JM,4),(11.0_8 * MAT_POWER(JZ,2) - J2 - 38.0_8 * IDENT)) + ALT_MATMUL((11.0_8 * MAT_POWER(JZ,2) - J2 - 38.0_8 * IDENT), MAT_POWER(JP,4) + MAT_POWER(JM,4)))
        O65  =   0.25_8 *      (ALT_MATMUL((MAT_POWER(JP,5) + MAT_POWER(JM,5)),Jz) + ALT_MATMUL(Jz, (MAT_POWER(JP,5) + MAT_POWER(JM,5))))
        O66  =   0.5_8  *      (MAT_POWER(JP,6) + MAT_POWER(JM,6))

    END SUBROUTINE STEVENS6_DP

    SUBROUTINE STEVENS6_QP(JP,JM,J2,JZ,O6M6,O6M5,O6M4,O6M3,O6M2,O6M1,O60,O61,O62,O63,O64,O65,O66)
        !Creates 6th rank Stevens operators at quadruple precision
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)  :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 16), INTENT(OUT) :: O6M6(:,:),O6M5(:,:),O6M4(:,:),O6M3(:,:),O6M2(:,:),O6M1(:,:),O60(:,:),O61(:,:),O62(:,:),O63(:,:),O64(:,:),O65(:,:),O66(:,:)
        COMPLEX(KIND = 16)              :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                       :: J
        COMPLEX(KIND = 16)               :: ii
        
        ii = (0.0_16, 1.0_16)
    
        IDENT = (0.0_16,0.0_16)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_16,0.0_16)
        END DO
    
        O6M6 =  -0.5_16  * ii * (MAT_POWER(JP,6) - MAT_POWER(JM,6))
        O6M5 =  -0.25_16 * ii * (ALT_MATMUL((MAT_POWER(JP,5) - MAT_POWER(JM,5)),Jz) + ALT_MATMUL(JZ, (MAT_POWER(JP,5) - MAT_POWER(JM,5))))
        O6M4 =  -0.25_16 * ii * (ALT_MATMUL(MAT_POWER(JP,4) - MAT_POWER(JM,4),(11.0_16 * MAT_POWER(JZ,2) - J2 - 38.0_16 * IDENT)) + ALT_MATMUL((11.0_16 * MAT_POWER(JZ,2) - J2 - 38.0_16 * IDENT), MAT_POWER(JP,4) - MAT_POWER(JM,4)))
        O6M3 =  -0.25_16 * ii * (ALT_MATMUL(MAT_POWER(JP,3) - MAT_POWER(JM,3),(11.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2,JZ) - 59.0_16*JZ)) + ALT_MATMUL((11.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2,JZ) - 59.0_16 * JZ), MAT_POWER(JP,3) - MAT_POWER(JM,3)))
        O6M2 =  -0.25_16 * ii * (ALT_MATMUL(MAT_POWER(JP,2) - MAT_POWER(JM,2),(33.0_16 * MAT_POWER(JZ,4) - 18.0_16 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_16 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_16 * J2 + 102.0_16 * IDENT)) + ALT_MATMUL((33.0_16 * MAT_POWER(JZ,4) - 18.0_16 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_16 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_16 * J2 + 102.0_16 * IDENT), MAT_POWER(JP,2) - MAT_POWER(JM,2)))
        O6M1 =  -0.25_16 * ii * (ALT_MATMUL(JP - JM, (33.0_16 * MAT_POWER(JZ,5) - 30.0_16 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_16 * MAT_POWER(JZ, 3) + 5.0_16 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_16 * ALT_MATMUL(J2, JZ) + 12.0_16 * JZ)) + ALT_MATMUL((33.0_16 * MAT_POWER(JZ,5) - 30.0_16 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_16 * MAT_POWER(JZ, 3) + 5.0_16 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_16 * ALT_MATMUL(J2, JZ) + 12.0_16 * JZ), JP - JM))
        O60  =   231.0_16 * MAT_POWER(JZ, 6) - 315.0_16 * ALT_MATMUL(J2, MAT_POWER(Jz, 4)) + 735.0_16 * MAT_POWER(JZ, 4) + 105.0_16 * ALT_MATMUL(MAT_POWER(J2,2), MAT_POWER(JZ, 2)) - 525.0_16 * ALT_MATMUL(J2, MAT_POWER(JZ, 2)) + 294.0_16 * MAT_POWER(JZ, 2) - 5.0_16 * MAT_POWER(J2, 3) + 40.0_16 * MAT_POWER(J2, 2) - 60.0_16 * J2
        O61  =   0.25_16 *      (ALT_MATMUL(JP + JM, (33.0_16 * MAT_POWER(JZ,5) - 30.0_16 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_16 * MAT_POWER(JZ, 3) + 5.0_16 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_16 * ALT_MATMUL(J2, JZ) + 12.0_16 * JZ)) + ALT_MATMUL((33.0_16 * MAT_POWER(JZ,5) - 30.0_16 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_16 * MAT_POWER(JZ, 3) + 5.0_16 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_16 * ALT_MATMUL(J2, JZ) + 12.0_16 * JZ), JP + JM))
        O62  =   0.25_16 *      (ALT_MATMUL(MAT_POWER(JP,2) + MAT_POWER(JM,2),(33.0_16 * MAT_POWER(JZ,4) - 18.0_16 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_16 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_16 * J2 + 102.0_16 * IDENT)) + ALT_MATMUL((33.0_16 * MAT_POWER(JZ,4) - 18.0_16 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_16 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_16 * J2 + 102.0_16 * IDENT), MAT_POWER(JP,2) + MAT_POWER(JM,2)))
        O63  =   0.25_16 *      (ALT_MATMUL(MAT_POWER(JP,3) + MAT_POWER(JM,3),(11.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2,JZ) - 59.0_16*JZ)) + ALT_MATMUL((11.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2,JZ) - 59.0_16 * JZ), MAT_POWER(JP,3) + MAT_POWER(JM,3)))
        O64  =   0.25_16 *      (ALT_MATMUL(MAT_POWER(JP,4) + MAT_POWER(JM,4),(11.0_16 * MAT_POWER(JZ,2) - J2 - 38.0_16 * IDENT)) + ALT_MATMUL((11.0_16 * MAT_POWER(JZ,2) - J2 - 38.0_16 * IDENT), MAT_POWER(JP,4) + MAT_POWER(JM,4)))
        O65  =   0.25_16 *      (ALT_MATMUL((MAT_POWER(JP,5) + MAT_POWER(JM,5)),Jz) + ALT_MATMUL(Jz, (MAT_POWER(JP,5) + MAT_POWER(JM,5))))
        O66  =   0.5_16  *      (MAT_POWER(JP,6) + MAT_POWER(JM,6))
    
    END SUBROUTINE STEVENS6_QP

    FUNCTION H_CF_NOOEF_DP(CFPs, stevens_ops, k_max) RESULT(matrix)
        ! Calculates crystal field Hamiltonian at double precision 
        ! without operator equivalent factors (OEFs)
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)        :: CFPs(:,:)
        COMPLEX(KIND = 8), ALLOCATABLE    :: matrix(:,:)
        COMPLEX(KIND = 8), INTENT(IN)     :: stevens_ops(:, :, :, :)
        INTEGER                           :: q, k
        INTEGER, INTENT(IN)               :: k_max

        IF (ALLOCATED(matrix)) DEALLOCATE(matrix)
        
        ALLOCATE(matrix(SIZE(stevens_ops,3), SIZE(stevens_ops,3)))
        matrix = (0.0_8,0.0_8)
        DO k = 1, k_max/2
            DO q = 1, 4*k + 1
                matrix = matrix + CFPs(k,q)*stevens_ops(k,q,:,:)
            END DO
        END DO

    END FUNCTION H_CF_NOOEF_DP

    FUNCTION H_CF_NOOEF_QP(CFPs, stevens_ops, k_max) RESULT(matrix)
        ! Calculates crystal field Hamiltonian at double precision 
        ! without operator equivalent factors (OEFs)
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN)        :: CFPs(:,:)
        COMPLEX(KIND = 16), ALLOCATABLE    :: matrix(:,:)
        COMPLEX(KIND = 16), INTENT(IN)     :: stevens_ops(:, :, :, :)
        INTEGER                            :: q, k
        INTEGER, INTENT(IN)                :: k_max

        IF (ALLOCATED(matrix)) DEALLOCATE(matrix)
        
        ALLOCATE(matrix(SIZE(stevens_ops,3), SIZE(stevens_ops,3)))
        matrix = (0.0_16,0.0_16)
        DO k = 1, k_max/2
            DO q = 1, 4*k + 1
                matrix = matrix + CFPs(k,q)*stevens_ops(k,q,:,:)
            END DO
        END DO

    END FUNCTION H_CF_NOOEF_QP

    FUNCTION H_CF_DP(CFPs, stevens_ops, oefs, k_max) RESULT(matrix)
        ! Calculates crystal field Hamiltonian at double precision 
        ! without operator equivalent factors (OEFs)
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)        :: CFPs(:,:), oefs(:)
        COMPLEX(KIND = 8), ALLOCATABLE    :: matrix(:,:)
        COMPLEX(KIND = 8), INTENT(IN)     :: stevens_ops(:, :, :, :)
        INTEGER                           :: q, k
        INTEGER, INTENT(IN)               :: k_max

        IF (ALLOCATED(matrix)) DEALLOCATE(matrix)
        
        ALLOCATE(matrix(SIZE(stevens_ops,3), SIZE(stevens_ops,3)))
        matrix = (0.0_8,0.0_8)
        DO k = 1, k_max/2
            DO q = 1, 4*k + 1
                matrix = matrix + oefs(k)*CFPs(k,q)*stevens_ops(k,q,:,:)
            END DO
        END DO

    END FUNCTION H_CF_DP

    FUNCTION H_CF_QP(CFPs, stevens_ops, oefs, k_max) RESULT(matrix)
        ! Calculates crystal field Hamiltonian at double precision 
        ! without operator equivalent factors (OEFs)
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN)        :: CFPs(:,:), oefs(:)
        COMPLEX(KIND = 16), ALLOCATABLE    :: matrix(:,:)
        COMPLEX(KIND = 16), INTENT(IN)     :: stevens_ops(:, :, :, :)
        INTEGER                            :: q, k
        INTEGER, INTENT(IN)                :: k_max

        IF (ALLOCATED(matrix)) DEALLOCATE(matrix)

        ALLOCATE(matrix(SIZE(stevens_ops,3), SIZE(stevens_ops,3)))
        matrix = (0.0_16,0.0_16)

        DO k = 1, k_max/2
            DO q = 1, 4*k + 1
                matrix = matrix + oefs(k)*CFPs(k,q)*stevens_ops(k,q,:,:)
            END DO
        END DO

    END FUNCTION H_CF_QP


    SUBROUTINE STE_2_WYB_DP(sCFPs, wCFPs)
    !Converts Stevens parameters to Wybourne scheme at double precision
    !Assumes CFPs is an array laid out as
    ! (m,n) where m = k/2 = 1,2,3 and n  = 1:13 and corresponds to q
    ! n is q shifted to start at 1, so k = 6, q = -6 is (3,1), k = 6, q = -5 is (3,2) and so on
    ! Invalid elements are zero, i.e. (1,12) would be B_2^? so is invalid and is set to 0
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(INOUT)  :: sCFPs(:,:)
        COMPLEX(KIND = 8), INTENT(OUT) :: wCFPs(:,:)
        COMPLEX(KIND = 8)              :: i
        REAL(KIND = 8)                 :: lambda2(5), lambda4(9), lambda6(13)

    !Imaginary unit
        i = (0.0_8, 1.0_8)

    !The equations relating the two formalisms are
    !$B_{k,q} = \\ \lambda B_{k}^{q}  \ \ \ \text{if}\, \,  q = 0 \\ \\\lambda (B_{k}^{q} + i B_{k}^{-q}) \ \ \ \text{if} \ \  q > 0 \\ \lambda (-1)^{q} (B_{k}^{-q} - i B_{k}^{q}) \ \  \text{if}\, \,  q < 0 

    !n.b. Wybourne Parameters are complex

    !Create array of conversion factors lambda for each rank (k)
    !Ordered -q to q
        lambda2 = [sqrt(6.0_8)/3.0_8, -sqrt(6.0_8)/3.0_8, 2.0_8, -sqrt(6.0_8)/3.0_8, sqrt(6.0_8)/3.0_8]
        lambda4 = [4.0_8*sqrt(70.0_8)/35.0_8, -2.0_8*sqrt(35.0_8)/35.0_8, 2.0_8*sqrt(10.0_8)/5.0_8, -2.0_8*sqrt(5.0_8)/5.0_8, 8.0_8, -2.0_8*sqrt(5.0_8)/5.0_8, 2.0_8*sqrt(10.0_8)/5.0_8, -2.0_8*sqrt(35.0_8)/35.0_8, 4.0_8*sqrt(70.0_8)/35.0_8]
        lambda6 = [16.0_8*sqrt(231.0_8)/231.0_8, -8.0_8*sqrt(77.0_8)/231.0_8, 8.0_8*sqrt(14.0_8)/21.0_8, -8.0_8*sqrt(105.0_8)/105.0_8, 16.0_8*sqrt(105.0_8)/105.0_8, -4.0_8*sqrt(42.0_8)/21.0_8, 16.0_8, -4.0_8*sqrt(42.0_8)/21.0_8, 16.0_8*sqrt(105.0_8)/105.0_8, -8.0_8*sqrt(105.0_8)/105.0_8, 8.0_8*sqrt(14.0_8)/21.0_8, -8.0_8*sqrt(77.0_8)/231.0_8,16.0_8*sqrt(231.0_8)/231.0_8]


    !Set Wybourne parameter array to zero
        wCFPs = (0.0_8, 0.0_8)

    !Convert k = 2 parameters
    !q < 0
    !q = -2
        wCFPs(1,1) = lambda2(1)*(-1.0_8)**(-2)*(sCFPs(1,5)-i*sCFPs(1,1))
    !q = -1
        wCFPs(1,2) = lambda2(2)*(-1.0_8)**(-1)*(sCFPs(1,4)-i*sCFPs(1,2))
    !q = 0
        wCFPs(1,3) = lambda2(3) * sCFPs(1,3)
    !q > 0
    !q = 1
        wCFPs(1,4) = lambda2(4)*(sCFPs(1,4)-i*sCFPs(1,2))
    !q = 2
        wCFPs(1,5) = lambda2(5)*(sCFPs(1,5)-i*sCFPs(1,1))

    !Convert k = 4 parameters
    !q < 0
    !q = -4
        wCFPs(2,1) = lambda4(1)*(-1.0_8)**(-4)*(sCFPs(2,9)-i*sCFPs(2,1))
    !q = -3
        wCFPs(2,2) = lambda4(2)*(-1.0_8)**(-3)*(sCFPs(2,8)-i*sCFPs(2,2))
    !q = -2
        wCFPs(2,3) = lambda4(3)*(-1.0_8)**(-2)*(sCFPs(2,7)-i*sCFPs(2,3))
    !q = -1
        wCFPs(2,4) = lambda4(4)*(-1.0_8)**(-1)*(sCFPs(2,6)-i*sCFPs(2,4))
    !q = 0
        wCFPs(2,5) = lambda4(5) * sCFPs(2,5)
    !q > 0
    !q = 1
        wCFPs(2,6) = lambda4(6)*(sCFPs(2,6)-i*sCFPs(2,4))
    !q = 2
        wCFPs(2,7) = lambda4(7)*(sCFPs(2,7)-i*sCFPs(2,3))
    !q = 3
        wCFPs(2,8) = lambda4(8)*(sCFPs(2,8)-i*sCFPs(2,2))
    !q = 4
        wCFPs(2,9) = lambda4(9)*(sCFPs(2,9)-i*sCFPs(2,1))

    !Convert k = 6 parameters
    !q < 0
    !q = -6
        wCFPs(3,1)  = lambda6(1)*(-1.0_8)**(-6)*(sCFPs(3,13)-i*sCFPs(3,1))
    !q = -5 
        wCFPs(3,2)  = lambda6(2)*(-1.0_8)**(-5)*(sCFPs(3,12)-i*sCFPs(3,2))
    !q = -4 
        wCFPs(3,3)  = lambda6(3)*(-1.0_8)**(-4)*(sCFPs(3,11)-i*sCFPs(3,3))
    !q = -3 
        wCFPs(3,4)  = lambda6(4)*(-1.0_8)**(-3)*(sCFPs(3,10)-i*sCFPs(3,4))
    !q = -2 
        wCFPs(3,5)  = lambda6(5)*(-1.0_8)**(-2)*(sCFPs(3,9)-i*sCFPs(3,5))
    !q = -1 
        wCFPs(3,6)  = lambda6(6)*(-1.0_8)**(-1)*(sCFPs(3,8)-i*sCFPs(3,6))
    !q = 0
        wCFPs(3,7)  = lambda6(7) * sCFPs(3,7)
    !q > 0 
    !q = 1 
        wCFPs(3,8)  = lambda6(8)*(sCFPs(3,8)-i*sCFPs(3,6))
    !q = 2 
        wCFPs(3,9)  = lambda6(9)*(sCFPs(3,9)-i*sCFPs(3,5))
    !q = 3
        wCFPs(3,10) = lambda6(10)*(sCFPs(3,10)-i*sCFPs(3,4))
    !q = 4
        wCFPs(3,11) = lambda6(11)*(sCFPs(3,11)-i*sCFPs(3,3))
    !q = 5
        wCFPs(3,12) = lambda6(12)*(sCFPs(3,12)-i*sCFPs(3,2))
    !q = 6
        wCFPs(3,13) = lambda6(13)*(sCFPs(3,13)-i*sCFPs(3,1))


    END SUBROUTINE STE_2_WYB_DP

    SUBROUTINE STE_2_WYB_QP(sCFPs, wCFPs)
    !Converts Stevens parameters to Wybourne scheme at quadruple precision
    !Assumes CFPs is an array laid out as
    ! (m,n) where m = k/2 = 1,2,3 and n  = 1:13 and corresponds to q
    ! n is q shifted to start at 1, so k = 6, q = -6 is (3,1), k = 6, q = -5 is (3,2) and so on
    ! Invalid elements are zero, i.e. (1,12) would be B_2^? so is invalid and is set to 0
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(INOUT)  :: sCFPs(:,:)
        COMPLEX(KIND = 16), INTENT(OUT) :: wCFPs(:,:)
        COMPLEX(KIND = 16)              :: i
        REAL(KIND = 16)                 :: lambda2(5), lambda4(9), lambda6(13)

    !Imaginary unit
        i = (0.0_16, 1.0_16)

    !The equations relating the two formalisms are
    !$B_{k,q} = \\ \lambda B_{k}^{q}  \ \ \ \text{if}\, \,  q = 0 \\ \\\lambda (B_{k}^{q} + i B_{k}^{-q}) \ \ \ \text{if} \ \  q > 0 \\ \lambda (-1)^{q} (B_{k}^{-q} - i B_{k}^{q}) \ \  \text{if}\, \,  q < 0 

    !n.b. Wybourne Parameters are complex

    !Create array of conversion factors lambda for each rank (k)
    !Ordered -q to q
        lambda2 = [sqrt(6.0_16)/3.0_16, -sqrt(6.0_16)/3.0_16, 2.0_16, -sqrt(6.0_16)/3.0_16, sqrt(6.0_16)/3.0_16]
        lambda4 = [4.0_16*sqrt(70.0_16)/35.0_16, -2.0_16*sqrt(35.0_16)/35.0_16, 2.0_16*sqrt(10.0_16)/5.0_16, -2.0_16*sqrt(5.0_16)/5.0_16, 8.0_16, -2.0_16*sqrt(5.0_16)/5.0_16, 2.0_16*sqrt(10.0_16)/5.0_16, -2.0_16*sqrt(35.0_16)/35.0_16, 4.0_16*sqrt(70.0_16)/35.0_16]
        lambda6 = [16.0_16*sqrt(231.0_16)/231.0_16, -8.0_16*sqrt(77.0_16)/231.0_16, 8.0_16*sqrt(14.0_16)/21.0_16, -8.0_16*sqrt(105.0_16)/105.0_16, 16.0_16*sqrt(105.0_16)/105.0_16, -4.0_16*sqrt(42.0_16)/21.0_16, 16.0_16, -4.0_16*sqrt(42.0_16)/21.0_16, 16.0_16*sqrt(105.0_16)/105.0_16, -8.0_16*sqrt(105.0_16)/105.0_16, 8.0_16*sqrt(14.0_16)/21.0_16, -8.0_16*sqrt(77.0_16)/231.0_16,16.0_16*sqrt(231.0_16)/231.0_16]

    !Set Wybourne parameter array to zero
        wCFPs = (0.0_16, 0.0_16)

    !Convert k = 2 parameters
    !q < 0
    !q = -2
        wCFPs(1,1) = lambda2(1)*(-1.0_16)**(-1)*(sCFPs(1,5)-i*sCFPs(1,1))
    !q = -1
        wCFPs(1,2) = lambda2(2)*(-1.0_16)**(-2)*(sCFPs(1,4)-i*sCFPs(1,2))
    !q = 0
        wCFPs(1,3) = lambda2(3) * sCFPs(1,3)
    !q > 0
    !q = 1
        wCFPs(1,4) = lambda2(4)*(sCFPs(1,4)-i*sCFPs(1,2))
    !q = 2
        wCFPs(1,5) = lambda2(5)*(sCFPs(1,5)-i*sCFPs(1,1))

    !Convert k = 4 parameters
    !q < 0
    !q = -4
        wCFPs(2,1) = lambda4(1)*(-1.0_16)**(-4)*(sCFPs(2,9)-i*sCFPs(2,1))
    !q = -3
        wCFPs(2,2) = lambda4(2)*(-1.0_16)**(-3)*(sCFPs(2,8)-i*sCFPs(2,2))
    !q = -2
        wCFPs(2,3) = lambda4(3)*(-1.0_16)**(-2)*(sCFPs(2,7)-i*sCFPs(2,3))
    !q = -1
        wCFPs(2,4) = lambda4(4)*(-1.0_16)**(-1)*(sCFPs(2,6)-i*sCFPs(2,4))
    !q = 0
        wCFPs(2,5) = lambda4(5) * sCFPs(2,5)
    !q > 0
    !q = 1
        wCFPs(2,6) = lambda4(6)*(sCFPs(2,6)-i*sCFPs(2,4))
    !q = 2
        wCFPs(2,7) = lambda4(7)*(sCFPs(2,7)-i*sCFPs(2,3))
    !q = 3
        wCFPs(2,8) = lambda4(8)*(sCFPs(2,8)-i*sCFPs(2,2))
    !q = 4
        wCFPs(2,9) = lambda4(9)*(sCFPs(2,9)-i*sCFPs(2,1))

    !Convert k = 6 parameters
    !q < 0
    !q = -6
        wCFPs(3,1)  = lambda6(1)*(-1.0_16)**(-6)*(sCFPs(3,13)-i*sCFPs(3,1))
    !q = -5 
        wCFPs(3,2)  = lambda6(2)*(-1.0_16)**(-5)*(sCFPs(3,12)-i*sCFPs(3,2))
    !q = -4 
        wCFPs(3,3)  = lambda6(3)*(-1.0_16)**(-4)*(sCFPs(3,11)-i*sCFPs(3,3))
    !q = -3 
        wCFPs(3,4)  = lambda6(4)*(-1.0_16)**(-3)*(sCFPs(3,10)-i*sCFPs(3,4))
    !q = -2 
        wCFPs(3,5)  = lambda6(5)*(-1.0_16)**(-2)*(sCFPs(3,9)-i*sCFPs(3,5))
    !q = -1 
        wCFPs(3,6)  = lambda6(6)*(-1.0_16)**(-1)*(sCFPs(3,8)-i*sCFPs(3,6))
    !q = 0
        wCFPs(3,7)  = lambda6(7) * sCFPs(3,7)
    !q > 0 
    !q = 1 
        wCFPs(3,8)  = lambda6(8)*(sCFPs(3,8)-i*sCFPs(3,6))
    !q = 2 
        wCFPs(3,9)  = lambda6(9)*(sCFPs(3,9)-i*sCFPs(3,5))
    !q = 3
        wCFPs(3,10) = lambda6(10)*(sCFPs(3,10)-i*sCFPs(3,4))
    !q = 4
        wCFPs(3,11) = lambda6(11)*(sCFPs(3,11)-i*sCFPs(3,3))
    !q = 5
        wCFPs(3,12) = lambda6(12)*(sCFPs(3,12)-i*sCFPs(3,2))
    !q = 6
        wCFPs(3,13) = lambda6(13)*(sCFPs(3,13)-i*sCFPs(3,1))


    END SUBROUTINE STE_2_WYB_QP

END MODULE STEV_OPS

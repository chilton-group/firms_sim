MODULE constants
    IMPLICIT NONE

    REAL(KIND = 8), parameter :: pi = 3.141592653589793238462643383279502884197_8
    REAL(KIND = 8), parameter :: kB = 0.6950387_8 !cm-1 K-1
    REAL(KIND = 8), parameter :: hbarcm = 5.30886139629910E-12_8 !cm-1 . s ! This is 1/(2*pi*c)
    REAL(KIND = 8), parameter :: hbar = 1.054571817E-34_8 !m2 kg s-1 rad-1 ! or J s rad-1
    REAL(KIND = 8), parameter :: h = 6.62607015E-34_8 !m2 kg s-1 ! or J s rad-1
    REAL(KIND = 8), parameter :: light = 29979245800.0_8 !cm s-1
    REAL(KIND = 8), parameter :: muB = 0.466866577042538_8 !cm-1 T-1
    REAL(KIND = 8), parameter :: muB_joules = 9.274009994E-24_8! J T-1
    REAL(KIND = 8), parameter :: ep0 = 8.8541878128E-12_8 !m-3 kg-1 s4 A2
    REAL(KIND = 8), parameter :: mu0 = 1.25663706212E-6_8 !m kg s-2 A-2
    REAL(KIND = 8), parameter :: amu_to_kg = 1.66053906660E-27_8
    REAL(KIND = 8), parameter :: ge = 2.00231930436256_8
    INTEGER, parameter        :: mode_limit = 10000

END MODULE constants

Installation
============

Requirements
------------
•	Fortran compiler (gfortran (tested 9.3.0) or ifort (tested 19.1.2.258))
•	Linear algebra Fortran libraries (lapack+blas (tested 3.9.0-1build1) or MKL (tested 19.1.2.258))
•	Python (tested 3.8.5 and 3.9.0) with packages listed in `src/requirements.txt`
•	Make (tested 4.2.1)
•   git

Compilation
------------
Currently ``firms_sim`` is only tested and officially supported on Linux and macOS, and has been tested on
Ubuntu 20.04 running both natively and within the Windows Subsystem for Linux, and on macOS Catalina 10.15.6.

To install, first clone a copy of the repository using git, or download manually through the gitlab page::

    git clone https://gitlab.com/nfchilton/firms_sim

then navigate to ``src``::

    cd src

and use the makefile::

    make all install

This installs ``firms_sim`` and its ancillary programs to::

    /usr/local/bin

The makefile will default to using ifort, to override this and use gfortran add the optional argument::

    compiler=gfortran

to the::

    make all install

command.

To test that the installation was successful, run::
    
    firms_sim -h

in the terminal.

Compilation and installation takes around a minute or so on modern personal computers.

Updating
--------

To update the code simply navigate to the repository on your machine and run::

    git pull

then move to ``src`` and run ``make``::

    cd src;make all install

If there is an error about local changes, first type::

    git stash

and repeat the above ``make`` command.

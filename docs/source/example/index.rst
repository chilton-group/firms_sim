Example simulation
==================

Example input files are provided in the ``example`` directory of the git repository.

These correspond to a two mode simulation of the FIRMS map of Yb(trensal) using modes 4 and 5 as detailed in Kragskow `et al.

Running ``firms_sim`` on these input files should take around a second or so on a single core,
and the expected output files are also given in example/output.


References
^^^^^^^^^^

J\. G. C. Kragskow, J. Marbey, C. D. Buch, J. Nehrkorn, M. Ozerov, S. Piligkos, S. Hill and N. F. Chilton, Nature Communications, 1, 13, 2022



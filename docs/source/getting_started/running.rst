Running `firms_sim`
===================

To run ``firms_sim`` using a given input file, simply execute ::

    firms_sim <input_file>

in a terminal. The files given in :ref:`Input files<Input Files>` must be present in the current directory.


By default ``firms_sim`` uses Open Multiprocessing (OMP) to parallelise the calculation of the individual IR spectra.
To modify the number of threads used by OMP, set the environment variable ``$OMP_NUM_THREADS`` using the command::
    
    export OMP_NUM_THREADS=n

Where ``n`` is the desired number of threads.


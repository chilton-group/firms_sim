Visualising Output Data
=======================

The python script `firms_heatmap` is included with `firms_sim` and can be used to visualise the output of a firms_sim
simulation. To run, ensure you are in the same directory as the data_XYZ.dat file you want to visualise,
and then execute the command::

    firms_heatmap <XYZ>

where <XYZ> is the list of mode indices employed in your simulation separated by underscores, e.g.::

    firms_heatmap 34_35_36

will plot the spectra obtained when coupling to modes 34-36 using the data from the file ``data_34_35_36.dat``.

The script normalises the data by dividing by the `average` FIRMS map, defined as the sum of the maps at different fields
divided by the number of field values.

To see the full options of ``firms_heatmap``, run the command::

    firms_heatmap -h

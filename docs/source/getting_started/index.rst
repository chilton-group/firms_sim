Getting Started
===============

.. toctree::
   :maxdepth: 2
   :hidden:

    Input Keywords <input_kwrds>
    Input Files <input_files>
    Running a simulation <running>
    Output Files <output_files>
    Visualising Output Data <output_plot>

`firms_sim` accepts input in the form of a plain text input file which configures the current simulation, 
alongside a series of plain text data files which contain parameters used in the simulation.



Output Files
============

firms_sim.out
-------------

This is the main output of ``firms_sim``, and contains::

    Number of threads and program version number (git commit hash)
    Input File repeated from input.dat
    Metal Ion Data
        oxidation state, L, S, J quantum numbers
    Magnetic Field Information
        Strength values
        Number of field orientations
    Equilibrium electronic structure
        Energies and magnetic moments of states
        CF Eigenvectors
    Equilibrium Vibrational Modes
        Equilibrium energies
    Zero Field Total Hamiltonian
        Number of states
        Energies with and without coupling Hamiltonian
        Eigenvectors with and without coupling Hamiltonian


data_XYZ.dat
------------------

For a given simulation involving modes XYZ (formatted as mode indices separated by underscores), this file
contains absorbance as a function of both incident energy and magnetic field.

The first line contains information of the magnetic fields and is written as::

    Min. field, Max. field, Field step

The second line contains the indices of the modes used in the simulation, e.g 45, 46, 47

The third line contains information on the range of indicdent energies, and is written as::

    Min. energy, Max energy, Number of energy points

and finally the raw data is written, where each column is a full spectrum with fixed magnetic field strength. 

In summary, the raw data will have ``n_fields`` columns, and ``n_energies`` rows, e.g. ::

    Field_1_energy_1, Field_2_energy_1, ..., Field_(n_fields)_energy_1
    Field_1_energy_2, Field_2_energy_2, ..., Field_(n_fields)_energy_2
    ..., ..., ..., ...
    Field_1_energy_(n_energies), Field_2_energy_(n_energies), ..., Field_(n_fields)_energy_(n_energies)

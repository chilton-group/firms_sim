Input Files
==================

The following files are required by ``firms_sim`` and must be in a single directory

* ``input.dat`` - Configuration options for simulation
* ``CFP_polynomials.dat`` - Parameters for 3rd order polynomial fits of change in each CFP as a function of displacement (in units of ZPD) along each mode (see :ref:`CFP polynomials<CFP polynomials>`)
* ``EQ_CFPs.dat`` - Equilibrium CFPs in |cm-1| (see :ref:`Equilibrium CFPs<Equilibrium CFPs>`)
* ``vib_energies.dat`` - Vibrational mode energies in |cm-1| (see :ref:`Vibrational energies<Vibrational energies>`)
* ``reduced_masses.dat`` - Reduced masses in atomic mass units (see :ref:`Reduced masses<Reduced masses>`)
* ``dipole_derivatives.dat`` - Derivatives of x, y, and z components of dipole moment with respect to displacement along each vibrational mode in |km1/2 mol-1/2| (see :ref:`Intensities<intensities>`)
* ``tau.hdf5`` - Instead of using the CFP method, the direct method using the derivatives of the ab initio Hamiltonian can be given.

Optionally, the file ``eq_cf_energies.dat`` can be provided, which contains alternative equilibrium CF energies in |cm-1| (see :ref:`Equilibrium CF Energies<Equilibrium CF Energies>`). `n.b.` this file must have this name.

tau.hdf5
--------

This file contains the direct derivatives of the ab initio Hamiltonian, as obtained from spin_phonon suite using the ``direct`` method. See the documentation for spin_phonon_suite for details.

CFP polynomials
---------------

This file contains fits of the change in each Stevens crystal field parameter (CFP) from its equilibrium value as a function
of displacement along a given vibrational mode (in units of zero point displacement) to a third order polynomial (See Theory).
These must be pure CFPs, i.e. they do *not* include the operator equivalent factors (OEFs).

The required format of this file is as follows, and an example is given in the ``example`` directory. ::

    Data for mode 1 = fitting parameters to ax^3 + bx^2 ¬+cx (comment line)
    2 -2 a b c
    2 -1 a b c
    2  0 a b c
    2  1 a b c
    2  2 a b c
    4 -4 a b c
    …
    6 -6 a b c
    Data for mode 2 = fitting parameters to ax^3 + bx^2 ¬+cx (comment line)
    …
    Data for mode ... = fitting parameters to ax^3 + bx^2 ¬+cx (comment line)
    ...

N.B. Data must be supplied for all modes of the system, not just those used in the simulation.

Equilibrium CFPs
----------------

This file contains the equilibrium Stevens crystal field parameters (CFP) up to rank (:math:`k`) 6.
The required format of this file is shown below, and an example is given in the ``example`` directory.
The CFPs can optionally include the OEFs, provided the nooef keyword is given in the ``firms_sim`` input file. ::

    Parameter for k = 2 q = - 2
    Parameter for k = 2 q = - 1
    Parameter for k = 2 q =   0
    Parameter for k = 2 q =   1
    Parameter for k = 2 q =   2
    Parameter for k = 4 q = - 4
    Parameter for k = 4 q = - 3
    …
    Parameter for k = 6 q =   6


Vibrational energies
--------------------
This file contains the vibrational mode energies in |cm-1| written in a single column with an arbitrary one-lie header.
An example can be seen in the `example` directory.

N.B. Data must be supplied for all modes of the system, not just those used in the simulation.

Reduced masses
--------------
This file contains the reduced mass of each mode in atomic mass units written in a single column with no headers.
An example can be seen in the `example` directory.

Can be obtained using the ``gaussian_suite extractor -i <file>.log --freq reduced_mass > reduced_masses.dat`` command.

N.B. Data must be supplied for all modes of the system, not just those used in the simulation.

Dipole derivatives
------------------
This file contains the dipole derivatives in units of |km1/2 mol-1/2| calculated for the x, y, and z components each mode. Note,
the displacements should not use the ZPD coordinate system employed by ``firms_sim`` as this conversion is carried out internally.
The required format of this file is as follows, and an example is given in the `example` directory. ::


    X derivative for mode 1  Y derivative for mode 1  Z derivative for mode 1
    X derivative for mode 2  Y derivative for mode 2  Z derivative for mode 2
    …
    X derivative for mode Nvib  Y derivative for mode Nvib  Z derivative for mode Nvib

Can be obtained using the ``gaussian_suite dip_deriv <file>.log <file>.fchk`` command.

N.B. Data must be supplied for all modes of the system, not just those used in the simulation.

Equilibrium CF Energies
-----------------------
This file contains the new equilibrium CF energies in |cm-1| written in a single column with no headers.
An example can be seen in the `example` directory.

.. |cm-1| replace:: cm\ :sup:`-1`
.. |km1/2 mol-1/2| replace:: km\ :sup:`1/2`\ mol\ :sup:`-1/2`

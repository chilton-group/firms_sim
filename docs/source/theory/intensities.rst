Intensities
===========

The FIRMS experiment measures the absorption of far-infrared radiation, where the absorption changes as a function of energy as the incident radiation moves on and off resonance
with vibrational, electronic, or vibronic transitions. The Einstein coefficient :math:`B_{12}` describes the probability of
an absorption event which results in population transfer from state 1 to state 2, and has electric and magnetic contributions
as it refers to interaction with electromagnetic radiation.

.. math ::
    B&=B_\mathrm{e} \otimes B_\mathrm{v} \\
    B_\mathrm{e}&=(2\pi \mu_0)/(3\hbar^2 c^2) \mu_\mathrm{M}^\dagger \mu_\mathrm{M} \\ 
    B_\mathrm{v}&=2\pi/(3\hbar^2 c^2 \epsilon_0) \mu_\mathrm{E}^\dagger \mu_\mathrm{E} \\

Electric dipole operator
^^^^^^^^^^^^^^^^^^^^^^^^

The electric dipole moment :math:`\mu_\mathrm{E}` for a single particle between a pair of states
(where :math:`z` is the particle's charge, and :math:`r` is the position operator) is

.. math ::
	\mu_{\mathrm{E}_{fi}}= \langle \psi_f |zr| \psi_i\rangle

For a system of many particles this becomes:

.. math ::
	\mu_{\mathrm{E}_{fi}}= \langle \psi_f |z_1 r_1 + z_2 r_2 + ...| \psi_i\rangle

In ``firms_sim`` the electric dipole moment is calculated in the basis of harmonic vibrational states :math:`|n\rangle`.
This is typically accomplished by expanding the electric dipole moment as a Taylor series in vibrational mode coordinate :math:`Q`,
such that for mode :math:`j`

.. math ::
	\mu_\mathrm{E}=\mu_\mathrm{E}^{0}+\frac{\partial \mu_\mathrm{E}}{\partial Q_j}_\mathrm{eq} Q_j+ \frac{1}{2} \frac{\partial ^2 \mu_\mathrm{E}}{\partial Q_j^2}_\mathrm{eq}  Q_j^2+ \dots

The first term is independent of :math:`Q` and is simply the electric dipole moment of the system at equilibrium
(i.e. the permanent electric dipole) and so any transition matrix element :math:`\langle n'|\mu_\mathrm{E}^{0}|n\rangle` will be zero.

The term linear in :math:`Q` is the transition dipole moment for a single quantum transition 
(e.g. :math:`|0\rangle\rightarrow|1\rangle`), while higher order terms correspond to overtones 
(e.g. :math:`|0\rangle\rightarrow|2\rangle`), which are small by comparison and are therefore excluded.
Then, the electric transition dipole moment matrix elements are

.. math ::
	\langle n' |\mu_\mathrm{E} |n\rangle=\langle n' |\frac{\partial \mu_\mathrm{E}}{\partial Q_j}_\mathrm{eq} Q_j |n\rangle=\frac{\partial \mu_\mathrm{E}}{\partial Q_j}_\mathrm{eq} \langle n' |Q_j |n \rangle

The quantities :math:`\frac{\partial \mu_\mathrm{E}}{\partial Q_j}_\mathrm{eq}` are the derivatives of the electric dipole
with respect to displacement along a given vibrational mode. These can be calculated using various quantum chemistry packages,
though in general these will not use the ZPD based coordinate system of ``firms_sim``, and will instead define :math:`Q_j` as
displacement along one unit of :math:`\hat{\vec{Q}}_j`, a definition referred to here as :math:`\tilde{Q}_j`, noting that

.. math ::
    \tilde{Q}_j=Q_{j,\mathrm{ZPD}}⋅Q_j

Therefore, to match the value of :math:`\frac{\partial \mu_\mathrm{E}}{\partial Q_j}_\mathrm{eq}` calculated *ab initio*,
the following definition must be used when evaluating :math:`\tilde{Q}` in the harmonic basis

.. math ::
	\tilde{Q}_j &= Q_{j,\mathrm{ZPD}}Q_j=\sqrt{\hbar/(\mu_j \omega_j)} \cdot \frac{1}{\sqrt{2}} (a^{\dagger}+a)\\
    &= \sqrt{\hbar/(4\pi c \bar{\nu}_j \mu_j)} (a^{\dagger}+a) a^{\dagger} |n\rangle \\
    &=\sqrt{n+1}|n+1\rangle a |n\rangle =\sqrt{n}|n-1\rangle

Which gives the non-zero matrix elements:

.. math ::
	\langle n_j-1|\tilde{Q}_j | n_j \rangle &= \sqrt{\hbar/(2\mu_j \omega_j)} \langle n_j-1|a | n_j \rangle = \sqrt{(\hbar n_j)/(2\mu_j \omega_j)}\\
	\langle n_j+1|\tilde{Q}_j | n_j \rangle &= \sqrt{\hbar/(2\mu_j \omega_j)} \langle n_j+1|a^{\dagger} | n_j \rangle =\sqrt{\hbar(n_j+1)/(2\mu_j \omega_j)}

Therefore, in terms of :math:`\tilde{Q}_j`

.. math ::
    \langle n_j' |\mu_\mathrm{E} |n_j\rangle =\frac{\partial\mu_\mathrm{E}}{\partial\tilde{Q}_j}_\mathrm{eq} \langle n_j' |\tilde{Q}_j |n_j\rangle 

``firms_sim`` expects derivatives with respect to unit displacement along a mode and performs this conversion internally.
The derivatives can be obtained from a Gaussian Frequency calculation using the ``gaussian_suite dip_deriv`` subprogram.

Magnetic dipole operator
^^^^^^^^^^^^^^^^^^^^^^^^

.. math ::
    \mu_\mathrm{M} = \mu_\mathrm{B} g_J \hat{\vec{J}}

Where :math:`\mu_\mathrm{B}` is the Bohr magneton [J T :sup:`-1` = A m :sup:`2`], :math:`g_J` the dimensionless Lande g-factor,
and :math:`\hat{\vec{J}}` the dimensionless total angular momentum vector operator which is constructed in
a basis of :math:`|J,m_J\rangle` states. 

.. |km1/2 mol-1/2| replace:: km\ :sup:`1/2`\ mol\ :sup:`-1/2`


Putting it all together
^^^^^^^^^^^^^^^^^^^^^^^

To construct a single FTIR absorption spectrum at fixed magnetic field, ``firms_sim`` assumes that each transition is
described by a normalised Gaussian function centred at the transition energy ΔE with an user-defined standard deviation,
and peak height equal to the product of the corresponding Einstein coefficient and the Boltzmann population of the initial state.
To simulate a FIRMS map multiple absorption spectra must be calculated as a function of magnetic field. However, as experiments
can be performed for powder samples, ``firms_sim`` is able integrate over the orientation of the external magnetic field in
using the Zaremba-Conroy-Wolfberg (ZCW) model with a user-configurable number of points. :sup:`[1]`

Furthermore, ``firms_sim`` assumes a Voight experimental configuration, where the incident radiation is perpendicular to the
applied magnetic field, and accounts for the unpolarised nature of the incident IR radiation by following the treatment of
Nehrkorn et al. to integrate all possible polarisation vectors. :sup:`[2]`

.. |km1/2 mol-1/2| replace:: km\ :sup:`1/2`\ mol\ :sup:`-1/2`

References
^^^^^^^^^^

1. M. Eden, and M. H. Levitt, J. Magn. Res. 1998, 132, 220-239
2. J. Nehrkorn, A. Schnegg, K. Holldack and S. Stoll, Phys. Rev. Lett., 2015, 114, 010801.


Electronic structure
====================

Due to the highly contracted nature of the 4f orbitals, the electronic structures of Ln(III) SIMs are typically described
with reference to the free-ion Hund’s rule ground term :math:`{^{2S+1}}L_J`, where :math:`J=L \pm S` depending on the
number of electrons in the 4f shell. While there are exceptions (e.g. Sm), for most Ln(III) ions the populations of
excited :math:`J` multiplets are essentially zero at room temperature.

Coordination of a set of ligands to a free Ln ion gives rise to a crystal field (CF) splitting of the degenerate
:math:`m_J` substates of the ground :math:`J` multiplet and is described by a CF Hamiltonian.

.. math ::

    \hat{H}_\mathrm{CF} = \sum_{k=2}^{2J} \sum_{q=-k}^{q=k} \theta_k B_k^q \hat{O}_k^q

Where the :math:`B_k^q` are Stevens CF parameters with rank :math:`k` (which take only even values), and order :math:`q`, the
:math:`\hat{O}_k^q` are Stevens operators in the :math:`|J,m_J\rangle` basis which are linear combinations of the angular
momentum operators :math:`\hat{J}`.


The vibrational modes of a molecule are commonly described using the harmonic approximation,
where the vibrational modes are orthogonal vectors obtained by diagonalising the mass-weighted Hessian matrix,
and the energies of the harmonic states of the jth mode are
given by

.. math ::

	\hat{H}_{\mathrm{vib}_j} = \hbar \omega_j \left(n_j+\frac{1}{2}\right)

Where :math:`\omega_j` is the angular frequency of the :math:`j^\mathrm{th}` vibrational mode.
This Hamiltonian is diagonal in the :math:`|n_j\rangle` basis, where :math:`n_j` is the vibrational quantum number
ranging from zero to infinity (in practice this is typically truncated at some finite value). 

The uncoupled, equilibrium electronic and vibrational states of a Ln(III) complex are given by

.. math ::

	\hat{H}_\mathrm{eq} = \hat{H}_\mathrm{CF} + \sum_j^{n_\mathrm{vib.}} \hat{H}_{\mathrm{vib}_j}

summing over the :math:`n_\mathrm{vib.}` vibrational modes of the molecule.

The eigenstates of this Hamiltonian can be written as :math:`|\Psi, n_1, n_2, \dots \rangle`, where :math:`\psi` denotes
the electronic (CF) component of a given state.

The vibrational modes of a molecule act to modulate the equilibrium electronic structure over time in a process known as
*vibronic* (or *spin-phonon*) coupling. To describe such coupling in Ln(III) ions, the crystal field parameters
:math:`B_k^q \left(Q_j,…,Q_{n_\mathrm{vib.}} \right)` can be expanded as a Taylor series in normal mode displacements
centred at the equilibrium structure :math:`Q_j=0 \ \ \forall j`, where :math:`Q_j=1` is defined as the zero-point displacement of mode j.

.. math ::

	B_k^q \left(Q_j,…,Q_{n_\mathrm{vib.}} \right) = {B_k^q}_\mathrm{eq.} + \sum_j^{3N-6} Q_j \left(\frac{\partial B_k^q}{\partial Q_j}\right)_\mathrm{eq} - \frac{1}{2} \sum_{j'}^{3N-6} \sum_j^{3N-6} Q_j Q_{j'} \left(\frac{\partial^2 B_k^q}{\partial Q_j \partial Q_{j'}}\right)_\mathrm{eq} + \dots

The term independent of :math:`Q_j` describes the equilibrium electronic structure and so can be discarded from any future
discussions of the form of the spin-phonon coupling. The remaining terms can then be partitioned into their contributions
to the various relaxation processes. Those terms which are linear in :math:`Q_j` contain all information on the spin-one phonon
interaction, while those quadratic in :math:`Q_j` describe spin-two phonon interactions.

Taking the linear term allows for the definition of the spin-one phonon Hamiltonian for coupling to the :math:`j\mathrm{th}`
mode 

.. math ::

	\hat{H}_{\mathrm{coup.}, j} = \sum_{k=2}^{2J} \sum_{q=-k}^{q=k} \theta_k Q_j \left(\frac{\partial B_k^q}{\partial Q_j}\right)_\mathrm{eq} \hat{O}_k^q

To calculate the matrix representation of this Hamiltonian the weak-coupling limit is assumed,
where vibrational modes are unaffected by coupling to electronic states, and thus the electronic and
vibrational components can be separated

.. math ::

	\hat{H}_{\mathrm{coup.}, j} &= \hat{H}_{\mathrm{coup_e}, j} \otimes \hat{H}_{\mathrm{coup_v}, j} \\
	\langle m_J' | \hat{H}_{\mathrm{coup_e}, j} | m_J \rangle &= \langle m_J' | \sum_{k=2}^{2J} \sum_{q=-k}^{q=k} \left(\frac{\partial B_k^q}{\partial Q_j}\right)_\mathrm{eq} \hat{O}_k^q | m_J \rangle \\
	\langle n_j \pm 1 | \hat{H}_{\mathrm{coup_v}, j} | n_j \rangle &= \langle n_j \pm 1 | Q_j | n_j \rangle

The matrix elements in Equation 10 are evaluated in the harmonic basis using the harmonic oscillator raising and
lowering operators, which using our ZPD-based coordinate system can be written as

.. math ::

	\langle n_j - 1| Q_j | n_j \rangle &= \frac{1}{\sqrt{2}} \langle n_j - 1|\hat{a}|n_j\rangle = \sqrt{\frac{n_j}{2}} \\ 
	\langle n_j + 1| Q_j | n_j \rangle &= \frac{1}{\sqrt{2}} \langle n_j - 1|\hat{a}^\dagger|n_j\rangle = \sqrt{\frac{n_j + 1}{2}} \\ 

The matrix elements of the electronic part are straightforward to evaluate as they are simply those of a crystal field Hamiltonian where
the parameters :math:`B_k^q` have been replaced by a set of first derivatives with respect to displacement along a given normal mode
vector. 

To obtain the CFP derivatives, the change in each crystal field parameter from its equilibrium value as a function of displacement
along a given mode is fitted to a 3rd order polynomial. Here, :math:`Q_j` is in units of :math:`Q_{j,\mathrm{ZPD}}`,
thus :math:`Q_j = 1` corresponds to a physical displacement of the atoms along :math:`\hat{Q}_j` up to :math:`Q_{j,\mathrm{ZPD}} = 1`. (See :ref:`Displacements <Vibrational Displacements>`)

.. math :: 

	B_k^q (Q_j)= aQ_j^3 + bQ_j^2 + cQ_j + {B_k^q}_{{}_\mathrm{eq}}

Defining the equilibrium geometry as :math:`Q_j=0` the first derivative of the above with respect to :math:`Q_j` evaluated at equilibrium
is simply the linear coefficient

.. math ::

	\left(\frac{\partial B_k^q}{\partial Q_j}\right)_\mathrm{eq} = c
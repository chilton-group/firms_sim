Theory
======

.. toctree::
   :maxdepth: 2
   :hidden:

    Electronic structure <electronic_structure>
    Vibrational Displacements <displacements>
    Intensities <intensities>

The theory behind ``firms_sim`` is explained in Kragskow `et al`, though some particular aspects are explained in these pages
for clarity.

References
^^^^^^^^^^

J\. G. C. Kragskow, J. Marbey, C. D. Buch, J. Nehrkorn, M. Ozerov, S. Piligkos, S. Hill and N. F. Chilton, Nature Communications, 1, 13, 2022

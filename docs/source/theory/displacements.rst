Vibrational Displacements
=========================

Assuming the harmonic approximation, the cartesian displacement vector of a given mode :math:`j` in its :math:`n^{\mathrm{th}}` harmonic
state is defined as

.. math ::
    \vec{Q}_{j,n} = \sqrt{\frac{2n_j + 1}{\omega_j \mu_j}} \hat{Q}_j

Where :math:`\mu_j` is the reduced mass of mode :math:`j`, and :math:`\hat{Q}_j` is a unit vector containing each atomic
displacement of mode :math:`j`. The zero-point displacement (ZPD) of a mode is

.. math ::
    Q_{j,\mathrm{ZPD}} = \left|\vec{Q}_{j,0}\right| = \sqrt{\frac{\hbar}{\omega_j \mu_j}}

In ``firms_sim``, displacements :math:`Q_j` are all treated in units of :math:`Q_{j,\mathrm{ZPD}}`.

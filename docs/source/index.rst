
Home
====

.. toctree::
   Installation <installation>
   Theory <theory/index>
   Getting Started <getting_started/index>
   Example <example/index>
   :maxdepth: 3
   :caption: Contents:
   :hidden:

Welcome to the documentation for ``firms_sim``.

These pages contain instructions for working with ``firms_sim``, along with explanations of the theory used within the code.

If you use ``firms_sim`` please cite

   J\. G. C. Kragskow, J. Marbey, C. D. Buch, J. Nehrkorn, M. Ozerov, S. Piligkos, S. Hill and N. F. Chilton, Nature Communications, 1, 13, 2022

